Replace a files text [from] [to] using regular expressions. For   
file names that match a user-specified glob or regex pattern.   
Traverse the directory tree at a specified path and depth.  
  
The default behaviour moves the processed file into the source  
files directory without replacing the source file: `--no-replace`.
  
Supports plain text files using the UTF-8 [Charset](https://docs.oracle.com/en/java/javase/19/docs/api/java.base/java/nio/charset/package-summary.html).
  
Usage: `jreplace [-h] [--[no-]replace] [-d=int] -f=text [-m=int] [--output=<output>] [-p=directory] -t=text (-r=expression | -g=pattern)` 
  
Rapid Eye Movement (R.E.M), 1992, Drive, Automatic For The People, viewed 03 June 2021, <https://open.spotify.com/track/3geRffSf6eAruhg23RkpKV?si=e80b0d913fd04051>  


----


![](src/main/resources/jpg/object-diagram.png)


----


hyperfine run on a directory size 752 megabytes.  


![](src/main/resources/jpg/jreplace-jlink-no-replace-752MB.png)
  
  
![](src/main/resources/jpg/jreplace-output.png)


----


#### GraalVM native-image ####


```shell
 
$ ./gradlew jar

# where ... is the name of the parent directory leading to the jar 
$ cp $GRADLE_USER_HOME/caches/modules-2/files-2.1/info.picocli/picocli/4.7.0/.../picocli-4.7.0.jar build/libs

# for short lived processes, see graalvm man page for additional command line flags.  
# declare the individual jars in build/libs.
$ $GRAALVM_HOME/bin/native-image --module-path build/libs/jreplace-0.6.1.jar:build/libs/picocli-4.7.0.jar:$GRAALVM_HOME/jmods \
                                 -H:Module=au.kjd.jreplace \
                                 -H:Name=jreplace \
                                 -H:+ReportExceptionStackTraces \
                                 --no-fallback

$ du -sh jreplace
> 19M    jreplace

$ chmod go-rwx jreplace
$ mv jreplace $HOME/local/bin

# Run the test suite
$ ./gradlew test --rerun-tasks
$ xdg-open build/reports/tests/test/index.html
```


#### jlink ####


```shell

$ ./gradlew jar

# where ... is the name of the parent directory leading to the jar 
$ cp $GRADLE_USER_HOME/caches/modules-2/files-2.1/info.picocli/picocli/4.7.0/.../picocli-4.7.0.jar build/libs

$ jlink --output jrt-jreplace \
        --module-path build/libs/jreplace-0.6.1.jar:build/libs/picocli-4.7.0.jar:$JAVA_HOME/jmods \
        --add-modules au.kjd.jreplace

$ du -sh jrt-jreplace
>  54M   jrt-jreplace

# see java man page for additional command line flags.
$ echo '$HOME/<your-path-to>/jrt-jreplace/bin/java -XX:+UseSerialGC --module au.kjd.jreplace "$@"' > jreplace

$ chmod -R go-rwx jrt-jreplace && \
  chmod u+x,go-rwx jreplace

$ mv jrt-jreplace /somewhere/you/want/ && \
  mv jreplace $HOME/local/bin/

# Run the test suite
$ ./gradlew test --rerun-tasks
$ xdg-open build/reports/tests/test/index.html
```


#### Usage ####


```text

Usage: jreplace [-h] [--[no-]replace] [-d=int] -f=text [-m=int] [--output=<output>] [-p=directory] -t=text (-r=expression | -g=pattern)

  -h, --help               JReplace source code location:
                           https://tinyurl.com/2p8b66n2

                           Enter a long URL to make a TinyURL
                           https://tinyurl.com/app/

  -p, --path=directory     The starting directory from where to traverse. If no option is
                           given defaults to the present working directory.

  -d, --depth=int          Maximum traversal depth where the default value is 1,
                           which traverses the starting directory. Possible range
                           is: 0 < depth < 2^31. The maximum depth is equal to
                           2147483646 and may be used to indicate that all levels
                           should be visited. Refer to the below link for further
                           documentation. https://tinyurl.com/2p8s3vjy

  -f, --from=text          Text structuring; word, sentence, syntax, punctuation, or regular
                           expression pattern. Refer to the below link for further
                           documentation on regular expressions.
                           https://tinyurl.com/8n948zu2

  -t, --to=text            Text structuring; word, sentence, syntax, punctuation.

  -m, --megabytes=int      Maximum number of megabytes to split a file on. Where the default
                           value is 0 which process a file without splitting. Possible range
                           is: 0 <= megabytes <= 2047. Splitting a large file reduces the
                           required memory to process the file.

      --[no-]replace       The default is not to replace a source file after processing.

                           Places the processed file in the source files directory and
                           appends '-jreplace-XXXXXXX' where X is current time in
                           milliseconds to ensure a unique file name.

      --output=<output>    Output the stats to a file rather than to the consoles standard
                           output. If the file doesn't exist create it. If the file exists
                           append to the file.

The tinyurl link takes you to the appropriate java
api documentation for expression and pattern support.

  -r, --regex=expression   A FileSystem implementation that supports the regex syntax.
                           https://tinyurl.com/8n948zu2

  -g, --glob=pattern       A FileSystem implementation that supports the glob syntax.
                           https://tinyurl.com/4w7ecmmt

```


----


JReplace operates sequentially, given a root directory with a height of six, it's first level has fifteen directories, and a total file space usage of 79 gigabytes that contains a range of file sizes in megabytes; 2304.0, 1152.0, 288.0, 71.0, 14.0, 2.7, 0.5.  

A further reduction to the footprint is achievable with the command line flags: `-XX:MaxHeapFreeRatio=10 -XX:MinHeapFreeRatio=5 -XX:-ShrinkHeapInSteps`. At an increase in cpu usage.  

`$ jrt-jreplace/bin/java -XX:+UseSerialGC --module au.kjd.jreplace --depth=12 --from="(Java|java)" --to="tzez" --glob="*.kak" --megabytes=20`


![](src/main/resources/jpg/jreplace-jlink-79GB-file-io.png)

![](src/main/resources/jpg/jreplace-jlink-79G-cpu-heap.png)

![](src/main/resources/jpg/jreplace-jlink-79G-method-profile.png)

![](src/main/resources/jpg/jreplace-jlink-79G-gc-summary.png)


----


TODO:


- [ ] [Apache PDFBox](https://pdfbox.apache.org/) - Java PDF Library
- [ ] [Apache POI](https://poi.apache.org/index.html) - Java API for Microsoft Documents


#### Lessons learnt ####


Software development with profiling.


- JetBrains 2022, When is profiling helpful?, Intellij IDEA, version 2022.2, viewed 20 November 2022, <https://www.jetbrains.com/help/idea/2022.2/profiler-intro.html?keymap=primary_gnome#when-is-profiling-helpful>
- SDKMAN 2022, JDK Mission Control (8.1.1.51-zulu), Oracle, viewed 20 November 2022, <https://sdkman.io/sdks#jmc>


#### Installing ####


The Software Development Kit Manager: *'[SDKMAN](https://sdkman.io/)! installs smoothly on Mac OSX, Linux, WLS, Cygwin, Solaris and FreeBSD. We also support Bash and ZSH shells'.*  


#### References ####


-  Apache 2020, _'Apache PDFBox - A Java PDF Library'_, Apache Software Foundation, viewed 30 November 2020, https://pdfbox.apache.org/
-  Apache 2020, _'Apache POI - Java API for Microsoft Documents'_, Apache Software Foundation, viewed 30 November 2020, https://poi.apache.org/index.html
-  David, P 2020, _'hyperfine: A command-line benchmarking tool.'_, Github, viewed 30 November 2020, https://github.com/sharkdp/hyperfine
-  Gonzalez, G 2016, _'bench: Command-line benchmark tool'_, Hackage, viewed 30 November 2020, https://hackage.haskell.org/package/bench
-  Gradle 2020, _'Command-Line Interface'_, viewed 01 December 2020, https://docs.gradle.org/current/userguide/command_line_interface.html
-  Gradle 2020, _'The Gradle Wrapper'_, viewed 13 August 2020, https://docs.gradle.org/current/userguide/gradle_wrapper.html
-  Masand, N 2020, _'Java – Read and Write Microsoft Word with Apache POI'_, MKyong dot com, viewed 15 May 2021, https://mkyong.com/java/java-read-and-write-microsoft-word-with-apache-poi/
-  Melendez, A 2021, _'How to split a file using Java'_, Admios 2021, viewed 10 October 2022, https://www.admios.com/blog/how-to-split-a-file-using-java
-  OpenJDK 2017, _'JEP 220: Modular Run-Time Images'_, Oracle Corporation, viewed 05 August 2020, https://openjdk.java.net/jeps/220
-  OpenJDK 2017, _'JEP 261: Module System'_, OpenJDK, Oracle Corporation, viewed 30 November 2020, https://openjdk.java.net/jeps/261
-  OpenJDK 2017, _'JEP 282: jlink: The Java Linker'_, Oracle Corporation, viewed 30 November 2020, https://openjdk.java.net/jeps/282
-  OpenJDK 2020, _'Java Microbenchmark Harness (JMH)'_', JMH Source Repository, viewed 27 November 2020, https://github.com/openjdk/jmh
-  Oracle 2020, _'Charset'_, java.nio.charset, Java 15 SE API, viewed 30 November 2020, https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/nio/charset/package-summary.html
-  Oracle 2020, _'FileSystem.getPathMatcher'_, java.nio.file, Java SE 15, viewed 20 November 2020, https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/nio/file/FileSystem.html
-  Oracle 2020, _'Java Development Kit Version 15 Tool Specifications'_, JDK Tool Specifications, JDK 15 Documentation, viewed 30 November 2020, https://docs.oracle.com/en/java/javase/15/docs/specs/man/index.html
-  Oracle 2020, _'Pattern'_, java.util.regex, Java SE 15, viewed 20 November 2020, https://docs.oracle.com/en/java/javase/15/docs/api/java.base/java/util/regex/Pattern.html
-  Oracle 2020, _'The jlink Command'_, Java Development Kit Version 15 Tool Specifications, viewed 30 November 2020, https://docs.oracle.com/en/java/javase/15/docs/specs/man/jlink.html
-  O'Sullivan, B 2016,  _'criterion: Robust, reliable performance measurement and analysis'_, Hackage, viewed 30 November 2020, https://hackage.haskell.org/package/criterion
-  Polar 2020, _'Polar Bookshelf: Read. Learn. Never Forget'_, viewed 30 November 2020, https://getpolarized.io/
-  Professional lorem ipsum generator for typographers, Generator for randomized typographic filler text, viewed 21 October 2022, https://generator.lorem-ipsum.info/_latin


----


That's it for the readme, anything else you may need to know just pick up a book and read it [_Polar Bookshelf_](https://getpolarized.io/). Thanks all. Bye.

