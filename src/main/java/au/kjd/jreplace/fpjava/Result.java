package au.kjd.jreplace.fpjava;

import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.IntSupplier;
import java.util.function.Supplier;
import java.util.stream.Stream;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import static au.kjd.jreplace.model.Nothing.NOTHING;


/*
 * Saumont, P-Y 2017, Listing 3.6 A Result that can handle Effects, Abstracting control structures,
 *                    Functional Programming in Java, chpt 3, p.65, Manning Publications,
 *                    viewed 08 October 2021, <https://www.manning.com/books/functional-programming-in-java>
 */
public abstract class Result<T> {

  public abstract boolean isSuccess();
  public abstract boolean isFailure();
  public abstract int resultAsInt(IntSupplier success, IntSupplier failure);
  public abstract T successValue();
  public abstract Exception failureValue();
  public abstract T orThrow();
  public abstract <U> Result<U> map(Function<T, U> f);
  public abstract <U> Result<U> flatMap(Function<T, Result<U>> f);
  public abstract Stream<T> stream();


  private static final class Success<T> extends Result<T> {

    private final T value;

    private Success(T value) { this.value = value; }

    @Contract(pure = true)
    @Override public boolean isSuccess() { return true; }

    @Contract(pure = true)
    @Override public boolean isFailure() { return false; }

    @Override public int resultAsInt(@NotNull IntSupplier success, IntSupplier failure) {  return success.getAsInt(); }

    @Contract(pure = true)
    @Override public T successValue() { return value; }

    @Contract(value = " -> fail", pure = true)
    @Override public RuntimeException failureValue() { throw new IllegalStateException("failureValue called on a Success instance"); }

    @Contract(pure = true)
    @Override public T orThrow() { return successValue(); }

    @Override public <U> Result<U> map(Function<T, U> f) {
      try { return success(f.apply(successValue())); }
      catch (Exception e) { return failure(e.getMessage(), e); }
    }

    @Override public <U> Result<U> flatMap(Function<T, Result<U>> f) {
      try { return f.apply(successValue()); }
      catch (Exception e) { return failure(e); }
    }

    @Contract(pure = true)
    @Override public @NotNull Stream<T> stream() { return Stream.of(value); }
  }


  private static final class Failure<T> extends Result<T> {

    private final RuntimeException exception;

    private Failure(String message) { this.exception = new IllegalStateException(message); }

    private Failure(RuntimeException e) { this.exception = e; }

    private Failure(Exception e) { this.exception = new IllegalStateException(e); }

    @Contract(pure = true)
    @Override public boolean isSuccess() { return false; }

    @Contract(pure = true)
    @Override public boolean isFailure() { return true; }

    @Override public int resultAsInt(IntSupplier success, @NotNull IntSupplier failure) { return failure.getAsInt(); }

    @Contract(value = " -> fail", pure = true)
    @Override public T successValue() { throw new IllegalStateException("successValue called on Failure instance"); }

    @Contract(pure = true)
    @Override public RuntimeException failureValue() { return this.exception; }

    @Contract(value = " -> fail", pure = true)
    @Override public T orThrow() { throw exception; }

    @Contract("_ -> new")
    @Override public <U> @NotNull Result<U> map(Function<T, U> f) { return failure(exception); }

    @Contract("_ -> new")
    @Override public <U> @NotNull Result<U> flatMap(Function<T, Result<U>> f) { return failure(exception.getMessage(), exception); }

    @Contract(pure = true)
    @Override public @NotNull Stream<T> stream() { return Stream.empty(); }
  }

  final public void ifPresent(Consumer<T> action) { if (isSuccess()) action.accept(this.successValue()); }

  @SuppressWarnings("unchecked")
  final public <R> R successOrNothing(Function<T, R> action) {
    if (isSuccess()) return action.apply(this.successValue());
    else return (R) NOTHING;
  }

  @Contract("_ -> this")
  final public <R> Result<T> onSuccess(Function<T, R> action) {
    if (isSuccess()) action.apply(this.successValue());
    return this;
  }

  @Contract(pure = true)
  public static <A, B> @NotNull Function<Result<A>, Result<B>> lift(Function<A, B> f) { return r -> r.map(f); }

  public static <T> Result<T> flatten(@NotNull Result<Result<T>> result) { return result.flatMap(t -> t); }

  @Contract("_ -> new")
  public static <T> @NotNull Result<T> success(T value) { return new Success<>(value); }

  @Contract("_ -> new")
  public static <T> @NotNull Result<T> success(@NotNull Supplier<T> value) { return new Success<>(value.get()); }

  @Contract("_ -> new")
  public static <T> @NotNull Result<T> failure(String message) { return new Failure<>(message); }

  @Contract("_ -> new")
  public static <T> @NotNull Result<T> failure(Exception e) { return new Failure<>(e); }

  @Contract("_, _ -> new")
  public static <T> @NotNull Result<T> failure(String message, Exception e) { return new Failure<>(new IllegalStateException(message, e)); }

}
