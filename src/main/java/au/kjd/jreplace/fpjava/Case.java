package au.kjd.jreplace.fpjava;

import au.kjd.jreplace.model.Nothing;
import au.kjd.jreplace.model.Pair;

import java.util.function.BooleanSupplier;
import java.util.function.Supplier;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

/* An abstraction over if...else control structures.
 *
 * Saumont, P-Y 2017, Listing 3.8 Matching conditions with the Case class, Abstracting control structures,
 *                    Functional Programming in Java, chpt 3, p.69, Manning Publications,
 *                    viewed 08 October 2021, <https://www.manning.com/books/functional-programming-in-java>
 */

public class Case extends Pair<BooleanSupplier, Supplier<Nothing>> {

  private Case(BooleanSupplier booleanSupplier, Supplier<Nothing> nothingSupplier) { super(booleanSupplier, nothingSupplier); }


  private static final class DefaultCase extends Case {

    private DefaultCase(BooleanSupplier booleanSupplier, Supplier<Nothing> nothingSupplier) { super(booleanSupplier, nothingSupplier); }
  }

  /* Java is a strictly evaluated language, the Supplier functional interface provides for lazy evaluation. */
  @Contract("_, _ -> new")
  public static @NotNull Case cause(BooleanSupplier condition, Supplier<Nothing> value) { return new Case(condition, value); }

  @Contract("_ -> new")
  public static @NotNull DefaultCase cause(Supplier<Nothing> value) { return new DefaultCase(() -> true, value); }

  /* Implementation places the default case first and evaluates it last. */
  public static void match(DefaultCase defaultCase, Case @NotNull ... causes) {

    for (Case aCase : causes) if (aCase.first().getAsBoolean()) { aCase.second().get(); return; }

    defaultCase.second().get();
  }

}
