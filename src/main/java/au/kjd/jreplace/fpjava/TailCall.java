package au.kjd.jreplace.fpjava;

import java.util.function.Function;
import java.util.function.Supplier;
import java.util.LinkedList;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;


/*
 * Saumont, P-Y 2017, Listing 4.1 The TailCall interface and its two implementations, Understanding corecursion and recursion,
 *                    Functional Programming in Java, chpt 4, pp.99-100, Manning Publications,
 *                    viewed 08 October 2021, <https://www.manning.com/books/functional-programming-in-java>
 */
public abstract class TailCall<T> {

  public abstract TailCall<T> resume();
  public abstract T eval();
  public abstract boolean isSuspend();

  @Contract(pure = true)
  private TailCall() { }


  private static final class Return<T> extends TailCall<T> {

    private final T t;

    private Return(T t) { this.t = t; }

    @Contract(pure = true)
    @Override public T eval() { return t; }

    @Contract(pure = true)
    @Override public boolean isSuspend() { return false; }

    @Contract(value = " -> fail", pure = true)
    @Override public TailCall<T> resume() { throw new IllegalStateException("Return has no resume"); }
  }


  private static final class Suspend<T> extends TailCall<T> {

    private final Supplier<TailCall<T>> resume;

    private Suspend(Supplier<TailCall<T>> resume) { this.resume = resume; }

    @Override public T eval() {
      TailCall<T> tailRec = this;
      while(tailRec.isSuspend()) { tailRec = tailRec.resume(); }
      return tailRec.eval();
    }

    @Contract(pure = true)
    @Override public boolean isSuspend() { return true; }

    @Override public TailCall<T> resume() { return resume.get(); }
  }


  @Contract("_ -> new")
  public static <T> @NotNull Return<T> ret(T t) { return new Return<>(t); }

  @Contract("_ -> new")
  public static <T> @NotNull Suspend<T> sus(Supplier<TailCall<T>> s) { return new Suspend<>(s); }

  /*
   * Implemented a zipWith function that felt unrewarding. So, picked up a book a read it.
   *
   * zipWith() {
   *  IntStream.range(0, tsize).mapToObj(index -> {
   *    var triple = listOfTriples.get(index);
   *    return new Tuple<>(triple.first(), triple.second(), triple.third(), list.get(index));
   *  }).collect(toCollection(LinkedList::new));
   * }
   *
   * Saumont, P-Y 2017, 8.3.1 Zipping and unzipping lists, Abstracting common List use cases,
   *                    Functional Programming in Java, chpt 8, pp.212-213, Manning Publications,
   *                    viewed 08 October 2021, <https://www.manning.com/books/functional-programming-in-java>
   */

  public static <A, B, C> LinkedList<C> zipWith(LinkedList<A> aList, LinkedList<B> bList, Function<A, Function<B, C>> fn) {

    return zipWithHelper(new LinkedList<>(), aList, bList, fn).eval();
  }

  private static <A, B, C> TailCall<LinkedList<C>> zipWithHelper(LinkedList<C> acc, @NotNull LinkedList<A> aList, LinkedList<B> bList, Function<A, Function<B, C>> fn) {

    return aList.isEmpty() || bList.isEmpty()
        ? ret(acc)
        : sus(() -> zipWithHelper((acc.add(fn.apply(aList.removeFirst()).apply(bList.removeFirst()))) ? acc : acc, aList, bList, fn));
  }

}
