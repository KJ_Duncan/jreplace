package au.kjd.jreplace.domain;

import au.kjd.jreplace.fpjava.Result;
import au.kjd.jreplace.handler.WalkerReplicateHandler;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Route;
import au.kjd.jreplace.model.Triple;
import au.kjd.jreplace.model.Tuple;

import java.io.IOException;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.Files;
import java.nio.file.FileVisitOption;
import java.nio.file.FileVisitor;
import java.nio.file.FileVisitResult;
import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.EnumSet;
import java.util.function.BiPredicate;
import java.util.LinkedList;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import static au.kjd.jreplace.exceptional.ExceptionalUtilities.results;
import static au.kjd.jreplace.model.Route.UC3;

// import java.util.logging.Logger;
// import static java.util.logging.Logger.getLogger;

/*
 * Walks a file tree rooted at a given starting file. The file tree
 * traversal is depth-first with the given FileVisitor invoked for
 * each file encountered. File tree traversal completes when all
 * accessible files in the tree have been visited, or a visit method
 * returns a result of TERMINATE.
 *
 * For each file encountered this method attempts to read its
 * BasicFileAttributes. If the file is not a directory then the
 * visitFile method is invoked with the file attributes.
 *
 * Where the file is a directory, and the directory could not be
 * opened, then the visitFileFailed method is invoked with the I/O
 * exception, after which, the file tree walk continues, by default,
 * at the next sibling of the directory.
 *
 * Where the directory is opened successfully, then the entries in
 * the directory, and their descendants are visited. When all entries
 * have been visited, or an I/O error occurs during iteration of the
 * directory, then the directory is closed and the visitor's
 * postVisitDirectory method is invoked.
 *
 * By default, symbolic links are not automatically followed by this
 * method. The maxDepth parameter is the maximum number of levels of
 * directories to visit. A value of 0 means that only the starting
 * file is visited.
 *
 * Oracle 2022, Files.walkFileTree, java.nio.file.Files, java se 19, viewed 23 November 2022,
 *             <https://docs.oracle.com/en/java/javase/19/docs/api/java.base/java/nio/file/Files.html#walkFileTree(java.nio.file.Path,java.util.Set,int,java.nio.file.FileVisitor)>
 */

public final class Walker extends WalkerReplicateHandler {

  // private static final Logger logger = getLogger(Walker.class.getSimpleName());

  private static final int ZERO = 0;
  private final Path startingDirectory;
  private final int traversalDepth;
  private final PathMatcher pathMatcher;


  public Walker(final @NotNull Tuple<Route, Path, Integer, PathMatcher> tuple) {
    this.startingDirectory = tuple.second();
    this.traversalDepth    = tuple.third();
    this.pathMatcher       = tuple.fourth();
  }

  /* WalkerReplicateHandler call point */
  @Contract(" -> new")
  public @NotNull Pair<Route, Result<Triple<LinkedList<Path>, LinkedList<Path>, LinkedList<Exception>>>> walker() {

    return new Pair<>(UC3, obtainResult());
  }

  private @NotNull Result<Triple<LinkedList<Path>, LinkedList<Path>, LinkedList<Exception>>> obtainResult() {

    return results(() -> {

      var traversal = new WalkerTraverse(pathMatcher);

      /* Traverse the file tree collecting files without following symbolic links */
      Files.walkFileTree(startingDirectory(), EnumSet.noneOf(FileVisitOption.class), traversalDepth(), traversal);

      return new Triple<>(traversal.successes, traversal.failures, traversal.exceptions);

    });
  }

  @Contract(pure = true)
  private Path startingDirectory() { return startingDirectory; }

  @Contract(pure = true)
  private int traversalDepth() { return traversalDepth; }


  private static final class WalkerTraverse implements FileVisitor<Path> {

    private final LinkedList<Exception> exceptions = new LinkedList<>();
    private final LinkedList<Path> failures        = new LinkedList<>();
    private final LinkedList<Path> successes       = new LinkedList<>();

    /* --glob=pattern or --regex=expression match a file name. */
    private final PathMatcher pathMatcher;

    private WalkerTraverse(final PathMatcher pathMatcher) {
      this.pathMatcher = pathMatcher;
    }

    /* This method returns CONTINUE, entries in the directory are visited. */
    @Contract(pure = true)
    @Override public FileVisitResult preVisitDirectory(Path dir, BasicFileAttributes attributes) {

      return FileVisitResult.CONTINUE;
    }

    /* A file in a directory. */
    @Override public FileVisitResult visitFile(Path source, BasicFileAttributes attributes) {

      if (predicate().test(source, attributes)) successes.add(source);

      return FileVisitResult.CONTINUE;
    }

    /*
     * This method is invoked if the file's attributes could not be read,
     * the file is a directory that could not be opened, and other reasons.
     */
    @Override public FileVisitResult visitFileFailed(Path source, IOException exception) {

      failures.add(source);

      exceptions.add(exception);

      return FileVisitResult.CONTINUE;
    }

    @Contract(pure = true)
    @Override public FileVisitResult postVisitDirectory(Path dir, IOException exception) {

      return FileVisitResult.CONTINUE;
    }

    /* applys the given predicate to each file visited */
    @Contract(pure = true)
    private @NotNull BiPredicate<Path, BasicFileAttributes> predicate() {

      return (source, attributes) -> {

        /* matches the users --glob or --regex pattern */
        if (syntaxAndPattern(source)) return hasAttributes(attributes);

        else return false;
      };
    }

    /*
     * Tells if given path matches this matcher's pattern
     * command line --glob=pattern or --regex=expression
     */
    private boolean syntaxAndPattern(@NotNull Path source) { return pathMatcher().matches(source.getFileName()); }

    @Contract(pure = true)
    private PathMatcher pathMatcher() { return pathMatcher; }

    /*
     * For each file encountered this method attempts to read the file is
     * a regular file with opaque content and the files not empty.
     */
     private boolean hasAttributes(@NotNull BasicFileAttributes attributes) {

      return attributes.isRegularFile() && attributes.size() > ZERO;
    }

  }

}
