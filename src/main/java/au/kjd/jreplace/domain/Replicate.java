package au.kjd.jreplace.domain;

import au.kjd.jreplace.exceptional.ExceptionalFunction;
import au.kjd.jreplace.fpjava.Result;
import au.kjd.jreplace.handler.WalkerReplicateHandler;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Route;
import au.kjd.jreplace.model.Triple;

import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.stream.Stream;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import static au.kjd.jreplace.exceptional.ExceptionalUtilities.results;
import static au.kjd.jreplace.model.Route.UC4;

import static java.nio.channels.FileChannel.open;
import static java.nio.file.Files.createTempDirectory;
import static java.nio.file.Files.createTempFile;
import static java.nio.file.Files.deleteIfExists;
import static java.nio.file.Files.size;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.WRITE;

// import java.util.logging.Logger;
// import static java.util.logging.Logger.getLogger;


/*
 * Clones a source file to temporary file.
 *
 * A file channel is a SeekableByteChannel that is connected to a
 * file. It has a current position within its file which can be both
 * queried and modified.
 *
 * Oracle 2022, FileChannel, java.nio.channels, viewed 23 November 2022,
 *              <https://docs.oracle.com/en/java/javase/19/docs/api/java.base/java/nio/channels/FileChannel.html>
 */

public final class Replicate extends WalkerReplicateHandler {

  // private static final Logger logger = getLogger(Replicate.class.getSimpleName());

  /*
   * Creates a new directory in the default temporary-file directory,
   * using the given prefix to generate its name.
   *
   * e.g. /tmp/jreplaceXXXXXXXXXX where X is a random number.
   *
   * Oracle 2022, Files.createTempDirectory, java.nio.file, viewed 23 November 2022,
   *              <https://docs.oracle.com/en/java/javase/19/docs/api/java.base/java/nio/file/Files.html#createTempDirectory(java.lang.String,java.nio.file.attribute.FileAttribute...)>
   */
  private static final Result<Path> jreplaceTempDirectory = results(() -> createTempDirectory("jreplace"));

  private final Path source;


  public Replicate(final @NotNull Pair<Route, Path> pair) {
    this.source = pair.second();
  }

  /* WalkerReplicateHandler call point */
  @Contract(" -> new")
  public @NotNull Pair<Route, Result<Triple<LinkedList<Pair<Path, Path>>, LinkedList<Path>, LinkedList<Exception>>>> replicate() {

    return new Pair<>(UC4, obtainResult());
  }

  /*
   * Each source file has a target/copy made in the temporary directory.
   *
   * LinkedList<Pair<Path, Path>>
   */
  private @NotNull Result<Triple<LinkedList<Pair<Path, Path>>, LinkedList<Path>, LinkedList<Exception>>> obtainResult() {

    return results(() -> {

      var successes  = new LinkedList<Pair<Path, Path>>();
      var failures   = new LinkedList<Path>();
      var exceptions = new LinkedList<Exception>();

      Result<Path> target = duplicateSource();

      if (target.isFailure()) { failures.add(source); exceptions.add(target.failureValue()); }

      else {

        Pair<Path, Path> pair = new Pair<>(target.successValue(), source);

        successes.add(pair);
      }

      return new Triple<>(successes, failures, exceptions);
    });
  }

  /*
   * A target channel for writing that transfers bytes from the source
   * channel's file to the given target writable byte channel. Copies a
   * source files content into a temporay target file.
   */
  private @NotNull Result<Path> duplicateSource() {

    return results(() -> {

      Path target = getEmptyFile(source).orThrow();

      try (FileChannel channel = open(target, WRITE)) { channel.transferFrom(open(source, READ), 0, size(source)); }

      return target;
    });
  }

  /*
   * Creates an empty file in the users default temporary-file directory,
   *
   * given a source file name: yourfile.txt,
   * a target files name: yourfile.txtXXXXXXXXX.tmp
   * where X is a random number.
   */
  @Contract(pure = true)
  private static @NotNull ExceptionalFunction<Path, Path> getTempFile() {

    return target -> createTempFile(jreplaceTempDirectory.orThrow(), target.getFileName().toString(), ".tmp");
  }

  /*
   * A shutdown hook on the jvm's exit calls this cleanup function to
   * delete the jreplaceXXXXXXXXXX folder and its contents if any in
   * the users temporary directory.
   *
   * see: jreplace/controller/Trailer.java
   */
  public static void deleteJreplaceTempDirectory() {

    results(() -> {

      Path tempdir = jreplaceTempDirectory.orThrow();

      try (Stream<Path> stream = Files.list(tempdir)) { for (Path path : stream.toList()) deleteTempFile(path); }

      return deleteTempFile(tempdir);
    });
  }

  /* Cleans up temporary files no longer needed by jreplace */
  static @NotNull Result<Boolean> deleteTempFile(Path path) { return results(() -> deleteIfExists(path)); }

  /* Returns an empty file located in the users temporary directory */
  static @NotNull Result<Path> getEmptyFile(Path target) { return results(() -> getTempFile().apply(target)); }

}
