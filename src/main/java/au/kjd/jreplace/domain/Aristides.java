package au.kjd.jreplace.domain;

import au.kjd.jreplace.fpjava.Result;
import au.kjd.jreplace.handler.atjhandlers.TheJustHandler;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Route;
import au.kjd.jreplace.model.Triple;

import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.Set;
import java.util.stream.Stream;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import static au.kjd.jreplace.domain.Replicate.deleteTempFile;
import static au.kjd.jreplace.domain.Replicate.getEmptyFile;
import static au.kjd.jreplace.exceptional.ExceptionalUtilities.results;
import static au.kjd.jreplace.model.Route.UC6;

import static java.nio.file.Files.size;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.WRITE;
import static java.util.stream.LongStream.range;

// import java.util.logging.Logger;
// import static java.util.logging.Logger.getLogger;


/*
 * Split a files content on given megabytes.
 *
 * Melendez, A 2021, How to split a file using Java, Admios 2021, viewed 10 October 2022,
 *                   <https://www.admios.com/blog/how-to-split-a-file-using-java>
 */

public final class Aristides extends TheJustHandler {

  // private static final Logger logger = getLogger(Aristides.class.getSimpleName());

  private final LinkedList<Pair<Path, Path>> targetsAndSources;
  private final long megabytes;

  /* target and source file with megabytes to spilt content on */
  public Aristides(final @NotNull Triple<Route, LinkedList<Pair<Path, Path>>, Long> triple) {
    this.targetsAndSources = triple.second();
    this.megabytes         = triple.third();
  }

  /* TheJustHandler call point */
  @Contract(" -> new")
  public @NotNull Pair<Route, Result<Triple<LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>> aristides() {

    return new Pair<>(UC6, obtainResult());
  }

  /* Successes list contains one or more split files for each source file. */
  private @NotNull Result<Triple<LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>, LinkedList<Path>, LinkedList<Exception>>> obtainResult() {

    return results(() -> {

      var successes  = new LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>();
      var failures   = new LinkedList<Path>();
      var exceptions = new LinkedList<Exception>();

      /* A terminal call on the stream. */
      aristidesFractionalise().forEach(aristides -> {

        if (aristides.splitFilesWithSource.isSuccess()) {

          Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path> pair = aristides.splitFilesWithSource.successValue();

          if (pair.first().isEmpty()) failures.add(pair.second());

          else successes.add(pair);
        }
        else exceptions.add(aristides.splitFilesWithSource.failureValue());
      });

      return new Triple<>(successes, failures, exceptions);
    });
  }

  /* Wrap target and source file in an instance off AristidesFractionalise. */
  private Stream<AristidesFractionalise> aristidesFractionalise() {

    return targetsAndSources
        .stream()
        .map(pair -> (megabytes > 0) ? new AristidesFractionalise(pair, megabytes) : new AristidesFractionalise(pair));
  }


  private static final class AristidesFractionalise {

    // Pair<list of split files with their positional pointers, source file>
    private final Result<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>> splitFilesWithSource;
    private Pair<Path, Path> targetAndSource;
    private FileChannel sourceChannel;
    private Path target;
    private long bytesPerSplit;
    private long numberOfSplits;
    private long remainingBytes;
    private long sourceSize;

    private AristidesFractionalise(final Pair<Path, Path> pair) {

      this.splitFilesWithSource = results(() -> {

        this.targetAndSource = pair;
        this.target          = targetAndSource.first();
        this.sourceSize      = size(target);

        long times = 1L;
        if (isGreaterThanIntegerMaxValue()) { times = nTimesGreater(); }

        this.bytesPerSplit  = sourceSize / times;
        this.numberOfSplits = times;
        this.remainingBytes = 0L;

        this.sourceChannel  = FileChannel.open(target, Set.of(READ));

        Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path> splitFilesAndSource = generateFileContent();

        sourceChannel.close();
        deleteTempFile(target);

        return splitFilesAndSource;
      });
    }

    private AristidesFractionalise(final Pair<Path, Path> pair, final long megaBytes) {

      /*
       * An upper bound is dependent on the
       * ByteBuffer maximum capacity Integer.MAX_VALUE
       * 2047 = Integer.MAX_VALUE / 1024 / 1024
       */
      assert 0 < megaBytes && megaBytes < 2048;

      this.splitFilesWithSource = results(() -> {

        this.targetAndSource = pair;
        this.target          = targetAndSource.first();
        this.sourceSize      = size(target);

        this.bytesPerSplit   = 1024L * 1024L * megaBytes;
        this.numberOfSplits  = sourceSize / bytesPerSplit;
        this.remainingBytes  = sourceSize % bytesPerSplit;

        this.sourceChannel   = FileChannel.open(target, Set.of(READ));

        Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path> splitFilesAndSource = generateFileContent();

        sourceChannel.close();
        deleteTempFile(target);

        return splitFilesAndSource;
      });
    }

    /* Byte buffer capacity limit */
    @Contract(pure = true)
    private boolean isGreaterThanIntegerMaxValue() { return Integer.MAX_VALUE < sourceSize; }

    /* Long value is n times greater than integer max value */
    @Contract(pure = true)
    private long nTimesGreater() {

      long totalsize       = sourceSize;
      long integermaxvalue = (long) Integer.MAX_VALUE;
      long times           = 0L;

      do { times++; totalsize -= integermaxvalue; } while (totalsize > 0);

      return times;
    }

    /* Return a list of split files associated with each source file. */
    @Contract(" -> new")
    private @NotNull Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path> generateFileContent() {

      Result<LinkedList<Pair<Path, LinkedList<Long>>>> splitList = results(() -> {

        var splits = new LinkedList<Pair<Path, LinkedList<Long>>>();

        Result<LinkedList<Pair<ByteBuffer, LinkedList<Long>>>> result = allocateBuffer();

        if (result.isSuccess()) {

          /* Operating on a stream. */
          traverseChannelBuffers(result.successValue()).forEach(channel -> {

            /*
             * If one split file results in a failure
             * delete all splits associated with that
             * source file as a partial file is useless.
             */
            if (channel.isFailure()) {

              while (!splits.isEmpty()) {

                Pair<Path, LinkedList<Long>> pair = splits.removeFirst();
                deleteTempFile(pair.first());
                pair.second().clear();

              }

              /* Used to terminate the stream. */
              throw new RuntimeException("A split file has resulted in a failure");
            }

            else splits.add(channel.successValue());
          });
        }

        return splits;
      });

      if (splitList.isFailure()) return new Pair<>(new LinkedList<>(), this.targetAndSource.second());

      else return new Pair<>(splitList.successValue(), this.targetAndSource.second());
    }

    /* Creates a byte buffer to read the source channels data into. */
    private @NotNull Result<LinkedList<Pair<ByteBuffer, LinkedList<Long>>>> allocateBuffer() {

      return results(() -> {

        var linkedList = new LinkedList<Pair<ByteBuffer, LinkedList<Long>>>();
        var location   = new LinkedList<Long>();

        int capacity = (int) bytesPerSplit;

        range(0, numberOfSplits).forEach(position -> {

          /* Maximum capacity Integer.MAX_VALUE */
          ByteBuffer buffer = ByteBuffer.allocate(capacity);

          long pointer = bytesPerSplit * position;

          location.add(position);

          Pair<ByteBuffer, LinkedList<Long>> pair = readIntoBuffer(location, buffer, pointer);

          linkedList.add(pair);
        });

        if (0 < remainingBytes) {

          ByteBuffer buffer = ByteBuffer.allocate((int) remainingBytes);

          long pointer = bytesPerSplit * numberOfSplits;

          location.addLast((long) location.size());

          Pair<ByteBuffer, LinkedList<Long>> pair = readIntoBuffer(location, buffer, pointer);

          linkedList.add(pair);
        }

        return linkedList;
      });
    }

    @Contract("_, _, _ -> new")
    private @NotNull Pair<ByteBuffer, LinkedList<Long>> readIntoBuffer(LinkedList<Long> location, ByteBuffer buffer, long pointer) {

      try { sourceChannel.read(buffer, pointer); } catch (Exception e) { throw new RuntimeException(e); }

      return new Pair<>(buffer, location);
    }

    private Stream<Result<Pair<Path, LinkedList<Long>>>> traverseChannelBuffers(@NotNull LinkedList<Pair<ByteBuffer, LinkedList<Long>>> channelBufferPointerList) {

      return channelBufferPointerList
          .stream()
          .map(this::writeIntoChannel);
    }

    private @NotNull Result<Pair<Path, LinkedList<Long>>> writeIntoChannel(@NotNull Pair<ByteBuffer, LinkedList<Long>> channelBufferPointer) {

      return results(() -> {

        Path empty = getEmptyFile(target).orThrow();

        try (FileChannel destChannel = FileChannel.open(empty, Set.of(WRITE))) { destChannel.write(channelBufferPointer.first().flip(), 0L); }

        return new Pair<>(empty, channelBufferPointer.second());
      });
    }
  }

}
