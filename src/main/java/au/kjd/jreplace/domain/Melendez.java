package au.kjd.jreplace.domain;

import au.kjd.jreplace.fpjava.Result;
import au.kjd.jreplace.handler.atjhandlers.TheJustHandler;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.RegexFrom;
import au.kjd.jreplace.model.RegexTo;
import au.kjd.jreplace.model.Route;
import au.kjd.jreplace.model.Scan;
import au.kjd.jreplace.model.Triple;

import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.util.concurrent.atomic.LongAdder;
import java.util.LinkedList;
import java.util.Objects;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import static au.kjd.jreplace.domain.Replicate.deleteTempFile;
import static au.kjd.jreplace.domain.Replicate.getEmptyFile;
import static au.kjd.jreplace.exceptional.ExceptionalUtilities.results;
import static au.kjd.jreplace.model.Route.UC8;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.nio.channels.FileChannel.open;
import static java.nio.file.Files.size;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.WRITE;

// import java.util.logging.Logger;
// import static java.util.logging.Logger.getLogger;


/*
 * Consolidates a list of split files and writes to the last
 * temporary file the endpoint channel.
 *
 * Melendez, A 2021, How to split a file using Java, Admios 2021,
 *                   viewed 10 October 2022, <https://www.admios.com/blog/how-to-split-a-file-using-java>
 */

public final class Melendez extends TheJustHandler {

  // private static final Logger logger = getLogger(Melendez.class.getSimpleName());

  /*
   * The inner datastructures Triple<Path, Long, Scan> holds:
   *   triple.first=target file,
   *   triple.second=file positional pointer,
   *   triple.third=scan of regex 'from' and regex 'to' count
   *
   * The files positional pointer is the location to begin inserting text
   *
   * Where Pair<List<Triple<>>, Path> holds the source files path location
   */
  private final LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>> targetPointerScanSource;

  /* A cumulative total of previous split file sizes for a positional file pointer. */
  private long previousSplitFileSize = 0L;

  public Melendez(final @NotNull Pair<Route, LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>> pair) {
    this.targetPointerScanSource = pair.second();
  }

  /* TheJustHandler call point */
  @Contract(" -> new")
  public @NotNull Pair<Route, Result<Pair<LinkedList<Triple<Path, Scan, Path>>, LinkedList<Object>>>> melendez() {

    return new Pair<>(UC8, obtainResult());
  }

  private @NotNull Result<Pair<LinkedList<Triple<Path, Scan, Path>>, LinkedList<Object>>> obtainResult() {

    return results(() -> {

      var successes = new LinkedList<Triple<Path, Scan, Path>>();
      var failures  = new LinkedList<>();

      do {

        Pair<LinkedList<Triple<Path, Long, Scan>>, Path> pair = targetPointerScanSource.removeFirst();

        Result<Boolean> result = openChannel(pair, successes, failures);

        if (result.isFailure()) failures.add(result.failureValue());

      } while (!targetPointerScanSource.isEmpty());

      return new Pair<>(successes, failures);
    });
  }

  /* Provide the final temporary file to consolidate a list of split files. */
  private @NotNull Result<Boolean> openChannel(Pair<LinkedList<Triple<Path, Long, Scan>>, Path> pair, LinkedList<Triple<Path, Scan, Path>> successes, LinkedList<Object> failures) {

    return results(() -> {

      LinkedList<Triple<Path, Long, Scan>> triples = pair.first();

      Path file = getEmptyFile(Objects.requireNonNull(triples.peekFirst()).first()).orThrow();

      FileChannel endpoint = open(file, WRITE);

      Pair<Boolean, Scan> pairBoolScan = bytesWritten(triples, endpoint);

      /* Delete the file if its incomplete as its worthless. */
      if (pairBoolScan.first()) { failures.add(pair.second()); deleteTempFile(file); }

      else successes.add(new Triple<>(file, pairBoolScan.second(), pair.second()));

      endpoint.close();

      return TRUE;
    });
  }

  /* Consolidate the list of split files into file channel endpoint. */
  @Contract("_, _ -> new")
  private @NotNull Pair<Boolean, Scan> bytesWritten(@NotNull LinkedList<Triple<Path, Long, Scan>> triples, FileChannel endpoint) {

    Scan scan = new Scan(new RegexFrom(new LongAdder()), new RegexTo(new LongAdder()));
    Boolean failed = FALSE;

    do {

      Triple<Path, Long, Scan> triple = triples.removeFirst();

      long from = triple.third().regexFrom().longValue().sumThenReset();
      long to   = triple.third().regexTo().longValue().sumThenReset();

      scan.regexFrom().longValue().add(from);
      scan.regexTo().longValue().add(to);

      Result<Long> result = channelTransfer(triple, endpoint);

      /* No data has been written into endpoint channel. */
      if (result.isFailure() || result.successValue() == 0) { failed = TRUE; break; }

      from   = 0L;
      to     = 0L;

    } while (!triples.isEmpty());

    return new Pair<>(failed, scan);
  }

  /*
   * Endpoint channel for transferring in the processed files
   * alterations at a specified file position given by the previous
   * split file size.
   */
  private @NotNull Result<Long> channelTransfer(Triple<Path, Long, Scan> triple, FileChannel endpoint) {

    return results(() -> {

      this.previousSplitFileSize = (0L == triple.second()) ? 0L : this.previousSplitFileSize;

      long size = size(triple.first());

      long bytes = endpoint.transferFrom(open(triple.first(), READ), this.previousSplitFileSize, size);

      /* NOTE: a negative one ensures the removal off the '\n' character when appending to the endpoint file */
      this.previousSplitFileSize += (size - 1);

      deleteTempFile(triple.first());

      size = 0L;

      /* The number of bytes, possibly zero, that were actually transferred. */
      return bytes;
    });
  }

}
