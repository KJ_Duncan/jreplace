package au.kjd.jreplace.domain;

import au.kjd.jreplace.fpjava.Result;
import au.kjd.jreplace.handler.InterchangeEmissionMOTHandler;
import au.kjd.jreplace.model.Patterns;
import au.kjd.jreplace.model.Route;
import au.kjd.jreplace.model.Scan;
import au.kjd.jreplace.model.Tuple;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.Optional;

import org.jetbrains.annotations.NotNull;

import static au.kjd.jreplace.exceptional.ExceptionalUtilities.results;

import static java.lang.String.format;
import static java.lang.System.out;
import static java.nio.file.Files.writeString;
import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.WRITE;

// import java.util.logging.Logger;
// import static java.util.logging.Logger.getLogger;

/*
 * Writes to standard output or to users output file.
 *
 * Example output:
 *
 *  > 2958636  (Java|java) -> tzez    2958636  -> level_1/javaConstants01_288MB.kak    -> moved: true
 *  > 725112   (Java|java) -> tzez    725112   -> level_2/javaConstants02_71MB.kak     -> moved: true
 *  > 26856    (Java|java) -> tzez    26856    -> level_2/javaConstants02_2.7MB.kak    -> moved: true
 *  > 26856    (Java|java) -> tzez    26856    -> level_3/javaConstants03_2.7MB.kak    -> moved: true
 *  > 134280   (Java|java) -> tzez    134280   -> level_4/javaConstants04_14MB.kak     -> moved: true
 *  > 2958636  (Java|java) -> tzez    2958636  -> level_4/javaConstants04_288MB.kak    -> moved: true
 *  > 11834542 (Java|java) -> tzez    11834542 -> level_4/javaConstants04_1152MB.kak   -> moved: true
 *  > 134280   (Java|java) -> tzez    134280   -> level_3/javaConstants03_14MB.kak     -> moved: true
 *  > 23669082 (Java|java) -> tzez    23669082 -> level_2/javaConstants02_2304MB.kak   -> moved: true
 *  > 26856    (Java|java) -> tzez    26856    -> level_1/javaConstants01_2.7MB.kak    -> moved: true
 *  > 11834542 (Java|java) -> tzez    11834542 -> level_1/javaConstants01_1152MB.kak   -> moved: true
 */

public final class Emission extends InterchangeEmissionMOTHandler {

  // private static final Logger logger = getLogger(Emission.class.getSimpleName());

  private static final String FORMAT = "%-8d %-10s -> %-8s %-8d -> %-36s -> moved: %s%n";
  private final LinkedList<Tuple<Path, Scan, Path, Result<Path>>> targetScanSourceMoveResult;
  private final Patterns patterns;
  private final Optional<Path> output;

  /* where outputPresent true = 1, false = 0 */
  private final int outputPresent;

  /*
   * Tuple<Route, List<>, Patterns, Optional<Path>>
   *   tuple.third is the compiled regular expression patterns --to and --from
   *   tuple.fourth is an optional path which contains --output=file
   *
   * List<Tuple<Path, Scan, Path, Result<Path>>>
   *   tuple.first is the target file,
   *   tuple.second scan of regex 'from' and regex 'to' count,
   *   tuple.third the source file,
   *   tuple.fourth result of move file
   */
  public Emission(final @NotNull Tuple<Route, LinkedList<Tuple<Path, Scan, Path, Result<Path>>>, Patterns, Optional<Path>> tuple) {
    this.targetScanSourceMoveResult = tuple.second();
    this.patterns                   = tuple.third();
    this.output                     = tuple.fourth();
    this.outputPresent              = (tuple.fourth().isPresent()) ? 1 : 0;
  }

  /* InterchangeEmissionHandler call point */
  public void emission() {

    do {

      Tuple<Path, Scan, Path, Result<Path>> tuple = targetScanSourceMoveResult.removeFirst();

      Scan scan          = tuple.second();
      Path source        = tuple.third();
      Result<Path> moved = tuple.fourth();

      selectStdout(scan, source, moved);

    } while (!targetScanSourceMoveResult.isEmpty());

  }

  /* Where outputPresent: false = 0, true = 1 */
  private void selectStdout(Scan scan, Path source, Result<Path> moved) {

    switch (outputPresent) {

      case 0 -> stdout(scan, source, moved, patterns);

      case 1 -> stdout(scan, source, moved, patterns, output.get());

    };
  }

  /*
   * Standard output to console
   * Liang, Y-D 2015, 4.6 Formatting Console Output, Introduction to Java Programming, Chapter 4, pp.145-148.
   */
  private static void stdout(@NotNull Scan scan, @NotNull Path source, @NotNull Result<Path> moved, @NotNull Patterns patterns) {

    final long countRegexTo   = scan.regexTo().longValue().sumThenReset();
    final long countRegexFrom = scan.regexFrom().longValue().sumThenReset();
    final int nameCount       = source.getNameCount();

    out.printf(FORMAT,
        countRegexFrom,
        patterns.getFrom(),
        patterns.getTo(),
        countRegexFrom + countRegexTo,
        (nameCount > 1) ? source.subpath(nameCount - 2, nameCount) : source.getFileName(),
        (moved.isSuccess()) ? true : moved.failureValue());
  }

  /* The results of jreplace the command line flag --output=file. */
  private static void stdout(@NotNull Scan scan, @NotNull Path source, @NotNull Result<Path> moved, @NotNull Patterns patterns, @NotNull Path output) {

    final long countRegexTo   = scan.regexTo().longValue().sumThenReset();
    final long countRegexFrom = scan.regexFrom().longValue().sumThenReset();
    final int nameCount       = source.getNameCount();

    results(() ->
        writeString(output,
          format(FORMAT,
            countRegexFrom,
            patterns.getFrom(),
            patterns.getTo(),
            countRegexFrom + countRegexTo,
            (nameCount > 1) ? source.subpath(nameCount - 2, nameCount) : source.getFileName(),
            (moved.isSuccess()) ? true : moved.failureValue()),
        CREATE, WRITE, APPEND)
    );
  }

  /* A page break with local date and time output.ifPresent(Emission::stdout) */
  private static void stdout(Path output) { results(() -> writeString(output, LocalDateTime.now().toString(), WRITE, APPEND)); }

}
