package au.kjd.jreplace.domain;

import au.kjd.jreplace.fpjava.Result;
import au.kjd.jreplace.handler.atjhandlers.AmanuensisHandler;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Patterns;
import au.kjd.jreplace.model.Route;
import au.kjd.jreplace.model.Triple;

import java.lang.String;
import java.nio.file.Path;
import java.util.LinkedList;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.stream.Stream;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import static au.kjd.jreplace.domain.Replicate.deleteTempFile;
import static au.kjd.jreplace.exceptional.ExceptionalUtilities.results;
import static au.kjd.jreplace.fpjava.TailCall.zipWith;
import static au.kjd.jreplace.model.Route.UC5;

import static java.lang.Boolean.FALSE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.Files.lines;
import static java.util.stream.Collectors.toCollection;

// import java.util.logging.Logger;
// import static java.util.logging.Logger.getLogger;

/*
 * Streams are lazy; computation on the source data is only performed
 * when the terminal operation is initiated, and source elements are
 * consumed only as needed.
 *
 * AnyMatch returns whether any elements of this stream match the
 * provided predicate. May not evaluate the predicate on all elements
 * if not necessary for determining the result. If the stream is
 * empty then false is returned and the predicate is not evaluated.
 *
 * Oracle 2022, Stream.anyMatch, java.util.stream, viewed 24 November 2022,
 *              <https://docs.oracle.com/en/java/javase/19/docs/api/java.base/java/util/stream/Stream.html#anyMatch(java.util.function.Predicate)>
 */

public final class AmanuensisMatch extends AmanuensisHandler {

  // private static final Logger logger = getLogger(AmanuensisMatch.class.getSimpleName());

  private final LinkedList<Pair<Path, Path>> targetAndSource;
  private final Matcher matchRegexFrom;

  public AmanuensisMatch(final @NotNull Triple<Route, LinkedList<Pair<Path, Path>>, Patterns> triple) {
    this.targetAndSource = triple.second();
    this.matchRegexFrom  = triple.third().getRegexFrom().matcher("");
  }

  /* AmanuensisHandler call point */
  @Contract(" -> new")
  public @NotNull Pair<Route, Result<Triple<LinkedList<Pair<Path, Path>>, LinkedList<Path>, LinkedList<Exception>>>> amanuensisMatch() {

    return new Pair<>(UC5, obtainResult());
  }

  /*
   * Pair<Path, Path>
   *   pair.first target file
   *   pair.second source file
   */
  private @NotNull Result<Triple<LinkedList<Pair<Path, Path>>, LinkedList<Path>, LinkedList<Exception>>> obtainResult() {

    return results(() -> {

      var successes  = new LinkedList<Pair<Path, Path>>();
      var failures   = new LinkedList<Path>();
      var exceptions = new LinkedList<Exception>();

      LinkedList<Result<Boolean>> list = checkForAnyMatches();

      /* Files.lines has thrown an exception resulting in failure */
      if (Objects.requireNonNull(list.peekFirst()).isFailure()) {

        exceptions.add(list.removeFirst().failureValue());

        return new Triple<>(successes, failures, exceptions);
      }

      LinkedList<Triple<Path, Path, Boolean>> triples = zipAmanuensisMatch(list);

      do {

        Triple<Path, Path, Boolean> triple = triples.removeFirst();

        /* triple third is false no matches found in file, not a failure case */
        if (triple.third()) {

          Pair<Path, Path> pair = new Pair<>(triple.first(), triple.second());
          successes.add(pair);
        }

        /* failures are files where no matches have been found */
        else { failures.add(triple.second()); deleteTempFile(triple.first()); }

      } while (!triples.isEmpty());

      return new Triple<>(successes, failures, exceptions);
    });
  }

  /* Collect all files that match the users regex 'from' pattern. */
  private LinkedList<Result<Boolean>> checkForAnyMatches() {

    return targetAndSource
        .stream()
        .map(pair -> processElements(pair.first()))
        .collect(toCollection(LinkedList::new));
  }

  /*
   * Read lines from a file as a stream decoded into the utf-8 charset.
   * If any match returns true start processing the file for regex
   * 'from' matches.
   */
  private @NotNull Result<Boolean> processElements(Path target) {

    return results(() -> {

      try (Stream<String> stream = lines(target, UTF_8)) { return stream.anyMatch(this::regexFromFound); }

    });
  }

  /* Any pattern matching the users regex 'from' found in the line of text. */
  private boolean regexFromFound(String line) { return matchRegexFrom.reset(line).find(); }

  /* bools.successValue() is giving back true or false that either a pattern match was found in file or not. */
  private LinkedList<Triple<Path, Path, Boolean>> zipAmanuensisMatch(LinkedList<Result<Boolean>> bools) {

    return zipWith(targetAndSource, bools, a -> b -> new Triple<>(a.first(), a.second(), (b.isSuccess()) ? b.successValue() : FALSE));
  }

}
