package au.kjd.jreplace.domain;

import au.kjd.jreplace.handler.InterchangeEmissionMOTHandler;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Route;

import java.nio.file.Path;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.regex.Pattern;
import java.util.StringJoiner;

import org.jetbrains.annotations.NotNull;

import static au.kjd.jreplace.exceptional.ExceptionalUtilities.results;
import static java.lang.String.format;
import static java.nio.file.Files.writeString;
import static java.nio.file.StandardOpenOption.APPEND;
import static java.nio.file.StandardOpenOption.CREATE;
import static java.nio.file.StandardOpenOption.WRITE;

// import java.util.logging.FileHandler;
// import java.util.logging.Handler;
// import java.util.logging.Level;
// import java.util.logging.SimpleFormatter;
// import java.util.logging.Logger;
// import static java.util.logging.Logger.getLogger;

/*
 * The FileHandler can either write to a specified file, or it can
 * write to a rotating set of files.
 *
 * For a rotating set of files, as each file reaches a given size
 * limit, it is closed, rotated out, and a new file opened.
 * Successively older files are named by adding "0", "1", "2", etc.
 * into the base filename.
 *
 * Oracle 2022, FileHandler, java.util.logging, viewed 23 November 2022,
 *              <https://docs.oracle.com/en/java/javase/19/docs/api/java.logging/java/util/logging/FileHandler.html>
 *
 * The Orwell Foundation 2022, Nineteen Eighty-Four, viewed 20 November 2022,
 *                             <https://www.orwellfoundation.com/the-orwell-foundation/orwell/books-by-orwell/nineteen-eighty-four/>
 */

public final class Minitru extends InterchangeEmissionMOTHandler {

  // private static final Logger logger = getLogger(Minitru.class.getSimpleName());

  private static final Path LOGFILE = Path.of("jreplace-tourism.log");

  /*
   * FileHandler
   *  A handler that writes formatted log records either to a single file, or to a set of rotating log files.
   *
   * LogManager.getLogManager().readConfiguration(Minitru.class.getResourceAsStream("/logging.properties"));
   *  A set of logging control properties read from the configuration file.
   *
   * SimpleFormatter
   *  Writes brief "human-readable" summaries of log records.
   *
   * Native Executable
   *  If you require additional logging handlers, you must register the corresponding classes for reflection.
   *  For example, if you use java.util.logging.FileHandler then provide the following reflection configuration:
   *   {
   *    "name" : "java.util.logging.FileHandler",
   *    "methods" : [
   *      { "name" : "<init>", "parameterTypes" : [] },
   *      { "name" : "<init>", "parameterTypes" : ["java.lang.String"] }
   *    ]
   *   }
   *
   * Oracle 2022, 8 Java Logging Overview, viewed 04 December 2022,
   *              <https://docs.oracle.com/en/java/javase/19/core/java-logging-overview.html>
   *
   * GraalVM 2022, Add Logging to a Native Executable, viewed 04 December 2022,
   *               <https://www.graalvm.org/latest/reference-manual/native-image/guides/add-logging-to-native-executable/>
   *
   *
   * Commented out due to ConsoleHandler with the GraalVM native-image.
   * static {
   *   try {
   *     Handler fileHandler = new FileHandler(LOGFILE);
   *     logger.addHandler(fileHandler);
   *     logger.setLevel(Level.WARNING);
   *     fileHandler.setFormatter(new SimpleFormatter());
   *   } catch (Exception ignored) { }
   * }
   */

  private static final String BANNER = "-----------------------------------------------------";
  private static final String INDENT = "-- ";
  private static final String INDENT_NL = "--";
  private static final String FORMAT_ERROR = "JReplace Error: %-8s -> %n-- %-8s";
  private static final String FORMAT_INFO  = "JReplace Information: %n-- No Matches Found: %-8s -> %n-- %-8s";

  /* Regular expressions 101, <https://regex101.com/r/9DrDRd/2> */
  private static final Pattern pattern = Pattern.compile("(,\\h)(?!second|third|fourth)|(?<=\\])(,\\h)");

  /* InterchangeEmissionMOTHandler call point */
  public static void winston(@NotNull Pair<Route, Pair<String, LinkedList<Object>>> pair) {

    loggerJoiner(format(FORMAT_ERROR, pair.second().first(), editorial(pair.second().second())));

    pair.second().second().clear();
  }

  /* no --from=text matches found in file */
  public static void julia(@NotNull Pair<Route, Pair<String, LinkedList<Path>>> pair) {

    loggerJoiner(format(FORMAT_INFO, pair.second().first(), pair.second().second()));

    pair.second().second().clear();
  }

  public static void syme(@NotNull Pair<Route, Pair<String, LinkedList<Object>>> pair) {

    loggerJoiner(format(FORMAT_ERROR, pair.second().first(), editorial(pair.second().second())));

    pair.second().second().clear();
  }

  /* replace ', ' with '\n-- ' */
  private static String editorial(@NotNull LinkedList<Object> list) {

    return pattern.matcher(list.toString()).replaceAll("\n-- ");
  }

  /*
   * Example output: jreplace-tourism.log
   *
   * 2022-12-04T07:00:42.752073
   * -------------------------------------------------------
   * --
   * -- JReplace Information:
   * -- No Matches Found: AmanuensisMatch ->
   * -- [data/domain/TCL.java]
   * --
   * -----------------------------------------------------
   *
   * Apache Derby 2020, Logger.java, org.apache.derbyDemo.scores.util, viewed 22 November 2022,
   *        <https://github.com/apache/derby/blob/trunk/java/demo/scores/java/common/org/apache/derbyDemo/scores/util/Logger.java>
   */
  private static void loggerJoiner(String text) {
    results(() ->
            writeString(LOGFILE, format("%s%n%s%n", LocalDateTime.now(), new StringJoiner("\n", BANNER, BANNER)
                                        .add(INDENT).add(INDENT).add(INDENT + text).add(INDENT).add(INDENT_NL)),
                                        CREATE, WRITE, APPEND));
  }

}
