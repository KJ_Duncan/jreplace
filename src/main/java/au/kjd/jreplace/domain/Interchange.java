package au.kjd.jreplace.domain;

import au.kjd.jreplace.fpjava.Result;
import au.kjd.jreplace.handler.InterchangeEmissionMOTHandler;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Route;
import au.kjd.jreplace.model.Scan;
import au.kjd.jreplace.model.Triple;
import au.kjd.jreplace.model.Tuple;

import java.nio.file.Path;
import java.util.LinkedList;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import static au.kjd.jreplace.exceptional.ExceptionalUtilities.results;
import static au.kjd.jreplace.fpjava.Result.failure;
import static au.kjd.jreplace.fpjava.TailCall.zipWith;
import static au.kjd.jreplace.model.Route.UC9;

import static java.lang.String.format;
import static java.lang.System.currentTimeMillis;
import static java.nio.file.Files.move;
import static java.nio.file.Path.of;
import static java.nio.file.StandardCopyOption.REPLACE_EXISTING;
import static java.util.stream.Collectors.toCollection;

// import java.util.logging.Logger;
// import static java.util.logging.Logger.getLogger;

/*
 * Move or rename a file to a target file.
 *
 * By default, this method attempts to move the file to the target
 * file, failing if the target file exists except if the source and
 * target are the same file, in which case this method has no effect.
 *
 * REPLACE_EXISTING:
 *   If the target file exists, then the target file is replaced if it
 *   is not a non-empty directory.
 *
 * Oracle 2022, Files.move, java.nio.file.Files, viewed 25 November 2022,
 *             <https://docs.oracle.com/en/java/javase/19/docs/api/java.base/java/nio/file/Files.html#move(java.nio.file.Path,java.nio.file.Path,java.nio.file.CopyOption...)>
 */

public final class Interchange extends InterchangeEmissionMOTHandler {

  // private static final Logger logger = getLogger(Interchange.class.getSimpleName());

  /* Where triple.first is the target file, triple.third is the source file. */
  private final LinkedList<Triple<Path, Scan, Path>> listOfTriples;

  /* True = 1, False = 0 is the default --no-replace. */
  private final int replace;

  public Interchange(final @NotNull Triple<Route, LinkedList<Triple<Path, Scan, Path>>, Boolean> triple) {
    this.listOfTriples = triple.second();
    this.replace       = (triple.third()) ? 1 : 0;
  }

  /* InterchangeEmissionMOTHandler call point */
  @Contract(" -> new")
  public @NotNull Pair<Route, Result<LinkedList<Tuple<Path, Scan, Path, Result<Path>>>>> interchange() {

    return new Pair<>(UC9, obtainResult());
  }

  /*
   * Where tuple.fourth Result<Path> is the result success or failure
   * of the target files move to the source files directory.
   */
  private @NotNull Result<LinkedList<Tuple<Path, Scan, Path, Result<Path>>>> obtainResult() {

    return results(() -> {

      LinkedList<Result<Path>> llist = moveAll();

      int lsize = llist.size();
      int tsize = listOfTriples.size();

      return (tsize == lsize) ? zipWith(listOfTriples, llist, a -> b -> new Tuple<>(a.first(), a.second(), a.third(), b))
                              : new LinkedList<>();
    });
  }

  /*
   * Collect the results of moving all target files from
   * temporary directory to the source files directory.
   */
  private LinkedList<Result<Path>> moveAll() {

    return listOfTriples
        .stream()
        .map(triple -> moveWithOrWithoutReplacement(triple.first(), triple.third()))
        .collect(toCollection(LinkedList::new));
  }

  /* Where False = 0, True = 1,  zero is the default --no-replace behaviour. */
  private Result<Path> moveWithOrWithoutReplacement(final Path completed, final Path source) {

    return switch (replace) {

      case 0  -> moveWithoutReplacement(completed, source);

      case 1  -> moveWithReplacement(completed, source);

      default -> failure("Interchange moveWithOrWithoutReplacement switch statement unexpected value not equalling 0 or 1");
    };
  }

  /* Keep the original file in place and move the newly processed file into the same working directory. */
  private static @NotNull Result<Path> moveWithoutReplacement(final Path completed, final @NotNull Path source) {

    return results(() -> {

      int index       = source.getFileName().toString().lastIndexOf(".");
      Path parent     = source.toAbsolutePath().getParent();
      String filename = source.getFileName().toString();

      Path newname;
      Path rename;

      if (-1 == index)
        newname = of(filename.concat(format("%s%d", "-jreplace-", currentTimeMillis())));

      else
        newname = of(filename.substring(0, index).concat(format("%s%d%s", "-jreplace-", currentTimeMillis(), filename.substring(index))));

      rename = move(completed, completed.resolveSibling(newname));

      return move(rename, parent.resolve(rename.getFileName()));
    });
  }

  /* Replace the original source file with the newly processed target file. */
  private static @NotNull Result<Path> moveWithReplacement(final Path completed, final @NotNull Path source) {

    return results(() -> {

      Path parent = source.toAbsolutePath().getParent();
      Path rename;

      if (1 < source.getNameCount())
        rename = move(completed, completed.resolveSibling(source.getFileName()));

      else
        rename = move(completed, completed.resolveSibling(source));

      return move(rename, parent.resolve(rename.getFileName()), REPLACE_EXISTING);
    });
  }

}
