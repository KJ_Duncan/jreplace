package au.kjd.jreplace.domain;

import au.kjd.jreplace.fpjava.Result;
import au.kjd.jreplace.handler.atjhandlers.AmanuensisHandler;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Patterns;
import au.kjd.jreplace.model.RegexFrom;
import au.kjd.jreplace.model.RegexTo;
import au.kjd.jreplace.model.Route;
import au.kjd.jreplace.model.Scan;
import au.kjd.jreplace.model.Triple;

import java.lang.String;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.util.concurrent.atomic.LongAdder;
import java.util.LinkedList;
import java.util.regex.Matcher;
import java.util.stream.Stream;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import static au.kjd.jreplace.domain.Replicate.deleteTempFile;
import static au.kjd.jreplace.domain.Replicate.getEmptyFile;
import static au.kjd.jreplace.exceptional.ExceptionalUtilities.results;
import static au.kjd.jreplace.model.Route.UC7;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.nio.file.Files.lines;
import static java.nio.file.StandardOpenOption.WRITE;
import static java.util.stream.Collectors.toCollection;

// import java.util.logging.Logger;
// import static java.util.logging.Logger.getLogger;

/*
 * Process a files content and replace all occurrences of users
 * 'from' pattern with thier 'to' text.
 *
 * Matcher is an engine that performs match operations on a character
 * sequence by interpreting a Pattern. A matcher is created from a
 * pattern by invoking the pattern's matcher method. The replaceAll
 * method creates a string in which every matching subsequence in the
 * input sequence is replaced.
 *
 * A matcher may be reset explicitly by invoking its reset() method
 * or, if a new input sequence is desired, its reset(CharSequence)
 * method. Resetting a matcher discards its explicit state
 * information and sets the append position to zero.
 *
 * Instances of this class are not safe for use by multiple
 * concurrent threads.
 *
 * Oracle 2022, Matcher, java.util.regex, viewed 24 November 2022,
 *              <https://docs.oracle.com/en/java/javase/19/docs/api/java.base/java/util/regex/Matcher.html>
 *
 * GNU 2020, sed - stream editor for filtering and transforming text, viewed 24 November 2022,
 *           <https://www.gnu.org/software/sed/>
 */

public final class AmanuensisProcess extends AmanuensisHandler {

  // private static final Logger logger = getLogger(AmanuensisProcess.class.getSimpleName());

  private final LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>> collections;
  private final Patterns patterns;

  /* Outer list is the amount of source files, inner list is the number of split files associated with the source file. */
  public AmanuensisProcess(final @NotNull Triple<Route, LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>, Patterns> triple) {
    this.collections = triple.second();
    this.patterns    = triple.third();
  }

  /* AmanuensisHandler call point */
  @Contract(" -> new")
  public @NotNull Pair<Route, Result<Triple<LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>> amanuensisProcess() {

    return new Pair<>(UC7, obtainResult());
  }

  private @NotNull Result<Triple<LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>, LinkedList<Path>, LinkedList<Exception>>> obtainResult() {

    return results(() -> {

      // exceptions is always an empty list, kept for code base consistency.
      var exceptions = new LinkedList<Exception>();
      var failures   = new LinkedList<Path>();
      var processed  = new LinkedList<Pair<Stream<Triple<Pair<Path, Boolean>, LinkedList<Long>, Scan>>, Path>>();

      do {

        Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path> collection = collections.removeFirst();

        Stream<AmanuensisSubstitute> stream = amanuensisSubstitute(collection.first());

        Stream<Triple<Pair<Path, Boolean>, LinkedList<Long>, Scan>> streamTriple = processAmanuensisSubstitute(stream);

        Pair<Stream<Triple<Pair<Path, Boolean>, LinkedList<Long>, Scan>>, Path> pair = new Pair<>(streamTriple, collection.second());

        processed.add(pair);

      } while (!collections.isEmpty());

      LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>> successes = filterProcessed(processed, failures);

      return new Triple<>(successes, failures, exceptions);
    });
  }

  private Stream<AmanuensisSubstitute> amanuensisSubstitute(@NotNull LinkedList<Pair<Path, LinkedList<Long>>> collection) {

    return collection.stream().map(pair -> new AmanuensisSubstitute(pair, patterns));
  }

  private Stream<Triple<Pair<Path, Boolean>, LinkedList<Long>, Scan>> processAmanuensisSubstitute(@NotNull Stream<AmanuensisSubstitute> stream) {

    return stream.map(ams -> new Triple<>(ams.processLines(), ams.pair.second(), ams.scan));
  }

  private @NotNull LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>> filterProcessed(@NotNull LinkedList<Pair<Stream<Triple<Pair<Path, Boolean>, LinkedList<Long>, Scan>>, Path>> processed, LinkedList<Path> failures) {

    return processed
        .stream()
        .map(pair ->
            new Pair<>(pair.first()

                   /* If the file is tagged with false it was not processed, add it to the list of failures. */
                  .peek(triple -> { if (!triple.first().second()) failures.add(triple.first().first()); })

                  /* Filter in all files tagged with true. */
                  .filter(triple1 -> triple1.first().second())

                  .map(this::untangleTriple)

                  .collect(toCollection(LinkedList::new))

                , pair.second()))

        .collect(toCollection(LinkedList::new));
  }

  /* Return the processed file, its positional pointer amongst the spilt files, and the regex counter. */
  @Contract("_ -> new")
  private @NotNull Triple<Path, Long, Scan> untangleTriple(@NotNull Triple<Pair<Path, Boolean>, LinkedList<Long>, Scan> triple) {

    return new Triple<>(triple.first().first(), triple.second().removeFirst(), triple.third());
  }

  /* Inner class wraps each file with processing logic. */
  private static final class AmanuensisSubstitute {

    private final Pair<Path, LinkedList<Long>> pair;
    private final Patterns patterns;
    private final Scan scan;
    private final Matcher matchRegexFrom;
    private final Matcher matchRegexTo;

    AmanuensisSubstitute(final Pair<Path, LinkedList<Long>> pair, final @NotNull Patterns patterns) {
      this.pair           = pair;
      this.patterns       = patterns;
      this.scan           = new Scan(new RegexFrom(new LongAdder()), new RegexTo(new LongAdder()));
      this.matchRegexFrom = patterns.getRegexFrom().matcher("");
      this.matchRegexTo   = patterns.getRegexTo().matcher("");
    }

    /*
     * Kept the functionality enclosed in a single method with the use of
     * java language try catch blocks. While the result type works and
     * subsequent extraction of method functionality was in place
     * inconsistencies arose from the code base. No analysis on the code
     * was preformed to isolate discrepancies.
     */
    @Contract(" -> new")
    private @NotNull Pair<Path, Boolean> processLines() {

      /* An intialiser value other than null */
      Path destination = Path.of("");

      try {

        destination = getEmptyFile(pair.first()).orThrow();

        FileChannel channel = FileChannel.open(destination, WRITE);

        /*
         * The use of position[0] is to overcome the requirement for a value
         * to be final or effectively final inside a lambda.
         */
        long[] position = { 0L };
        byte[] newline  = "\n".getBytes(UTF_8);
        int one         = newline.length;

        /*
         * NOTE: flipping the buffer here produces incorrect results: '\n' == NUL
         * bb.flip();
         */
        ByteBuffer bb   = ByteBuffer.allocate(one);
        bb.put(newline);

        /*
         * Read all lines from a file as a Stream. Bytes from the file are
         * decoded into characters using the specified charset.
         *
         * UTF-8 Eight-bit UCS Transformation Format, The UTF-8 charset is
         * specified by RFC 2279.
         *
         * Oracle 2022, Files.lines, java.nio.file, viewed 24 November 2022,
         *              <https://docs.oracle.com/en/java/javase/19/docs/api/java.base/java/nio/file/Files.html#lines(java.nio.file.Path,java.nio.charset.Charset)>
         *
         * Network Working Group 1998, UTF-8, a transformation format of ISO 10646, viewed 24 November 2022,
         *                             <https://www.ietf.org/rfc/rfc2279.txt>
         */
        try (Stream<String> stream = lines(pair.first(), UTF_8)) {

          stream.forEach(line -> {

            try {

              long count = countRegexToOccurrences(line);
              scan.regexTo().longValue().add(count);
              count = 0L;

              String processed = processLine(line);

              int length = processed.length();

              if (0 != length) {

                /* A call to string.indexOf("\n") returns -1 as Files.lines strips the '\n' character. */
                byte[] bytes = processed.getBytes(UTF_8);

                /*
                 * We receive a buffer overflow exception if called with (length + one),
                 * unsure why, hence, bytes.length + one.
                 */
                int total = bytes.length + one;

                ByteBuffer buffer = ByteBuffer.allocate(total);

                buffer.put(bytes);
                buffer.put(newline);

                channel.write(buffer.flip(), position[0]);

                /* Where length + 1 = '\n'. */
                position[0] += total;

                buffer.clear();
                total  = 0;
              }
              else {
                channel.write(bb.flip(), position[0]);
                position[0]++;
              }
            }
            catch (Exception e) { throw new RuntimeException(e); }
          });
        }

        channel.close();
        deleteTempFile(pair.first());

        /* Return the processed file and tag with boolean true as success. */
        return new Pair<>(destination, TRUE);

      } catch (Exception e) {

        if (!destination.startsWith("")) deleteTempFile(destination);

        /*
         * Return the file without any processing and tag with boolean
         * false as failure.
         */
        return new Pair<>(pair.first(), FALSE);
      }
    }

    /* Before processing a line if no regex 'from' is found return the line. */
    private String processLine(String string) {

      if (noRegexFromFound(string)) return string;

      long count = countRegexFromOccurrences(string);
      scan.regexFrom().longValue().add(count);

      count = 0L;

      return transformRegexTo();
    }

    /* Checks line for the regex 'from' pattern. */
    private boolean noRegexFromFound(String line) {

      return !matchRegexFrom.reset(line).find();
    }

    /*
     * Match on the regex 'from' pattern and replace all occurrence with
     * user specified 'to'.
     */
    private String transformRegexTo() {

      return matchRegexFrom.replaceAll(patterns.getTo());
    }

    /*
     * Before processing the file count the number of occurrences that match the regex 'to' pattern.
     * Before processing the file count the number of occurrences that match the regex 'from' pattern.
     *
     * The number of regex 'to' patterns plus the number of regex 'from' patterns equals
     * the total number of occurrences of the regex 'to' pattern in the file after processing.
     */
    private long countRegexToOccurrences(String line) {

      long count = 0L;

      if (matchRegexTo.reset(line).find()) count = patterns.getRegexTo().splitAsStream(line).count() - 1L;

      return count;
    }

    private long countRegexFromOccurrences(String line) {

      return patterns.getRegexFrom().splitAsStream(line).count() - 1L;
    }

  }

}
