package au.kjd.jreplace.cli;

import au.kjd.jreplace.handler.CliHandler;
import au.kjd.jreplace.model.Nothing;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.concurrent.Callable;
import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import picocli.CommandLine.ArgGroup;
import picocli.CommandLine.Command;
import picocli.CommandLine.Model.CommandSpec;
import picocli.CommandLine.Option;
import picocli.CommandLine.ParameterException;
import picocli.CommandLine.Spec;

import static au.kjd.jreplace.model.Nothing.NOTHING;

import static java.lang.String.format;
import static java.nio.file.FileSystems.getDefault;
import static java.util.Optional.ofNullable;

/*
 * Command line entry point.
 *
 * Picocli 2022, picocli - a mighty tiny command line interface,
 *               version 4.6.3, viewed 28 September 2022,
 *               <https://picocli.info>
 *
 * Usage: jreplace --path=StartingDirectory --depth=N --from=Text --to=Text (--regex|--glob) "Syntax" --megabytes=N --replace
 */

@Command(name = "jreplace", sortOptions = false, usageHelpAutoWidth = true, showAtFileInUsageHelp = false, description = "")
public final class Cli extends CliHandler implements Callable<Nothing> {

  private static final String PWD   = "";
  private static final String GLOB  = "glob";
  private static final String REGEX = "regex";

  @Spec CommandSpec spec;

  @Option(order = 1, names = { "-h", "--help" }, usageHelp = true, description = """
          JReplace source code location:
          https://tinyurl.com/2p8b66n2

          Enter a long URL to make a TinyURL
          https://tinyurl.com/app/
          """)
  private boolean helpRequested = false;

  @Option(order = 2, names = { "-p", "--path"}, paramLabel = "directory", description = """
  The starting directory from where to traverse. If no option is
  given defaults to the present working directory.
  """)
  private Path startingDirectory;

  private int traversalDepth;

  @Option(order = 3, names = { "-d", "--depth" }, paramLabel = "int", defaultValue = "1", description = """
  Maximum traversal depth where the default value is ${DEFAULT-VALUE},
  which traverses the starting directory. Possible range
  is: 0 < depth < 2^31. The maximum depth is equal to
  2147483646 and may be used to indicate that all levels
  should be visited. Refer to the below link for further
  documentation. https://tinyurl.com/2p8s3vjy
  """)
  private void setTraversalDepth(long value) {
    /*
     * NOTE: value is a long, it can have a maximum value of Integer.MAX_VALUE or a non-negative minimum
     *       due to a poor default error message on integer overflow I have implemented value as long and
     *       restricted its range.
     */
    boolean invalid = 1 > value || Integer.MAX_VALUE < value;
    if (invalid) throw new ParameterException(spec.commandLine(), format("Invalid value '%d' for option '--depth': 0 < depth < 2147483647", value));
    traversalDepth = (int) value;
  }

  @Option(order = 4, names = { "-f", "--from" }, required = true, paramLabel = "text", description = """
  Text structuring; word, sentence, syntax, punctuation, or regular
  expression pattern. Refer to the below link for further
  documentation on regular expressions.
  https://tinyurl.com/8n948zu2
  """)
  private String from;

  @Option(order = 5, names = { "-t", "--to" }, required = true, paramLabel = "text", description = """
  Text structuring; word, sentence, syntax, punctuation.
  """)
  private String to;

  @ArgGroup(exclusive = true, multiplicity = "1..1", heading = """
            The tinyurl link takes you to the appropriate java
            api documentation for expression and pattern support.\n
            """)
  private Syntax syntax;

  private static class Syntax {

    private String regex;
    private String glob;
    @Spec CommandSpec spec;

    @Contract(pure = true)
    private @NotNull Function<String, Pattern> check() throws PatternSyntaxException { return Pattern::compile; }


    @Option(order = 6, names = { "-r", "--regex" }, paramLabel = "expression", description = """
            A FileSystem implementation that supports the regex syntax.
            https://tinyurl.com/8n948zu2
            """)
    private void setRegex(String value) {
      try { check().apply(Pattern.quote(value)); }
      catch (Exception e) { throw new ParameterException(spec.commandLine(), format("Invalid value '%s' for option '--regex'%n'%s'%n", value, e.toString())); }
      regex = value;
    }

    @Option(order = 7, names = { "-g", "--glob" }, paramLabel = "pattern", description = """
            A FileSystem implementation that supports the glob syntax.
            https://tinyurl.com/4w7ecmmt
            """)
    private void setGlob(String value) {
      try { check().apply(Pattern.quote(value)); }
      catch (Exception e) { throw new ParameterException(spec.commandLine(), format("Invalid value '%s' for option '--glob'%n'%s'%n", value, e.toString())); }
      glob = value;
    }
  }

  private long megaBytes;

  @Option(order = 8, names = { "-m", "--megabytes" }, paramLabel = "int", defaultValue = "0", description = """
  Maximum number of megabytes to split a file on. Where the default
  value is ${DEFAULT-VALUE} which process a file without splitting. Possible range
  is: 0 <= megabytes <= 2047. Splitting a large file reduces the
  required memory to process the file.
  """)
  private void setMegaBytes(long value) {
    boolean invalid = 0 > value || 2047 < value;
    if (invalid) throw new ParameterException(spec.commandLine(), format("Invalid value '%d' for option '--megabytes': 0 <= megabytes <= 2047", value));
    megaBytes = value;
  }

  @Option(order = 9, names = "--replace", negatable = true, description = """
          The default is not to replace a source file after processing.

          Places the processed file in the source files directory and
          appends '-jreplace-XXXXXXX' where X is current time in
          milliseconds to ensure a unique file name.
          """)
  private boolean replace = false;

  @Option(order = 10, names = "--output", description = """
          Output the stats to a file rather than to the consoles standard
          output. If the file doesn't exist create it. If the file exists
          append to the file.
          """)
  private Path output;

  /* TODO: implement as --no-stats don't count the number of occurrences */
  @Option(order = 11, names = "--stats", negatable = true, hidden = true, description = "output the count off occurrences")
  private boolean stats = true;

  /* TODO: implement as --no-verbose standard output */
  @Option(order = 12, names = "--verbose", negatable = true, hidden = true, description = "output stats to console")
  private boolean verbose = true;


  @Override public @NotNull Nothing call() {

      cliHandler(
        (null != startingDirectory) ? startingDirectory : Paths.get(PWD),
        traversalDepth,
        (null != syntax.regex) ? getDefault().getPathMatcher(format("%s:%s", REGEX, syntax.regex))
                               : getDefault().getPathMatcher(format("%s:%s", GLOB, syntax.glob)),
        from,
        to,
        megaBytes,
        replace,
        ofNullable(output)
      );

      return NOTHING;
  }

}
