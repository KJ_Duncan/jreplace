package au.kjd.jreplace.exceptional;


@FunctionalInterface public interface ExceptionalConsumer<T> { void accept(T t) throws Exception; }
