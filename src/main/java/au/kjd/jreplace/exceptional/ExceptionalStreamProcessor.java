package au.kjd.jreplace.exceptional;

import java.io.IOException;

@FunctionalInterface public interface ExceptionalStreamProcessor<T, R> { R process(T t) throws IOException; }

