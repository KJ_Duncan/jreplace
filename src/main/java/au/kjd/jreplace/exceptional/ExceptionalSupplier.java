package au.kjd.jreplace.exceptional;


@FunctionalInterface public interface ExceptionalSupplier<T> { T get() throws Exception; }

