package au.kjd.jreplace.exceptional;

import au.kjd.jreplace.fpjava.Result;

import org.jetbrains.annotations.NotNull;

import static au.kjd.jreplace.fpjava.Result.failure;
import static au.kjd.jreplace.fpjava.Result.success;


/*
 * Batonga In Batongaville, Budapest Philharmonic Orchestra, Natural Born Killers,
 *                          Spotify, viewed 10 October 2022,
 *                          <https://open.spotify.com/track/67QhOzUSldGPZRyo05YisD?si=86ac0fde45eb4607>
 */
public interface ExceptionalUtilities {

  default <T> @NotNull Result<T> result(ExceptionalSupplier<T> fn) {
    try { return success(fn.get()); } catch (Exception e) { return failure(e); }
  }

  static <T> @NotNull Result<T> results(ExceptionalSupplier<T> fn) {
    try { return success(fn.get()); } catch (Exception e) { return failure(e); }
  }

  /* sobeit: synonym of 'if' */
  default <T> @NotNull Result<T> sobeit(boolean condition, ExceptionalSupplier<T> truecase, ExceptionalSupplier<T> falsecase) {
    try { return condition ? success(truecase.get()) : success(falsecase.get()); }
    catch (Exception e) { return failure(e); }
  }

  static <T> @NotNull Result<T> ifs(boolean condition, ExceptionalSupplier<T> truecase, ExceptionalSupplier<T> falsecase) {
    try { return condition ? success(truecase.get()) : success(falsecase.get()); }
    catch (Exception e) { return failure(e); }
  }

  static <T> void perhaps(boolean condition, T value, ExceptionalConsumer<T> truecase, ExceptionalConsumer<T> falsecase) {
    try { if(condition) truecase.accept(value); else falsecase.accept(value); }
    catch (Exception ignored) { }
  }

}
