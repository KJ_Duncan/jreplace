package au.kjd.jreplace.model;


public record Scan(RegexFrom regexFrom, RegexTo regexTo) { }
