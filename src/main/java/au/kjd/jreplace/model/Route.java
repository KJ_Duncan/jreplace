package au.kjd.jreplace.model;

import java.util.concurrent.atomic.AtomicInteger;

/*
 * A-L: National Routes
 * M-R: State Routes
 * STR: Strategic Tourism Route
 * TS:  Test suite
 * UC:  Use-Case
 * MOT: Ministry of Truth
 * VIC: Visitor Information Centre
 *
 * Google, Road numbering systems, Australia, viewed 05 November 2022,
 *        <https://sites.google.com/site/roadnumberingsystems/home/countries/australia>
 *
 * Javamex UK 2021, Reading the thread profile data: synchronization issues (2), viewed 18 November 2022,
 *               <https://www.javamex.com/tutorials/profiling/profiling_java5_threads_synchronization_2.shtml>
 */
public enum Route {
  A1, A2, A3,
  B1, B2, B3,
  C1, C2, C3,
  D1, D2, D3, D4, D5,
  E1, E2, E3,
  M1, M2, M3, M4,
  MOT,
  N1, N2, N3, N4,
  STR,
  TS,
  UC1, UC2, UC3, UC4, UC5, UC6, UC7, UC8, UC9, UC10, UC11,
  VIC;

  /* When it has a value of 1, the data is in use, and we need to wait for it to return to 0. */
  public static final AtomicInteger lock = new AtomicInteger(1);

}
