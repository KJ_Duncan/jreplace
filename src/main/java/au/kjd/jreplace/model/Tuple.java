package au.kjd.jreplace.model;

/*
 * ICE-T 1988, I'm Your Pusher / Pusherman, Power, Rhino/Warner Records, Spotify,
 *             viewed 20 October 2022, <https://open.spotify.com/track/2m2dKtaKequ1QZYGWYG3hG?si=3a9ea29e456e4c9f>
 */
public record Tuple<I, C, E, T>(I first, C second, E third, T fourth) { }

