package au.kjd.jreplace.model;

import au.kjd.jreplace.fpjava.Result;

import java.util.function.BooleanSupplier;
import java.util.function.Supplier;
import java.util.LinkedList;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;


public abstract class BooleanSuppliers {

  /*
   * AmanuensisHandler
   * Supplier<Pair<LinkedList<S>, LinkedList<T>>>
   *
   * TheJustHandler
   * Supplier<Pair<LinkedList<S>, LinkedList<T>>>
   *
   * WalkerReplicateHandler
   * Supplier<Pair<LinkedList<S>, LinkedList<T>>>
   */
  public static final class BooleanSupplierPair<S, T> {

    private final Supplier<Pair<LinkedList<S>, LinkedList<T>>> success;

    @Contract(pure = true)
    private @NotNull BooleanSupplier hasSuccesses() { return () -> !success.get().first().isEmpty();  }

    @Contract(pure = true)
    private @NotNull BooleanSupplier noSuccesses()  { return () -> !hasSuccesses().getAsBoolean();    }

    @Contract(pure = true)
    private @NotNull BooleanSupplier hasFailures()  { return () -> !success.get().second().isEmpty(); }

    @Contract(pure = true)
    private @NotNull BooleanSupplier noFailures()   { return () -> !hasFailures().getAsBoolean();     }

    public final BooleanSupplier hasSuccessFailures     = () -> hasSuccesses().getAsBoolean() && hasFailures().getAsBoolean();
    public final BooleanSupplier hasSuccessNoFailures   = () -> hasSuccesses().getAsBoolean() && noFailures().getAsBoolean();
    public final BooleanSupplier noSuccessesHasFailures = () -> noSuccesses().getAsBoolean()  && hasFailures().getAsBoolean();
    public final BooleanSupplier noSuccessesFailures    = () -> noSuccesses().getAsBoolean()  && noFailures().getAsBoolean();

    @Contract(pure = true)
    public BooleanSupplierPair(final Pair<Route, Result<Pair<LinkedList<S>, LinkedList<T>>>> pair) { this.success = () -> pair.second().successValue(); }

    public Pair<LinkedList<S>, LinkedList<T>> success() { return success.get(); }

  }

  /*
   * AmanuensisHandler
   * Supplier<Triple<LinkedList<S>, LinkedList<T>, LinkedList<U>>>
   *
   * TheJustHandler
   * Supplier<Triple<LinkedList<S>, LinkedList<T>, LinkedList<U>>>
   */
  public static final class BooleanSupplierTriple<S, T, U> {

    private final Supplier<Triple<LinkedList<S>, LinkedList<T>, LinkedList<U>>> success;

    @Contract(pure = true)
    private @NotNull BooleanSupplier hasSuccesses()  { return () -> !success.get().first().isEmpty();  }

    @Contract(pure = true)
    private @NotNull BooleanSupplier noSuccesses()   { return () -> !hasSuccesses().getAsBoolean();    }

    @Contract(pure = true)
    private @NotNull BooleanSupplier hasFailures()   { return () -> !success.get().second().isEmpty(); }

    @Contract(pure = true)
    private @NotNull BooleanSupplier noFailures()    { return () -> !hasFailures().getAsBoolean();     }

    @Contract(pure = true)
    private @NotNull BooleanSupplier hasExceptions() { return () -> !success.get().third().isEmpty();  }

    @Contract(pure = true)
    private @NotNull BooleanSupplier noExceptions()  { return () -> !hasExceptions().getAsBoolean();   }

    public final BooleanSupplier hasSuccessesFailuresExceptions   = () -> hasSuccesses().getAsBoolean() && hasFailures().getAsBoolean()  && hasExceptions().getAsBoolean();
    public final BooleanSupplier hasSuccessesFailuresNoExceptions = () -> hasSuccesses().getAsBoolean() && hasFailures().getAsBoolean()  && noExceptions().getAsBoolean();
    public final BooleanSupplier hasSuccessesExceptionsNoFailures = () -> hasSuccesses().getAsBoolean() && noFailures().getAsBoolean()   && hasExceptions().getAsBoolean();
    public final BooleanSupplier hasSuccessesNoFailuresExceptions = () -> hasSuccesses().getAsBoolean() && noFailures().getAsBoolean()   && noExceptions().getAsBoolean();
    public final BooleanSupplier noSuccessesHasFailuresExceptions = () -> noSuccesses().getAsBoolean()  && hasFailures().getAsBoolean()  && hasExceptions().getAsBoolean();
    public final BooleanSupplier noSuccessesExceptionsHasFailures = () -> noSuccesses().getAsBoolean()  && noExceptions().getAsBoolean() && hasFailures().getAsBoolean();
    public final BooleanSupplier noSuccessesFailuresHasExceptions = () -> noSuccesses().getAsBoolean()  && noFailures().getAsBoolean()   && hasExceptions().getAsBoolean();

    @Contract(pure = true)
    public BooleanSupplierTriple(final Pair<Route, Result<Triple<LinkedList<S>, LinkedList<T>, LinkedList<U>>>> pair) { this.success = () -> pair.second().successValue(); }

    public Triple<LinkedList<S>, LinkedList<T>, LinkedList<U>> success() { return success.get(); }

  }

}
