package au.kjd.jreplace.model;

/*
 * Runway, Bree & Elliott, Missy 2020, ATM, 2000AND4EVA,
 *   EMI, Wixen Music Publishing, Spotify, viewed on 29 September 2020,
 *   <https://open.spotify.com/track/3cqm5i4yDaNuzu9oQmQzBC?si=bdb5edf612be4dc3>
 */
public record Triple<A, T, M> (A first, T second, M third) { }
