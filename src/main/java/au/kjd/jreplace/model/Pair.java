package au.kjd.jreplace.model;

import java.util.StringJoiner;
import org.jetbrains.annotations.Contract;

/*
 * The Chats, 2017, Bus Money, Get This In Ya,
 *            Bargin Bin Records, Spotify,
 *            viewed 29 September 2020,
 *            <https://open.spotify.com/track/3bg0C4y3BvpaB0TPj5sQlf?si=02dedf890b97400d>
 */
public class Pair<B, M> {

  private final B first;
  private final M second;

  @Contract(pure = true)
  public Pair(final B first, final M second) {
    this.first  = first;
    this.second = second;
  }

  @Contract(pure = true)
  final public B first()  { return first; }

  @Contract(pure = true)
  final public M second() { return second; }

  /* Intellij implements the override methods */
  @Contract(value = "null -> false", pure = true)
  @Override final public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Pair<?, ?> pair)) return false;

    if (!first.equals(pair.first)) return false;
    return second.equals(pair.second);
  }

  @Override final public int hashCode() {
    int result = first.hashCode();
    result = 31 * result + second.hashCode();
    return result;
  }

  @Override final public String toString() {
    return new StringJoiner(", ", Pair.class.getSimpleName() + "[", "]")
        .add("first=" + first)
        .add("second=" + second)
        .toString();
  }

}
