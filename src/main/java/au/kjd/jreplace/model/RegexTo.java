package au.kjd.jreplace.model;

import java.util.concurrent.atomic.LongAdder;

public record RegexTo(LongAdder longValue) { }
