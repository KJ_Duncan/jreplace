package au.kjd.jreplace.model;

import java.util.function.Function;
import java.util.regex.Pattern;
import java.util.regex.PatternSyntaxException;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;


public final class Patterns {

  private final String to;
  private final String from;
  private final Pattern regexTo;
  private final Pattern regexFrom;

  public Patterns(final @NotNull From from, final @NotNull To to) {
    this.from      = from.syntax();
    this.to        = to.syntax();
    this.regexFrom = regexFrom();
    this.regexTo   = regexTo();
  }

  @Contract(pure = true)
  public String getTo() { return to; }

  @Contract(pure = true)
  public String getFrom() { return from; }

  @Contract(pure = true)
  public Pattern getRegexTo() { return regexTo; }

  @Contract(pure = true)
  public Pattern getRegexFrom() { return regexFrom; }

  private Pattern regexTo() { return check().apply(to); }

  private Pattern regexFrom() { return check().apply(from); }

  @Contract(pure = true)
  private @NotNull Function<String, Pattern> check() throws PatternSyntaxException { return Pattern::compile; }

}
