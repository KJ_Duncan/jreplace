package au.kjd.jreplace.controller;

import au.kjd.jreplace.domain.Replicate;
import au.kjd.jreplace.model.Patterns;

import java.nio.file.Path;
import java.util.Optional;

//import java.util.logging.Logger;
//import static java.util.logging.Logger.getLogger;


record Trailer(Patterns patterns, long megabytes, boolean replace, Optional<Path> output) {

  // private static final Logger logger = getLogger(Trailer.class.getSimpleName());

  /*
   * The Java virtual machine shuts down in response to two kinds of
   * events:
   *
   * 1) The program exits normally, when the last non-daemon thread exits
   *    or when the exit (equivalently, System.exit) method is invoked, or
   *
   * 2) The virtual machine is terminated in response to a user interrupt,
   *    such as typing ^C, or a system-wide event, such as user logoff or
   *    system shutdown.
   *
   * A shutdown hook is simply an initialized but unstarted thread.
   * When the virtual machine begins its shutdown sequence it will
   * start all registered shutdown hooks in some unspecified order and
   * let them run concurrently.
   *
   * Oracle 2022, addShutdownHook, java.lang.Runtime,
   *              java se 18, viewed 11 November 2022,
   *              <https://docs.oracle.com/en/java/javase/18/docs/api/java.base/java/lang/Runtime.html#addShutdownHook(java.lang.Thread)>
   */
  static { Runtime.getRuntime().addShutdownHook(new Thread(Replicate::deleteJreplaceTempDirectory)); }

}
