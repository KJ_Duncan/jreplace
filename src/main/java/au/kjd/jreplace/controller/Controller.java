package au.kjd.jreplace.controller;

import au.kjd.jreplace.handler.ATJHandler;
import au.kjd.jreplace.handler.InterchangeEmissionMOTHandler;
import au.kjd.jreplace.handler.WalkerReplicateHandler;
import au.kjd.jreplace.model.Nothing;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Patterns;
import au.kjd.jreplace.model.Route;
import au.kjd.jreplace.model.Scan;
import au.kjd.jreplace.model.Triple;

import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.LinkedList;
import java.util.Optional;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import static au.kjd.jreplace.model.Nothing.NOTHING;
import static au.kjd.jreplace.model.Route.B1;
import static au.kjd.jreplace.model.Route.B2;
import static au.kjd.jreplace.model.Route.B3;

// import java.util.logging.Logger;
// import static java.util.logging.Logger.getLogger;


/*
 * Controller directs the routes for the level one handlers.
 *
 * B1->C3 WalkerReplicateHandler
 * B2->D5 ATJHandler
 * B3->. InterchangeEmissionMOTHandler
 */

public abstract class Controller {

  // private static final Logger logger = getLogger(Controller.class.getSimpleName());

  private static Trailer trailer;

  /* Creates a package private singleton of the users command line options. */
  public static void trailer(Patterns patterns, long megaBytes, boolean replace, Optional<Path> output) {

    if (null == trailer) trailer = new Trailer(patterns, megaBytes, replace, output);
  }

  public static <M> Nothing nationalRoute(Pair<Route, M> pair) { return dataflow(pair); }

  @SuppressWarnings("unchecked")
  private static <M> Nothing dataflow(@NotNull Pair<Route, M> pair) {

    return switch (pair.first()) {

      case A1  -> { WalkerReplicateHandler.walkerReplicate(new Pair<>(B1, (Triple<Path, Integer, PathMatcher>) pair.second())); yield NOTHING; }

      case C3  -> { ATJHandler.atjHandler(new Triple<>(B2, (LinkedList<Pair<Path, Path>>) pair.second(), new Pair<>(getPatterns(), getMegabytes()))); yield NOTHING; }

      case D5  -> { InterchangeEmissionMOTHandler.interchangeEmission(new Triple<>(B3, (LinkedList<Triple<Path, Scan, Path>>) pair.second(), new Triple<>(getReplace(), getPatterns(), getOutput()))); yield NOTHING; }

      case MOT -> { InterchangeEmissionMOTHandler.localRoute(new Pair<>(B3, (Pair<String, LinkedList<Object>>) pair.second())); yield NOTHING; }

      case STR -> { InterchangeEmissionMOTHandler.touristRoute(new Pair<>(B3, (Pair<String, LinkedList<Object>>) pair.second())); yield NOTHING; }

      case VIC -> { InterchangeEmissionMOTHandler.informationRoute(new Pair<>(B3, (Pair<String, LinkedList<Path>>) pair.second())); yield NOTHING; }

      default  -> NOTHING;

    };
  }

  @Contract(pure = true)
  private static Patterns getPatterns() { return trailer.patterns(); }

  @Contract(pure = true)
  private static long getMegabytes() { return trailer.megabytes(); }

  @Contract(pure = true)
  private static boolean getReplace() { return trailer.replace(); }

  @Contract(pure = true)
  private static Optional<Path> getOutput() { return trailer.output(); }

}

