package au.kjd.jreplace.handler;

import au.kjd.jreplace.domain.Emission;
import au.kjd.jreplace.domain.Interchange;
import au.kjd.jreplace.domain.Minitru;
import au.kjd.jreplace.fpjava.Result;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Patterns;
import au.kjd.jreplace.model.Route;
import au.kjd.jreplace.model.Scan;
import au.kjd.jreplace.model.Triple;
import au.kjd.jreplace.model.Tuple;

import java.nio.file.Path;
import java.util.function.Supplier;
import java.util.LinkedList;
import java.util.Optional;

import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;

import static au.kjd.jreplace.controller.Controller.nationalRoute;
import static au.kjd.jreplace.model.Route.E1;
import static au.kjd.jreplace.model.Route.E2;
import static au.kjd.jreplace.model.Route.E3;
import static au.kjd.jreplace.model.Route.STR;


/*
 * Handler for the Interchange, Emission, and Minitru domain.
 *
 * E1->UC9 Interchange move or rename a file to a target file.
 * E2->UC10 Emission writes to standard output or to users output file.
 * E3->UC11 Minitru writes errors and exceptions to a specified log file.
 */

public sealed abstract class InterchangeEmissionMOTHandler permits Interchange, Emission, Minitru {

  private static Triple<Boolean, Patterns, Optional<Path>> localTriple;

  public static void interchangeEmission(@NotNull Triple<Route, LinkedList<Triple<Path, Scan, Path>>, Triple<Boolean, Patterns, Optional<Path>>> triple) {

    localTriple = triple.third();

    interchange(triple.second());
  }

  private static void interchange(LinkedList<Triple<Path, Scan, Path>> list) {

    Triple<Route, LinkedList<Triple<Path, Scan, Path>>, Boolean> a = new Triple<>(E1, list, localTriple.first());

    Interchange b = new Interchange(a);

    Pair<Route, Result<LinkedList<Tuple<Path, Scan, Path, Result<Path>>>>> c = b.interchange();

    interchangeHandler(c);
  }

  private static void interchangeHandler(Pair<Route, Result<LinkedList<Tuple<Path, Scan, Path, Result<Path>>>>> pairResult) {

    Supplier<LinkedList<Tuple<Path, Scan, Path, Result<Path>>>> success = resultSuccessValue(pairResult);

    boolean checkResult = pairResult.second().isSuccess();

    if (checkResult) emissionHandler(success.get());

    else {

      var list = new LinkedList<Exception>();

      list.add(pairResult.second().failureValue());

      nationalRoute(new Pair<>(STR, new Pair<>("Interchange", list)));
    }
  }

  /* termianl case for standard out to file or console */
  private static void emissionHandler(LinkedList<Tuple<Path, Scan, Path, Result<Path>>> list) {

    var a = new Tuple<>(E2, list, localTriple.second(), localTriple.third());

    var b = new Emission(a);

    b.emission();

    /* Releases the lock on WalkerReplicateHandler.replicate() while loop. */
    Route.lock.set(0);
  }

  /* intermediate processing for failures or exceptions */
  public static void localRoute(@NotNull Pair<Route, Pair<String, LinkedList<Object>>> pair) {

    var a = new Pair<>(E3, pair.second());

    Minitru.syme(a);
  }

  /* terminal case for failures or exceptions */
  public static void touristRoute(@NotNull Pair<Route, Pair<String, LinkedList<Object>>> pair) {

    var a = new Pair<>(E3, pair.second());

    Minitru.winston(a);

    /* Releases the lock on WalkerReplicateHandler.replicate() while loop. */
    Route.lock.set(0);
  }

  /* termial case for user feedback information */
  public static void informationRoute(@NotNull Pair<Route, Pair<String, LinkedList<Path>>> pair) {

    var a = new Pair<>(E3, pair.second());

    Minitru.julia(a);

    /* Releases the lock on WalkerReplicateHandler.replicate() while loop. */
    Route.lock.set(0);
  }

  @Contract(pure = true)
  private static <T> @NotNull Supplier<T> resultSuccessValue(Pair<Route, Result<T>> pair) { return () -> pair.second().successValue(); }

}
