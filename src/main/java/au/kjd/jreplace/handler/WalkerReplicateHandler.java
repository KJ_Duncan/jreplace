package au.kjd.jreplace.handler;

import au.kjd.jreplace.domain.Replicate;
import au.kjd.jreplace.domain.Walker;
import au.kjd.jreplace.fpjava.Result;
import au.kjd.jreplace.model.Nothing;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Route;
import au.kjd.jreplace.model.Triple;
import au.kjd.jreplace.model.Tuple;

import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.LinkedList;

import org.jetbrains.annotations.NotNull;

import static au.kjd.jreplace.controller.Controller.nationalRoute;
import static au.kjd.jreplace.exceptional.ExceptionalUtilities.perhaps;
import static au.kjd.jreplace.fpjava.Case.cause;
import static au.kjd.jreplace.fpjava.Case.match;
import static au.kjd.jreplace.model.BooleanSuppliers.BooleanSupplierTriple;
import static au.kjd.jreplace.model.Nothing.NOTHING;
import static au.kjd.jreplace.model.Route.C1;
import static au.kjd.jreplace.model.Route.C2;
import static au.kjd.jreplace.model.Route.C3;
import static au.kjd.jreplace.model.Route.MOT;
import static au.kjd.jreplace.model.Route.STR;


/*
 * Handler for the Walker and Replicate domain.
 *
 * C1->UC3 Walker walks a file tree rooted at a given starting file.
 * C2->UC4 Replicate clones a source file to temporary file.
 */

public sealed abstract class WalkerReplicateHandler permits Walker, Replicate {


  public static void walkerReplicate(@NotNull Pair<Route, Triple<Path, Integer, PathMatcher>> pair) {

    Tuple<Route, Path, Integer, PathMatcher> a = new Tuple<>(C1, pair.second().first(), pair.second().second(), pair.second().third());

    Walker b = new Walker(a);

    Pair<Route, Result<Triple<LinkedList<Path>, LinkedList<Path>, LinkedList<Exception>>>> c = b.walker();

    walkerHandler(c);
  }

  /* Triple(List<>, List<>, List<>) where triple first could be an empty list or triple second and third are empty lists or neither are empty lists. */
  private static void walkerHandler(Pair<Route, Result<Triple<LinkedList<Path>, LinkedList<Path>, LinkedList<Exception>>>> pairResult) {

    BooleanSupplierTriple<Path, Path, Exception> bst = new BooleanSupplierTriple<>(pairResult);

    boolean checkResult = pairResult.second().isSuccess();

    /*
     * +-----------+-----------+-------------+------------+
     * | p   q   r | p ^ q ^ r | p ^ ~q ^ ~r | ~p ^ q ^ r |
     * +-----------+-----------+-------------+------------+
     * | T | T | T |     T     |      F      |      F     |
     * +-----------+-----------+-------------+------------+
     * | T | T | F |     F     |      F      |      F     |
     * +-----------+-----------+-------------+------------+
     * | T | F | T |     F     |      F      |      F     |
     * +-----------+-----------+-------------+------------+
     * | T | F | F |     F     |      T      |      F     |
     * +-----------+-----------+-------------+------------+
     * | F | T | T |     F     |      F      |      T     |
     * +-----------+-----------+-------------+------------+
     * | F | T | F |     F     |      F      |      F     |
     * +-----------+-----------+-------------+------------+
     * | F | F | T |     F     |      F      |      F     |
     * +-----------+-----------+-------------+------------+
     * | F | F | F |     F     |      F      |      F     |
     * +-----------+-----------+-------------+------------+
     *
     * Where p = successes, q = failures, r = exceptions.
     */

    perhaps(checkResult,
        NOTHING,
        otiose ->
            match(
                /* default cause no: successes, failures, exceptions */
                cause(() -> {

                  var list = new LinkedList<>();

                  list.add("no successes no failures no exceptions");

                  list.add(bst.success().first());

                  return nationalRoute(new Pair<>(STR, new Pair<>("Walker", list)));
                }),

                /* p ^ q ^ r */
                cause(bst.hasSuccessesFailuresExceptions, () -> {

                  nationalRoute(new Pair<>(MOT, new Pair<>("Walker", bst.success().third())));

                  nationalRoute(new Pair<>(MOT, new Pair<>("Walker", bst.success().second())));

                  return replicate(bst.success().first());
                }),

                /* p ^ ~q ^ ~r */
                cause(bst.hasSuccessesNoFailuresExceptions, () -> replicate(bst.success().first())),

                /* ~p ^ q ^ r */
                cause(bst.noSuccessesHasFailuresExceptions, () -> {

                  nationalRoute(new Pair<>(MOT, new Pair<>("Walker", bst.success().third())));

                  return nationalRoute(new Pair<>(STR, new Pair<>("Walker", bst.success().second())));
                })
            ),

        otiose -> {

          var list = new LinkedList<Exception>();

          list.add(pairResult.second().failureValue());

          nationalRoute(new Pair<>(STR, new Pair<>("Walker", list)));
        });
  }

  private static Nothing replicate(@NotNull LinkedList<Path> list) {

    do {

      Path source = list.removeFirst();

      Pair<Route, Path> a = new Pair<>(C2, source);

      Replicate b = new Replicate(a);

      /*
       * The triple holds a one element list if the first list is empty the
       * other two hold the failure source file, and it's exception.
       */
      Pair<Route, Result<Triple<LinkedList<Pair<Path, Path>>, LinkedList<Path>, LinkedList<Exception>>>> c = b.replicate();

      replicateHandler(c);

      /*
       * Wait until lock is removed: InterchangeEmissionMOTHandler.emissionHandler()
       * compareAndSet when current value == expected value
       * then set the current value to new value
       * where expected value = 0, new value = 1.
       *
       * Oracle 2022, AtomicInteger.compareAndSet, java.util.concurrent.atomic, viewed 24 November 2022,
       *              <https://docs.oracle.com/en/java/javase/19/docs/api/java.base/java/util/concurrent/atomic/AtomicInteger.html#compareAndSet(int,int)>
       */
      while (!Route.lock.compareAndSet(0, 1)) { }

    } while (!list.isEmpty());

    return NOTHING;
  }

  /* Triple(List<>, List<>, List<>) where either triple first could be an empty list or triple second and third are empty lists. */
  private static void replicateHandler(Pair<Route, Result<Triple<LinkedList<Pair<Path, Path>>, LinkedList<Path>, LinkedList<Exception>>>> pairResult) {

    BooleanSupplierTriple<Pair<Path, Path>, Path, Exception> bst = new BooleanSupplierTriple<>(pairResult);

    boolean checkResult = pairResult.second().isSuccess();

    /*
     * Replicate.replicate() returns:
     *  has successes no failures no exceptions
     *   p ^ ~(q ^ r)
     *   T ^  (T ^ T)
     *   T ^ ~(F ^ F)
     *  no successes has failures has exceptions
     *   ~p ^ (q ^ r)
     *    T ^ (T ^ T)
     *   ~F ^ (T ^ T)
     *
     * +-----------+-------------+------------+
     * | p   q   r | p ^ ~q ^ ~r | ~p ^ q ^ r |
     * +-----------+-------------+------------+
     * | T | T | T |      F      |      F     |
     * +-----------+             |            |
     * | T | T | F |      F      |      F     |
     * +-----------+             |            |
     * | T | F | T |      F      |      F     |
     * +-----------+             |            |
     * | T | F | F |      T      |      F     |
     * +-----------+             |            |
     * | F | T | T |      F      |      T     |
     * +-----------+             |            |
     * | F | T | F |      F      |      F     |
     * +-----------+             |            |
     * | F | F | T |      F      |      F     |
     * +-----------+             |            |
     * | F | F | F |      F      |      F     |
     * +-----------+-------------+------------+
     *
     * Where p = successes, q = failures, r = exceptions.
     */

    perhaps(checkResult,
        NOTHING,
        otiose ->
            match(
                /* default cause no: successes, failures, exceptions */
                cause(() -> {

                  var list = new LinkedList<>();

                  list.add("no successes no failures no exceptions");

                  list.add(bst.success().first());

                  return nationalRoute(new Pair<>(STR, new Pair<>("Replicate", list)));
                }),

                /* p ^ ~q ^ ~r */
                cause(bst.hasSuccessesNoFailuresExceptions, () -> nationalRoute(new Pair<>(C3, bst.success().first()))),

                /* ~p ^ q ^ r */
                cause(bst.noSuccessesHasFailuresExceptions, () -> {

                  nationalRoute(new Pair<>(MOT, new Pair<>("Replicate", bst.success().third())));

                  return nationalRoute(new Pair<>(STR, new Pair<>("Replicate", bst.success().second())));
                })
            ),

        otiose -> {

          var list = new LinkedList<>();

          list.add(pairResult.second().failureValue());

          nationalRoute(new Pair<>(STR, new Pair<>("Replicate", list)));
        });
  }

}
