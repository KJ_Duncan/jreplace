package au.kjd.jreplace.handler;

import au.kjd.jreplace.cli.Cli;
import au.kjd.jreplace.model.From;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Patterns;
import au.kjd.jreplace.model.To;
import au.kjd.jreplace.model.Triple;

import java.nio.file.Path;
import java.nio.file.PathMatcher;
import java.util.Optional;

import static au.kjd.jreplace.controller.Controller.nationalRoute;
import static au.kjd.jreplace.controller.Controller.trailer;
import static au.kjd.jreplace.model.Route.A1;

// import java.util.logging.Logger;
// import static java.util.logging.Logger.getLogger;

/*
 * Handler for the Cli domain.
 *
 * A1->UC2 Cli command line entry point.
 */

public sealed abstract class CliHandler permits Cli {

  // private static final Logger logger = getLogger(CliHandler.class.getSimpleName());

  public static void cliHandler(final Path startingDirectory,
                                final int traversalDepth,
                                final PathMatcher syntax,
                                final String from,
                                final String to,
                                final long megaBytes,
                                final boolean replace,
                                final Optional<Path> output)
  {
    trailer(new Patterns(new From(from), new To(to)), megaBytes, replace, output);

    nationalRoute(new Pair<>(A1, new Triple<>(startingDirectory, traversalDepth, syntax)));
  }

}
