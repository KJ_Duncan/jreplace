package au.kjd.jreplace.handler;

import au.kjd.jreplace.controller.Controller;
import au.kjd.jreplace.handler.atjhandlers.AmanuensisHandler;
import au.kjd.jreplace.handler.atjhandlers.TheJustHandler;
import au.kjd.jreplace.model.Nothing;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Patterns;
import au.kjd.jreplace.model.Route;
import au.kjd.jreplace.model.Scan;
import au.kjd.jreplace.model.Triple;

import java.nio.file.Path;
import java.util.LinkedList;

import org.jetbrains.annotations.NotNull;

import static au.kjd.jreplace.model.Nothing.NOTHING;
import static au.kjd.jreplace.model.Route.D1;
import static au.kjd.jreplace.model.Route.D2;
import static au.kjd.jreplace.model.Route.D3;
import static au.kjd.jreplace.model.Route.D4;
import static au.kjd.jreplace.model.Route.D5;

/*
 * Handler for the AmanuensisHandler and TheJustHandler handlers.
 *
 * D1->M2 AmanuensisHandler amanuensisMatch
 * D2->N2 TheJustHandler aristides
 * D3->M4 AmanuensisHandler amanuensisProcess
 * D4->N4 TheJustHandler melendez
 */

public sealed abstract class ATJHandler permits AmanuensisHandler, TheJustHandler {

  private static Pair<Patterns, Long> localPair;

  public static void atjHandler(@NotNull Triple<Route, LinkedList<Pair<Path, Path>>, Pair<Patterns, Long>> triple) {

    localPair = triple.third();

    amanuensisHandler(triple.second());
  }

  private static void amanuensisHandler(LinkedList<Pair<Path, Path>> list) {

    Triple<Route, LinkedList<Pair<Path, Path>>, Patterns> a = new Triple<>(D1, list, localPair.first());

    AmanuensisHandler.amanuensisMatch(a);
  }

  @SuppressWarnings("unchecked")
  protected static <M> Nothing stateRoute(@NotNull Pair<Route, M> pair) {

    return switch (pair.first()) {

      case M2  -> { TheJustHandler.aristides(new Triple<>(D2, (LinkedList<Pair<Path, Path>>) pair.second(), localPair.second())); yield NOTHING; }

      case N2  -> { AmanuensisHandler.amanuensisProcess(new Triple<>(D3, (LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>) pair.second(), localPair.first())); yield NOTHING; }

      case M4  -> { TheJustHandler.melendez(new Pair<>(D4, (LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>) pair.second())); yield NOTHING; }

      case N4  -> { Controller.nationalRoute(new Pair<>(D5, (LinkedList<Triple<Path, Scan, Path>>) pair.second())); yield NOTHING; }

      case MOT, STR, VIC -> { Controller.nationalRoute(pair); yield NOTHING; }

      default -> NOTHING;

    };
  }

}
