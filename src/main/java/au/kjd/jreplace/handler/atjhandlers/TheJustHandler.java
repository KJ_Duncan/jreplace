package au.kjd.jreplace.handler.atjhandlers;

import au.kjd.jreplace.domain.Aristides;
import au.kjd.jreplace.domain.Melendez;
import au.kjd.jreplace.fpjava.Result;
import au.kjd.jreplace.handler.ATJHandler;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Route;
import au.kjd.jreplace.model.Scan;
import au.kjd.jreplace.model.Triple;

import java.nio.file.Path;
import java.util.LinkedList;

import org.jetbrains.annotations.NotNull;

import static au.kjd.jreplace.exceptional.ExceptionalUtilities.perhaps;
import static au.kjd.jreplace.fpjava.Case.cause;
import static au.kjd.jreplace.fpjava.Case.match;
import static au.kjd.jreplace.model.BooleanSuppliers.BooleanSupplierPair;
import static au.kjd.jreplace.model.BooleanSuppliers.BooleanSupplierTriple;
import static au.kjd.jreplace.model.Nothing.NOTHING;
import static au.kjd.jreplace.model.Route.N1;
import static au.kjd.jreplace.model.Route.N2;
import static au.kjd.jreplace.model.Route.N3;
import static au.kjd.jreplace.model.Route.N4;
import static au.kjd.jreplace.model.Route.STR;

/*
 * Handler for the Aristides and Melendez domains.
 *
 * N1->UC6 Aristides split a files content on given megabytes.
 * N3->UC8 Melendez consolidates a list of split files.
 */

public sealed abstract class TheJustHandler extends ATJHandler permits Aristides, Melendez {


  public static void aristides(@NotNull Triple<Route, LinkedList<Pair<Path, Path>>, Long> triple) {

    Aristides a = new Aristides(new Triple<>(N1, triple.second(), triple.third()));

    Pair<Route, Result<Triple<LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>> b = a.aristides();

    aristidesHandler(b);
  }

  /* Triple(List<>, List<>, List<>) where one parameter is an empty list. */
  private static void aristidesHandler(Pair<Route, Result<Triple<LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>> pairResult) {

    BooleanSupplierTriple<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>, Path, Exception> bst = new BooleanSupplierTriple<>(pairResult);

    boolean checkResult = pairResult.second().isSuccess();

    /*
     * +-----------+-------------+-------------+-------------+
     * | p   q   r | p ^ ~q ^ ~r | ~p ^ q ^ ~r | ~p ^ ~q ^ r |
     * +-----------+-------------+-------------+-------------+
     * | T | T | T |      F      |      F      |      F      |
     * +-----------+-------------+-------------+-------------+
     * | T | T | F |      F      |      F      |      F      |
     * +-----------+-------------+-------------+-------------+
     * | T | F | T |      F      |      F      |      F      |
     * +-----------+-------------+-------------+-------------+
     * | T | F | F |      T      |      F      |      F      |
     * +-----------+-------------+-------------+-------------+
     * | F | T | T |      F      |      F      |      F      |
     * +-----------+-------------+-------------+-------------+
     * | F | T | F |      F      |      T      |      F      |
     * +-----------+-------------+-------------+-------------+
     * | F | F | T |      F      |      F      |      T      |
     * +-----------+-------------+-------------+-------------+
     * | F | F | F |      F      |      F      |      F      |
     * +-----------+-------------+-------------+-------------+
     *
     * Where p = successes, q = failures, r = exceptions.
     */

    perhaps(checkResult,
        NOTHING,
        otiose ->
            match(
                /* default cause no: successes, failures, exceptions */
                cause(() -> {

                  var list = new LinkedList<>();

                  list.add("no successes no failures no exceptions");

                  list.add(bst.success());

                  return stateRoute(new Pair<>(STR, new Pair<>("Aristides", list)));
                }),

                /* p ^ ~q ^ ~r */
                cause(bst.hasSuccessesNoFailuresExceptions, () -> stateRoute(new Pair<>(N2, bst.success().first()))),

                /* ~p ^ q ^ ~r */
                cause(bst.noSuccessesExceptionsHasFailures, () -> stateRoute(new Pair<>(STR, new Pair<>("Aristides", bst.success().second())))),

                /* ~p ^ ~q ^ r */
                cause(bst.noSuccessesFailuresHasExceptions, () -> stateRoute(new Pair<>(STR, new Pair<>("Aristides", bst.success().third()))))
            ),

        otiose -> {

          var list = new LinkedList<Exception>();

          list.add(pairResult.second().failureValue());

          stateRoute(new Pair<>(STR, new Pair<>("Aristides", list)));
    });
  }

  public static void melendez(@NotNull Pair<Route, LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>> pair) {

    Pair<Route, LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>> a = new Pair<>(N3, pair.second());

    Melendez b = new Melendez(a);

    Pair<Route, Result<Pair<LinkedList<Triple<Path, Scan, Path>>, LinkedList<Object>>>> c = b.melendez();

    melendezHandler(c);
  }

  /* Pair<List<>, List<>> holds one element list either success or failure */
  private static void melendezHandler(Pair<Route, Result<Pair<LinkedList<Triple<Path, Scan, Path>>, LinkedList<Object>>>> pairResult) {

    BooleanSupplierPair<Triple<Path, Scan, Path>, Object> bsp = new BooleanSupplierPair<>(pairResult);

    boolean checkResult = pairResult.second().isSuccess();

    /*
     * +-------+-----------------+
     * | p   q | p ^ ~q | ~p ^ q |
     * +-------+-----------------+
     * | T | T |   F    |   F    |
     * +-------+-----------------+
     * | T | F |   T    |   F    |
     * +-------+-----------------+
     * | F | T |   F    |   T    |
     * +-------+-----------------+
     * | F | F |   F    |   F    |
     * +-------+-----------------+
     *
     * Where p = successes, q = failures.
     */

    perhaps(checkResult,
        NOTHING,
        otiose ->
            match(
                /* default case no: successes, failures */
                cause(() -> {
                  var list = new LinkedList<>();

                  list.add("no successes no failures");

                  list.add(bsp.success().first());

                  return stateRoute(new Pair<>(STR, new Pair<>("Melendez", list)));
                }),

                /* p ^ ~q */
                cause(bsp.hasSuccessNoFailures, () -> stateRoute(new Pair<>(N4, bsp.success().first()))),

                /* ~p ^ q */
                cause(bsp.noSuccessesHasFailures, () -> stateRoute(new Pair<>(STR, new Pair<>("Melendez", bsp.success().second()))))
            ),

        otiose -> {

          var list = new LinkedList<Exception>();

          list.add(pairResult.second().failureValue());

          stateRoute(new Pair<>(STR, new Pair<>("Melendez", list)));
    });
  }

}
