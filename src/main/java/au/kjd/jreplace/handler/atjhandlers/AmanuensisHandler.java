package au.kjd.jreplace.handler.atjhandlers;

import au.kjd.jreplace.domain.AmanuensisMatch;
import au.kjd.jreplace.domain.AmanuensisProcess;
import au.kjd.jreplace.fpjava.Result;
import au.kjd.jreplace.handler.ATJHandler;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Patterns;
import au.kjd.jreplace.model.Route;
import au.kjd.jreplace.model.Scan;
import au.kjd.jreplace.model.Triple;

import java.nio.file.Path;
import java.util.LinkedList;

import org.jetbrains.annotations.NotNull;

import static au.kjd.jreplace.exceptional.ExceptionalUtilities.perhaps;
import static au.kjd.jreplace.fpjava.Case.cause;
import static au.kjd.jreplace.fpjava.Case.match;
import static au.kjd.jreplace.model.BooleanSuppliers.BooleanSupplierTriple;
import static au.kjd.jreplace.model.Nothing.NOTHING;
import static au.kjd.jreplace.model.Route.M1;
import static au.kjd.jreplace.model.Route.M2;
import static au.kjd.jreplace.model.Route.M3;
import static au.kjd.jreplace.model.Route.M4;
import static au.kjd.jreplace.model.Route.MOT;
import static au.kjd.jreplace.model.Route.STR;
import static au.kjd.jreplace.model.Route.VIC;

/*
 * Handler for the AmanuensisMatch and AmanuensisProcess domains.
 *
 * M1->UC5 AmanuensisMatch whether any elements of match the provided predicate.
 * M3->UC7 AmanuensisProcess performs match operations on a character sequence by interpreting a Pattern.
 */

public sealed abstract class AmanuensisHandler extends ATJHandler permits AmanuensisMatch, AmanuensisProcess {


  public static void amanuensisMatch(@NotNull Triple<Route, LinkedList<Pair<Path, Path>>, Patterns> triple) {

    AmanuensisMatch a = new AmanuensisMatch(new Triple<>(M1, triple.second(), triple.third()));

    Pair<Route, Result<Triple<LinkedList<Pair<Path, Path>>, LinkedList<Path>, LinkedList<Exception>>>> b = a.amanuensisMatch();

    amanuensisMatchHandler(b);
  }

  /* Triple(List<>, List<>, List<>) where one parameter is an empty list. */
  private static void amanuensisMatchHandler(@NotNull Pair<Route, Result<Triple<LinkedList<Pair<Path, Path>>, LinkedList<Path>, LinkedList<Exception>>>> pairResult) {

    BooleanSupplierTriple<Pair<Path, Path>, Path, Exception> bst = new BooleanSupplierTriple<>(pairResult);

    boolean checkResult = pairResult.second().isSuccess();

    /*
     * +-----------+-------------+-------------+-------------+
     * | p   q   r | p ^ ~q ^ ~r | ~p ^ q ^ ~r | ~p ^ ~q ^ r |
     * +-----------+-------------+-------------+-------------+
     * | T | T | T |      F      |      F      |      F      |
     * +-----------+-------------+-------------+-------------+
     * | T | T | F |      F      |      F      |      F      |
     * +-----------+-------------+-------------+-------------+
     * | T | F | T |      F      |      F      |      F      |
     * +-----------+-------------+-------------+-------------+
     * | T | F | F |      T      |      F      |      F      |
     * +-----------+-------------+-------------+-------------+
     * | F | T | T |      F      |      F      |      F      |
     * +-----------+-------------+-------------+-------------+
     * | F | T | F |      F      |      T      |      F      |
     * +-----------+-------------+-------------+-------------+
     * | F | F | T |      F      |      F      |      T      |
     * +-----------+-------------+-------------+-------------+
     * | F | F | F |      F      |      F      |      F      |
     * +-----------+-------------+-------------+-------------+
     *
     * Where p = successes, q = failures, r = exceptions.
     */

    perhaps(checkResult,
        NOTHING,
        otiose ->
            match(
                /* default cause no: successes, failures, exceptions */
                cause(() -> {

                    var list = new LinkedList<>();

                    list.add("no successes no failures no exceptions");

                    list.add(bst.success());

                    return stateRoute(new Pair<>(STR, new Pair<>("AmanuensisMatch", list)));
                }),

                /* p ^ ~q ^ ~r */
                cause(bst.hasSuccessesNoFailuresExceptions, () -> stateRoute(new Pair<>(M2, bst.success().first()))),

                /* ~p ^ q ^ ~r */
                cause(bst.noSuccessesExceptionsHasFailures, () -> stateRoute(new Pair<>(VIC, new Pair<>("AmanuensisMatch", bst.success().second())))),

                /* ~p ^ ~q ^ r */
                cause(bst.noSuccessesFailuresHasExceptions, () -> stateRoute(new Pair<>(STR, new Pair<>("AmanuensisMatch", bst.success().third()))))
            ),

        otiose -> {

          var list = new LinkedList<>();

          list.add(pairResult.second().failureValue());

          stateRoute(new Pair<>(STR, new Pair<>("AmanuensisMatch", list)));
        });
  }

  public static void amanuensisProcess(@NotNull Triple<Route, LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>, Patterns> triple) {

    Triple<Route, LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>, Patterns> a = new Triple<>(M3, triple.second(), triple.third());

    AmanuensisProcess b = new AmanuensisProcess(a);

    Pair<Route, Result<Triple<LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>> c = b.amanuensisProcess();

    amanuensisProcessHandler(c);
  }

  /* Triple(List<>, List<>, List<>) where triple third exceptions is always an empty list, kept for code base consistency. */
  private static void amanuensisProcessHandler(@NotNull Pair<Route, Result<Triple<LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>> pairResult) {

    BooleanSupplierTriple<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>, Path, Exception> bst = new BooleanSupplierTriple<>(pairResult);

    boolean checkResult = pairResult.second().isSuccess();

    /*
     * +-----------+----------------------------------------+
     * | p   q   r | p ^ q ^ ~r | p ^ ~q ^ ~r | ~p ^ q ^ ~r |
     * +-----------+----------------------------------------+
     * | T | T | T |     F      |      F      |      F      |
     * +-----------+----------------------------------------+
     * | T | T | F |     T      |      F      |      F      |
     * +-----------+----------------------------------------+
     * | T | F | T |     F      |      F      |      F      |
     * +-----------+----------------------------------------+
     * | T | F | F |     F      |      T      |      F      |
     * +-----------+----------------------------------------+
     * | F | T | T |     F      |      F      |      F      |
     * +-----------+----------------------------------------+
     * | F | T | F |     F      |      F      |      T      |
     * +-----------+----------------------------------------+
     * | F | F | T |     F      |      F      |      F      |
     * +-----------+----------------------------------------+
     * | F | F | F |     F      |      F      |      F      |
     * +-----------+----------------------------------------+
     *
     * Where p = successes, q = failures, r = exceptions.
     */

    perhaps(checkResult,
        NOTHING,
        otiose ->
            match(
                /* default cause no: successes, failures, exceptions */
                cause(() -> {

                  var list = new LinkedList<>();

                  list.add("no successes no failures no exceptions");

                  list.add(bst.success());

                  return stateRoute(new Pair<>(STR, new Pair<>("AmanuensisProcess", list)));
                }),

                /* p ^ q ^ ~r */
                cause(bst.hasSuccessesFailuresNoExceptions, () -> {

                  stateRoute(new Pair<>(MOT, new Pair<>("AmanuensisProcess", bst.success().second())));

                  return stateRoute(new Pair<>(M4, bst.success().first()));
                }),

                /* p ^ ~q ^ ~r */
                cause(bst.hasSuccessesNoFailuresExceptions, () -> stateRoute(new Pair<>(M4, bst.success().first()))),

                /* ~p ^ q ^ ~r */
                cause(bst.noSuccessesExceptionsHasFailures, () -> stateRoute(new Pair<>(STR, new Pair<>("AmanuensisProcess", bst.success().second()))))
            ),

        otiose -> {

          var list = new LinkedList<>();

          list.add(pairResult.second().failureValue());

          stateRoute(new Pair<>(STR, new Pair<>("AmanuensisProcess", list)));
        });
  }

}
