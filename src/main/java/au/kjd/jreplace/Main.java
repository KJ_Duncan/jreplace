package au.kjd.jreplace;

import au.kjd.jreplace.cli.Cli;
import org.jetbrains.annotations.Contract;
import org.jetbrains.annotations.NotNull;
import picocli.CommandLine;

import static java.lang.System.out;


/**
 *
 *                                                                                           ,-----------------.            ,---------.
 *                                                                                           |AmanuensisProcess|            |Aristides|
 *                                                                                           |                 |            |         |
 *                                                                                           `-----------------'            `---------'
 *                                                                                                UC7 ^                      UC6 ^
 *                                                                                                    |                          |
 *                                                                                                    |                          |
 *                                                                                                    |                          |
 *                                                                                                 M3 v                       N1 v
 *                                                               ,---------------.           ,-----------------.         ,--------------.            ,--------.
 *                                                               |AmanuensisMatch| UC5    M1 |AmanuensisHandler|         |TheJustHandler| N3     UC8 |Melendez|
 *                                                               |               |<--------->|                 |         |              |<---------->|        |
 *                                                               `---------------'           `-----------------'         `--------------'            `--------'
 *                                                                                            M2 ^     M4 ^               ^ N4      N2 ^
 *                                                                                               |        |               |            |
 *                                                                                               |        |               |            |
 *                                                                                               |        |               |            |
 *     ,-------.  ,--------.  ,-----------.             ,---------.      ,------.                |        |               |            |
 *     |Minitru|  |Emission|  |Interchange|             |Replicate|      |Walker|                |        |               |            |
 *     |       |  |        |  |           |             |         |      |      |                |        |               |            |
 *     `-------'  `--------'  `-----------'             `---------'      `------'                |        |               |            |
 *    UC11 x      UC10 x             ^ UC9                UC4 ^              ^ UC3               |        |               |            |
 *         |           |             |                        |              |                   |        |               |            |
 *         |           |             |                        |              |                   |        |               |            |
 *      E3 |        E2 |             v E1                  C2 v              v C1             D1 v     D3 v               |            |
 *         ,-----------------------------.                ,----------------------.              ,----------.<-------------+            |
 *         |InterchangeEmissionMOTHandler|                |WalkerReplicateHandler|              |ATJHandler| D4                        |
 *         |                             |                |                      |              |          |                           |
 *         `-----------------------------'                `----------------------'              `----------'<--------------------------+
 *                      ^                                      C3 ^                             D5 ^      D2
 *                      |                                         |                                |
 *                      |                                         |                                |
 *                      |                                      B1 v                                |
 *                      |                                    ,----------.                          |
 *                      |                                 B3 |Controller| B2                       |
 *                      +------------------------------------|          |<-------------------------+
 *                                                           `----------'
 *                                                                ^
 *                                                                |
 *                                                                |
 *                                                             A1 |
 *                                                           ,----------.
 *                                                           |CliHandler|
 *                                                           |          |
 *                                                           `----------'
 *                                                                ^
 *                                                                |
 *                                                                |
 *                                                            UC2 |
 *                                                              ,---.
 *                                                              |Cli|
 *                                                              |   |
 *                                                              `---'
 *                                                                ^
 *                                                                |
 *                                                                |
 *                                                            UC1 |
 *                                                              ,----.
 *                                                              |Main|
 *                                                              |    |
 *                                                              `----'
 * <p>
 *
 *
 * Command line entry point see Cli.java.
 * <p>
 * Picocli 2022, picocli - a mighty tiny command line interface,
 *               version 4.6.3, viewed 28 September 2022,
 *               <https://picocli.info>
 * <p>
 * Usage: jreplace [--[no-]replace] [--path=directory] [--depth=int] --from=text --to=text (--regex=expression | --glob=pattern) [--megabytes=int]
 * <p>
 *
 */
final class Main {

  @Contract(pure = true)
  private Main() { }

  public static void main(String @NotNull [] args) {

    try { new CommandLine(new Cli()).execute(args); }

    // to console without the stacktrace
    catch (Exception e) { out.printf("jreplace an unchecked exception was thrown: %s%n", e.toString()); }
  }

}
