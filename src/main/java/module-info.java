module au.kjd.jreplace {
  requires java.base;
  requires java.logging;

  // cli java flight recorder
  // requires jdk.jfr;

  // static used for compile time only
  requires static org.jetbrains.annotations;
  requires info.picocli;

  opens au.kjd.jreplace.cli to info.picocli;
}
