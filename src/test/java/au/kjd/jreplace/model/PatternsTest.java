package au.kjd.jreplace.model;

import java.util.regex.Pattern;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.junit.jupiter.api.Assertions.assertEquals;


class PatternsTest {

  @Test
  void test_regex_from_and_regex_to_patterns() {

    assertDoesNotThrow(() -> new Patterns(new From("syntax"), new To("syntax")));

    var patterns = new Patterns(new From("syntax"), new To("syntax"));

    assertEquals("syntax", patterns.getFrom());
    assertEquals("syntax", patterns.getTo());
    assertTrue(patterns.getRegexFrom().matcher("syntax").find());
    assertTrue(patterns.getRegexTo().matcher("syntax").find());
  }

  @Test
  void test_capture_group_split_as_stream_returns_4() {

    var sentence = """
        voluhasptaria hass yu has.
        """;

    var pattern = Pattern.compile("(?m)(has)");
    var stream = pattern.splitAsStream(sentence);

    // 3 matches plus the 1 match group
    assertEquals(4, stream.count());
  }

  @Test
  void test_newline_index_of_string() {

    var sentence1 = "newline\n";
    var sentence2 = "ne";

    assertEquals(8, sentence1.length());
    assertEquals(7, sentence1.indexOf("\n"));
    assertEquals(2, sentence2.length());
    assertEquals(-1, sentence2.indexOf("\n"));
  }
}
