package au.kjd.jreplace.domain;

import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.RegexFrom;
import au.kjd.jreplace.model.RegexTo;
import au.kjd.jreplace.model.Scan;
import au.kjd.jreplace.model.Triple;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.concurrent.atomic.LongAdder;
import java.util.LinkedList;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestClassOrder;

import static au.kjd.jreplace.model.Route.TS;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.io.CleanupMode.ALWAYS;


@TestClassOrder(ClassOrderer.OrderAnnotation.class)
class MelendezTest implements TempFileProducer {

//  @AfterAll
//  static void teardown() { Replicate.deleteJreplaceTempDirectory(); }

  @Nested
  @Order(1)
  class Melendez_Make_It {

    // Pair<Route, LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>>

    @Test
    void given_5_files_should_return_list_of_triples_size_5_and_0_failures(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      var listOfPairPaths = pullout29KB(tempDir);

      assertEquals(5, listOfPairPaths.size());

      var targetPointerScanSource = new LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>();

      var listOfPathSizes = new LinkedList<Pair<Path, Long>>();

      do {

        var scan = new Scan(new RegexFrom(new LongAdder()), new RegexTo(new LongAdder()));

        var triples = new LinkedList<Triple<Path, Long, Scan>>();

        var pairPaths = listOfPairPaths.removeFirst();

        listOfPathSizes.add(new Pair<>(pairPaths.first(), Files.size(pairPaths.first())));

        scan.regexFrom().longValue().increment();
        scan.regexTo().longValue().increment();

        triples.add(new Triple<>(pairPaths.first(), 0L, scan));

        var pair = new Pair<>(triples, pairPaths.second());

        targetPointerScanSource.add(pair);

        scan = null;
        triples = null;
        pairPaths = null;
        pair = null;

      } while (!listOfPairPaths.isEmpty());

      // -------------- Melendez ------------------- \\
      var melendezPair = new Pair<>(TS, targetPointerScanSource);
      var melendezObj = new Melendez(melendezPair);

      // Pair<Route, Result<Pair<LinkedList<Triple<Path, Scan, Path>>, LinkedList<Object>>>>
      var generated = melendezObj.melendez();

      assertTrue(generated.second().isSuccess());

      var pairs = generated.second().successValue();

      assertFalse(pairs.first().isEmpty());
      assertTrue(pairs.second().isEmpty());
      assertEquals(5, pairs.first().size());

      var linkedList = pairs.first();

      do {

        var triple = linkedList.removeFirst();
        var pathSizes = listOfPathSizes.removeFirst();

        var length = pathSizes.first().getFileName().toString().length();

        assertEquals(pathSizes.first().getFileName().toString(), triple.first().getFileName().toString().substring(0, length));
        assertEquals(pathSizes.second(), Files.size(triple.first()));
        assertEquals(1, triple.second().regexFrom().longValue().sumThenReset());
        assertEquals(1, triple.second().regexTo().longValue().sumThenReset());

        triple = null;
        pathSizes = null;

      } while (!linkedList.isEmpty());
    }
  }

  @Nested
  @Order(2)
  class Melendez_Break_It {

    @Test
    void given_empty_list_should_return_is_failure() {

      var targetPointerScanSource = new LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>();

      var melendezPair = new Pair<>(TS, targetPointerScanSource);
      var melendezObj = new Melendez(melendezPair);

      // Result<Pair<LinkedList<Triple<Path, Scan, Path>>, LinkedList<Object>>>
      var generated = melendezObj.melendez();

      assertTrue(generated.second().isFailure());
      assertInstanceOf(IllegalStateException.class, generated.second().failureValue());
    }

    @Test
    void given_list_of_null_should_return_list_of_failures_size_1_IllegalStateException() {

      var targetPointerScanSource = new LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>();

      targetPointerScanSource.add(null);

      var melendezPair = new Pair<>(TS, targetPointerScanSource);
      var melendezObj = new Melendez(melendezPair);


      // Result<Pair<LinkedList<Triple<Path, Scan, Path>>, LinkedList<Object>>>
      var generated = melendezObj.melendez();

      assertTrue(generated.second().isSuccess());
      assertTrue(generated.second().successValue().first().isEmpty());
      assertEquals(1, generated.second().successValue().second().size());
      assertInstanceOf(IllegalStateException.class, generated.second().successValue().second().removeFirst());
    }

    @Test
    void given_triple_first_null_should_return_list_of_failures_size_5_IllegalStateException() {

      var scan = new Scan(new RegexFrom(new LongAdder()), new RegexTo(new LongAdder()));

      var targetPointerScanSource = new LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>();

      int counter = 0;

      do {

        var triples = new LinkedList<Triple<Path, Long, Scan>>();

        scan.regexFrom().longValue().increment();
        scan.regexTo().longValue().increment();

        triples.add(new Triple<>(null, 0L, scan));

        var pair = new Pair<LinkedList<Triple<Path, Long, Scan>>, Path>(triples, null);

        targetPointerScanSource.add(pair);

        triples = null;
        pair = null;

        counter++;

      } while (counter < 5);

      counter = 0;

      var melendezPair = new Pair<>(TS, targetPointerScanSource);
      var melendezObj = new Melendez(melendezPair);

      // Result<Pair<LinkedList<Triple<Path, Scan, Path>>, LinkedList<Object>>>
      var generated = melendezObj.melendez();

      assertTrue(generated.second().isSuccess());

      var pairs = generated.second().successValue();

      assertTrue(pairs.first().isEmpty());
      assertEquals(5, pairs.second().size());
      assertInstanceOf(IllegalStateException.class, pairs.second().removeFirst());
    }
  }
}
