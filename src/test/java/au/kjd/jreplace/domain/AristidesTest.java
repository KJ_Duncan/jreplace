package au.kjd.jreplace.domain;

import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Route;
import au.kjd.jreplace.model.Triple;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.LinkedList;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestClassOrder;

import static au.kjd.jreplace.model.Route.TS;

import static au.kjd.jreplace.domain.DomainObjectsAndMethods.AristidesMethodsImpl.AristidesFractionaliseTest.isGreaterThanIntegerMaxValueTest;
import static au.kjd.jreplace.domain.DomainObjectsAndMethods.AristidesMethodsImpl.AristidesFractionaliseTest.nTimesGreaterTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.io.CleanupMode.ALWAYS;


@TestClassOrder(ClassOrderer.OrderAnnotation.class)
class AristidesTest implements TempFileProducer {

//  @AfterAll
//  static void teardown() { Replicate.deleteJreplaceTempDirectory(); }

  @Nested
  @Order(1)
  class Aristides_Make_It {

    @Test
    void given_values_is_greater_than_integer_max_value_should_be_false_then_true() {

      long value = (long) Integer.MAX_VALUE;
      long valuePlusOne = value + 1L;

      assertFalse(isGreaterThanIntegerMaxValueTest(value));

      assertTrue(isGreaterThanIntegerMaxValueTest(valuePlusOne));
    }

    @Test
    void given_values_n_times_greater_should_return_1_2_3() {

      long value = (long) Integer.MAX_VALUE;
      long valueMinusOne = value - 1L;
      long valuePlusOne = value + 1L;
      long valueTimesTwo = value * 2L;
      long valueTimesTwoPlusOne = valueTimesTwo + 1L;

      assertEquals(1, nTimesGreaterTest(valueMinusOne));

      assertEquals(1, nTimesGreaterTest(value));

      assertEquals(2, nTimesGreaterTest(valuePlusOne));

      assertEquals(2, nTimesGreaterTest(valueTimesTwo));

      assertEquals(3, nTimesGreaterTest(valueTimesTwoPlusOne));
    }

    // when there are zero splits we are copying target to temp file
    @Test
    void given_5_files_with_default_split_size_0_should_return_list_size_5(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      var listOfPairPaths = pullout29KB(tempDir);

      assertEquals(5, listOfPairPaths.size());

      var aristidesObj = new Aristides(new Triple<>(TS, listOfPairPaths, 0L));

      // Pair<Route, Result<Triple<LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>>
      var process = aristidesObj.aristides();
      var result = process.second();

      assertTrue(result.isSuccess());

      // Triple<LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>, LinkedList<Path>, LinkedList<Exception>>
      var triple = result.successValue();

      // LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>
      var llist = triple.first();

      assertEquals(5, llist.size());


      llist.forEach(pair -> assertEquals(1, pair.first().size()));

      Path original = Paths.get("src/test/resources/Latin-Lipsum.txt");

      do {

        // Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>
        var pair = llist.removeFirst();
        var path = pair.first().removeFirst().first();

        assertLinesMatch(Files.lines(original), Files.lines(path));

        assertTrue(Replicate.deleteTempFile(path).successValue());

        pair = null;
        path = null;

      } while (!llist.isEmpty());
    }

    @Test
    void given_5_files_with_split_size_1_should_return_outer_list_size_5_and_inner_list_size_3(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      var listOfPairPaths = pullout2800KB(tempDir);

      assertEquals(5, listOfPairPaths.size());


      var aristidesObj = new Aristides(new Triple<>(TS, listOfPairPaths, 1L));

      // Pair<Route, Result<Triple<LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>>
      var process = aristidesObj.aristides();
      var result = process.second();

      assertTrue(result.isSuccess());

      // Triple<LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>, LinkedList<Path>, LinkedList<Exception>>
      var triple = result.successValue();

      assertFalse(triple.first().isEmpty());
      assertEquals(0, triple.second().size());
      assertEquals(0, triple.third().size());

      // LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>
      var llist = triple.first();

      // outer list is the amount of files
      assertEquals(5, llist.size());

      // inner list is the amount of split files
      llist.forEach(pair -> assertEquals(3, pair.first().size()));

      Path original = Paths.get("src/test/resources/Latin-Lipsum.txt");

      String contents = Files.readString(original);

      do {

        // Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>
        var outerpair = llist.removeFirst();
        var listSizeThree = outerpair.first();

        assertEquals(3, listSizeThree.size());

        var innerpair01 = listSizeThree.removeFirst();
        var innerpair02 = listSizeThree.removeFirst();
        var innerpair03 = listSizeThree.removeFirst();

        var path1 = innerpair01.first();
        var path2 = innerpair02.first();
        var path3 = innerpair03.first();

        var position0 = innerpair01.second().removeFirst();
        var position1 = innerpair02.second().removeFirst();
        var position2 = innerpair03.second().removeFirst();

        assertEquals(0L, position0);
        assertEquals(1L, position1);
        assertEquals(2L, position2);

        assertTrue(Files.readString(path1).contains(contents));
        assertTrue(Files.readString(path2).contains(contents));
        assertTrue(Files.readString(path3).contains(contents));

        assertTrue(Replicate.deleteTempFile(path1).successValue());
        assertTrue(Replicate.deleteTempFile(path2).successValue());
        assertTrue(Replicate.deleteTempFile(path3).successValue());

        outerpair = null;
        listSizeThree = null;
        path1 = null;
        path2 = null;
        path3 = null;

      } while (!llist.isEmpty());
    }
  }

  @Nested
  @Order(2)
  class Aristides_Break_It {

    @Test
    void given_empty_list_off_paths_and_default_split_size_should_return_a_triple_off_empty_lists() {

      var listOfPairPaths = new LinkedList<Pair<Path, Path>>();

      var aristidesObj = new Aristides(new Triple<>(TS, listOfPairPaths, 0L));

      // Pair<Route, Result<Triple<LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>>
      var process = aristidesObj.aristides();
      var result = process.second();

      assertTrue(result.isSuccess());

      var triple = result.successValue();

      assertEquals(0, triple.first().size());
      assertEquals(0, triple.second().size());
      assertEquals(0, triple.third().size());
    }

    @Test
    void given_empty_list_off_paths_and_split_size_1_should_return_a_triple_off_empty_lists() {

      var listOfPairPaths = new LinkedList<Pair<Path, Path>>();

      var aristidesObj = new Aristides(new Triple<>(TS, listOfPairPaths, 1L));

      // Pair<Route, Result<Triple<LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>>
      var process = aristidesObj.aristides();
      var result = process.second();

      assertTrue(result.isSuccess());

      var triple = result.successValue();

      assertEquals(0, triple.first().size());
      assertEquals(0, triple.second().size());
      assertEquals(0, triple.third().size());
    }

      // System.out.println("triple first -> " + triple.first() + " triple second -> " + triple.second() + " triple third -> " + triple.third());
    @Test
    void given_list_off_null_should_return_a_triple_1_and_2_empty_lists_and_3rd_list_off_a_IllegalStateException() {

      var listOfPairPaths = new LinkedList<Pair<Path, Path>>();

      listOfPairPaths.add(null);

      var aristidesObj = new Aristides(new Triple<>(TS, listOfPairPaths, 0L));

      // Pair<Route, Result<Triple<LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>>
      var process = aristidesObj.aristides();
      var result = process.second();

      assertTrue(result.isSuccess());

      var triple = result.successValue();

      assertEquals(0, triple.first().size());
      assertEquals(0, triple.second().size());
      assertEquals(1, triple.third().size());

      assertInstanceOf(IllegalStateException.class, triple.third().removeFirst());
    }

    @Test
    void given_5_files_with_negative_split_size_should_return_list_size_5(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      var listOfPairPaths = pullout29KB(tempDir);

      assertEquals(5, listOfPairPaths.size());

      var aristidesObj = new Aristides(new Triple<>(TS, listOfPairPaths, -1L));

      // Pair<Route, Result<Triple<LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>>
      var process = aristidesObj.aristides();
      var result = process.second();

      assertTrue(result.isSuccess());

      var triple = result.successValue();

      assertEquals(5, triple.first().size());
      assertEquals(0, triple.second().size());
      assertEquals(0, triple.third().size());
    }

    // NOTE: new Triple<Route, LinkedList<Pair<Path, Path>>, Long>(null, null, null);
    //       expose: java.lang.NullPointerException: Cannot invoke "java.lang.Long.longValue()" because the return value of "au.kjd.jreplace.model.Triple.third()" is null
    @Test
    void given_triple_null_values_should_be_result_is_failure() {

      // new Triple<>(TS, listOfPairPaths, 0L)
      var triple = new Triple<Route, LinkedList<Pair<Path, Path>>, Long>(null, null, 0L);

      var aristidesObj = new Aristides(triple);

      // Pair<Route, Result<Triple<LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>>
      var process = aristidesObj.aristides();
      var result = process.second();

      assertTrue(result.isFailure());
      assertInstanceOf(IllegalStateException.class, result.failureValue());
    }

  }

}
