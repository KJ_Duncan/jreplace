package au.kjd.jreplace.domain;

import au.kjd.jreplace.model.From;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Patterns;
import au.kjd.jreplace.model.Scan;
import au.kjd.jreplace.model.To;
import au.kjd.jreplace.model.Triple;
import au.kjd.jreplace.model.Tuple;

import java.io.IOException;
import java.lang.String;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Stream;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestClassOrder;

import static au.kjd.jreplace.domain.Replicate.deleteTempFile;
import static au.kjd.jreplace.model.Route.TS;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.io.CleanupMode.ALWAYS;


@TestClassOrder(ClassOrderer.OrderAnnotation.class)
class AmanuensisProcessTest implements TempFileProducer {

//  @AfterAll
//  static void teardown() { Replicate.deleteJreplaceTempDirectory(); }

  @Nested
  @Order(1)
  class AmanuensisProcess_Make_It {

    @Test
    void given_5_files_each_split_into_3_separate_files_should_return_list_triple_path_long_scan(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      var listOfPairPaths = pullout2800KB(tempDir);

      assertEquals(5, listOfPairPaths.size());

      // ---------------- Aristides -------------------- \\
      // Triple<Integer, LinkedList<Pair<Path, Path>>, Long>
      var aristidesObj = new Aristides(new Triple<>(TS, listOfPairPaths, 1L));
      var process = aristidesObj.aristides();
      var resultAristides = process.second();

      assertTrue(resultAristides.isSuccess());

      // Triple<LinkedList<Pair<LinkedList<Pair<Path, Long>>, Path>>, LinkedList<Path>, LinkedList<?>>
      var tripleAristides = resultAristides.successValue();

      assertFalse(tripleAristides.first().isEmpty());
      assertTrue(tripleAristides.second().isEmpty());
      assertTrue(tripleAristides.third().isEmpty());

      // LinkedList<Pair<LinkedList<Pair<Path, Long>>, Path>>
      var llist = tripleAristides.first();

      // outer list is the amount of files
      assertEquals(5, llist.size());

      // ---------------- AmanuensisProcess -------------------- \\
      // Triple<Route, LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>, Patterns>
      var patterns = new Patterns(new From("a"), new To("j"));
      var tripleAmanuensis = new Triple<>(TS, llist, patterns);

      var amanuensisProcessObj = new AmanuensisProcess(tripleAmanuensis);

      // Pair<Route, Result<Triple<LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>>
      var pair = amanuensisProcessObj.amanuensisProcess();

      var result = pair.second();

      assertTrue(result.isSuccess());

      // Triple<LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>, LinkedList<Path>, LinkedList<Exception>>
      var triple = result.successValue();

      assertFalse(triple.first().isEmpty());
      assertTrue(triple.second().isEmpty());
      assertTrue(triple.third().isEmpty());

      var listPair = triple.first();

      assertEquals(5, listPair.size());
      assertEquals(3, Objects.requireNonNull(listPair.peekFirst()).first().size());

      do {

        // Pair<LinkedList<Triple<Path, Long, Scan>>, Path>
        var pairList = listPair.removeFirst();
        var source = pairList.second();
        var tripleList = pairList.first();

        var triple01 = tripleList.removeFirst();
        var triple02 = tripleList.removeFirst();
        var triple03 = tripleList.removeFirst();

        assertEquals(0L, triple01.second());
        assertEquals(1L, triple02.second());
        assertEquals(2L, triple03.second());

        assertEquals(67583L, triple01.third().regexFrom().longValue().sumThenReset());
        assertEquals(67577L, triple02.third().regexFrom().longValue().sumThenReset());
        assertEquals(54013L, triple03.third().regexFrom().longValue().sumThenReset());

        assertEquals(0L, triple01.third().regexTo().longValue().sumThenReset());
        assertEquals(0L, triple02.third().regexTo().longValue().sumThenReset());
        assertEquals(0L, triple03.third().regexTo().longValue().sumThenReset());

      } while (!listPair.isEmpty());

      /*
        Triple[first=/tmp/jreplace123762773109394197/lipsum.txt16946451699475599212.tmp11652578850791548297.tmp, second=0, third=Scan[regexFrom=RegexFrom[longValue=67583], regexTo=RegexTo[longValue=0]]]
        Triple[first=/tmp/jreplace123762773109394197/lipsum.txt3811910176227809061.tmp6913297867661363923.tmp, second=1, third=Scan[regexFrom=RegexFrom[longValue=67577], regexTo=RegexTo[longValue=0]]]
        Triple[first=/tmp/jreplace123762773109394197/lipsum.txt10292551561492131218.tmp6116863858195113559.tmp, second=2, third=Scan[regexFrom=RegexFrom[longValue=54013], regexTo=RegexTo[longValue=0]]]

        Triple[first=/tmp/jreplace123762773109394197/lipsum_01.txt3218210207260255093.tmp16304726299634841011.tmp, second=0, third=Scan[regexFrom=RegexFrom[longValue=67583], regexTo=RegexTo[longValue=0]]]
        Triple[first=/tmp/jreplace123762773109394197/lipsum_01.txt14749336783436237207.tmp6010211138754289199.tmp, second=1, third=Scan[regexFrom=RegexFrom[longValue=67577], regexTo=RegexTo[longValue=0]]]
        Triple[first=/tmp/jreplace123762773109394197/lipsum_01.txt2263075696708198442.tmp14957710849707301286.tmp, second=2, third=Scan[regexFrom=RegexFrom[longValue=54013], regexTo=RegexTo[longValue=0]]]

        Triple[first=/tmp/jreplace123762773109394197/lipsum_02.txt2410029255804725940.tmp17248583986822649448.tmp, second=0, third=Scan[regexFrom=RegexFrom[longValue=67583], regexTo=RegexTo[longValue=0]]]
        Triple[first=/tmp/jreplace123762773109394197/lipsum_02.txt983033321850305095.tmp3722144545151971344.tmp, second=1, third=Scan[regexFrom=RegexFrom[longValue=67577], regexTo=RegexTo[longValue=0]]]
        Triple[first=/tmp/jreplace123762773109394197/lipsum_02.txt2627508246751960573.tmp9597383239784481824.tmp, second=2, third=Scan[regexFrom=RegexFrom[longValue=54013], regexTo=RegexTo[longValue=0]]]

        Triple[first=/tmp/jreplace123762773109394197/lipsum_03.txt1913594938281852178.tmp2009149402770130264.tmp, second=0, third=Scan[regexFrom=RegexFrom[longValue=67583], regexTo=RegexTo[longValue=0]]]
        Triple[first=/tmp/jreplace123762773109394197/lipsum_03.txt5164038376501440938.tmp2630584251519327326.tmp, second=1, third=Scan[regexFrom=RegexFrom[longValue=67577], regexTo=RegexTo[longValue=0]]]
        Triple[first=/tmp/jreplace123762773109394197/lipsum_03.txt12633395774537785599.tmp3509461150194281249.tmp, second=2, third=Scan[regexFrom=RegexFrom[longValue=54013], regexTo=RegexTo[longValue=0]]]

        Triple[first=/tmp/jreplace123762773109394197/lipsum_04.txt17872066336613896700.tmp15447120243999861512.tmp, second=0, third=Scan[regexFrom=RegexFrom[longValue=67583], regexTo=RegexTo[longValue=0]]]
        Triple[first=/tmp/jreplace123762773109394197/lipsum_04.txt11317902508273056404.tmp172831509678498808.tmp, second=1, third=Scan[regexFrom=RegexFrom[longValue=67577], regexTo=RegexTo[longValue=0]]]
        Triple[first=/tmp/jreplace123762773109394197/lipsum_04.txt11039606620679030133.tmp1370715103769385103.tmp, second=2, third=Scan[regexFrom=RegexFrom[longValue=54013], regexTo=RegexTo[longValue=0]]]
       */
    }

    @Test
    void given_5_files_and_pattern_a_to_a_should_return_no_changes_or_malformed_files(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      // LinkedList<Pair<Path, Path>>
      var listOfPairPaths = pullout29KB(tempDir);

      assertEquals(5, listOfPairPaths.size());

      var listOfPairs = new LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>();

      var streamStrings = new LinkedList<Pair<Stream<String>, String>>();

      do {

        var pairPath = listOfPairPaths.removeFirst();
        var position = new LinkedList<Long>();
        position.add(0L);

        var pairPathLong = new Pair<>(pairPath.first(), position);

        var ll = new LinkedList<Pair<Path, LinkedList<Long>>>();
        ll.addFirst(pairPathLong);

        var pairPathLongPath = new Pair<>(ll, pairPath.second());

        listOfPairs.add(pairPathLongPath);

        var pairStreamString = new Pair<>(Files.lines(pairPath.first()), Files.readString(pairPath.first()));

        streamStrings.add(pairStreamString);

        pairPath = null;
        position = null;
        pairPathLong = null;
        ll = null;
        pairPathLongPath = null;
        pairStreamString = null;

      } while (!listOfPairPaths.isEmpty());

      assertTrue(Files.readString(tempDir.resolve("lipsum.txt")).contains("a"));

      // ---------------- AmanuensisProcess -------------------- \\
      // process the file and check for any malformations
      var patterns = new Patterns(new From("a"), new To("a"));

      var triple = new Triple<>(TS, listOfPairs, patterns);

      var amanuensisProcessObj = new AmanuensisProcess(triple);

      // Pair<Route, Result<Triple<LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>>
      var pair = amanuensisProcessObj.amanuensisProcess();

      assertTrue(pair.second().isSuccess());
      assertFalse(pair.second().successValue().first().isEmpty());
      assertTrue(pair.second().successValue().second().isEmpty());
      assertTrue(pair.second().successValue().third().isEmpty());

      // LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>
      var llist = pair.second().successValue().first();

      assertEquals(5, llist.size());
      assertEquals(1, Objects.requireNonNull(llist.peekFirst()).first().size());

      do {

        var innerpair = llist.removeFirst();
        var innerlist = innerpair.first();
        var innertriple = innerlist.removeFirst();

        assertEquals(Objects.requireNonNull(streamStrings.peekFirst()).second(), Files.readString(innertriple.first()));
        assertLinesMatch(streamStrings.removeFirst().first(), Files.lines(innertriple.first()));

        innerpair = null;
        innerlist = null;
        innertriple = null;

      } while (!llist.isEmpty());
    }

    // character count: regex from == regex to
    @Test
    void given_5_files_and_pattern_a_to_a_split_on_1MB_should_return_no_changes_or_malformed_files(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      // LinkedList<Pair<Path, Path>>
      var listOfPairPaths = pullout2800KB(tempDir);

      assertEquals(5, listOfPairPaths.size());

      var strings = new LinkedList<Pair<Stream<String>, List<String>>>();

      for (Pair<Path, Path> pairs : listOfPairPaths) {
        try { strings.add(new Pair<>(Files.lines(pairs.first()), Files.readAllLines(pairs.first()))); } catch (IOException e) { throw new RuntimeException(e); }
      }

      assertTrue(Files.readString(tempDir.resolve("lipsum.txt")).contains("a"));

      // ------------ Aristides ------------------------- \\
      // ------------ STAGE 1 --------------------------- \\
      var triple = new Triple<>(TS, listOfPairPaths, 1L);
      var aristidesObj = new Aristides(triple);
      var process = aristidesObj.aristides();
      var result = process.second();

      assertTrue(result.isSuccess());
      assertFalse(result.successValue().first().isEmpty());
      assertTrue(result.successValue().second().isEmpty());
      assertTrue(result.successValue().third().isEmpty());

      var listOfPairs = result.successValue().first();

      // ---------------- AmanuensisProcess ------------ \\
      // ---------------- STAGE 2 ----------------------- \\
      // process the file and check for any malformations
      var patterns = new Patterns(new From("a"), new To("a"));
      var amanuensisProcessObj = new AmanuensisProcess(new Triple<>(TS, listOfPairs, patterns));
      var pair = amanuensisProcessObj.amanuensisProcess();

      assertTrue(pair.second().isSuccess());
      assertFalse(pair.second().successValue().first().isEmpty());
      assertTrue(pair.second().successValue().second().isEmpty());
      assertTrue(pair.second().successValue().third().isEmpty());

      var llist = pair.second().successValue().first();

      assertFalse(llist.isEmpty());
      assertEquals(5, llist.size());
      assertEquals(3, Objects.requireNonNull(llist.peekFirst()).first().size());

      // ----------------- Melendez -------------------- \\
      // ----------------- STAGE 3 --------------------- \\
      // --------- write to endpoint ------------------- \\
      var melendezObj = new Melendez(new Pair<>(TS, llist));
      var melendezProcessed = melendezObj.melendez();
      var mpllist = melendezProcessed.second().successValue().first();
      var failures = melendezProcessed.second().successValue().second();

      assertFalse(mpllist.isEmpty());
      assertTrue(failures.isEmpty());
      assertEquals(5, mpllist.size());

      do {

        var innertriple = mpllist.removeFirst();
        var source = innertriple.third();

        var stringPair = strings.removeFirst();
        var pairPaths = listOfPairPaths.removeFirst();

        assertEquals(pairPaths.first().getFileName().toString(), source.getFileName().toString());

        assertLinesMatch(stringPair.first(), Files.lines(innertriple.first()));

        deleteTempFile(innertriple.first());

        innertriple = null;
        source = null;
        pairPaths = null;
        stringPair = null;

      } while (!mpllist.isEmpty());

      /* result.bind()
         Triple[first=[
                        Pair[first=[
                                     Pair[first=/tmp/jreplace15136626061785690216/lipsum.txt10250398574780248292.tmp, second=0],
                                     Pair[first=/tmp/jreplace15136626061785690216/lipsum.txt16563465441130464299.tmp, second=1048576],
                                     Pair[first=/tmp/jreplace15136626061785690216/lipsum.txt9982555490964439229.tmp, second=2097152]
                                   ],
                             second=/tmp/junit12846080606552793251/lipsum.txt],
                        Pair[first=[
                                     Pair[first=/tmp/jreplace15136626061785690216/lipsum_01.txt11952626427712081167.tmp, second=0],
                                     Pair[first=/tmp/jreplace15136626061785690216/lipsum_01.txt17343231718226189877.tmp, second=1048576],
                                     Pair[first=/tmp/jreplace15136626061785690216/lipsum_01.txt2561211113277364894.tmp, second=2097152]
                                   ],
                             second=/tmp/junit12846080606552793251/level_01_5765478396524244896/lipsum_01.txt],
                        Pair[first=[
                                     Pair[first=/tmp/jreplace15136626061785690216/lipsum_02.txt6376837281501448414.tmp, second=0],
                                     Pair[first=/tmp/jreplace15136626061785690216/lipsum_02.txt18155111062695799891.tmp, second=1048576],
                                     Pair[first=/tmp/jreplace15136626061785690216/lipsum_02.txt15741048775395839070.tmp, second=2097152]
                                   ],
                             second=/tmp/junit12846080606552793251/level_01_5765478396524244896/level_02_7852129066820767536/lipsum_02.txt],
                        Pair[first=[
                                     Pair[first=/tmp/jreplace15136626061785690216/lipsum_03.txt16762056969114561535.tmp, second=0],
                                     Pair[first=/tmp/jreplace15136626061785690216/lipsum_03.txt10162570515204107521.tmp, second=1048576],
                                     Pair[first=/tmp/jreplace15136626061785690216/lipsum_03.txt1665172573392251643.tmp, second=2097152]
                                   ],
                             second=/tmp/junit12846080606552793251/level_01_5765478396524244896/level_02_7852129066820767536/level_03_18111795681972204346/lipsum_03.txt],
                        Pair[first=[
                                     Pair[first=/tmp/jreplace15136626061785690216/lipsum_04.txt11811057212557646234.tmp, second=0],
                                     Pair[first=/tmp/jreplace15136626061785690216/lipsum_04.txt14860531713481245193.tmp, second=1048576],
                                     Pair[first=/tmp/jreplace15136626061785690216/lipsum_04.txt5453474654273938298.tmp, second=2097152]
                                   ],
                             second=/tmp/junit12846080606552793251/level_01_5765478396524244896/level_02_7852129066820767536/level_03_18111795681972204346/level_04_11980346687805166542/lipsum_04.txt]
                      ],
                second=[],
                third=[]
               ]

         melendezProcessed.second().bind()
         Pair[first=[
                      Triple[first=/tmp/jreplace15136626061785690216/lipsum.txt10250398574780248292.tmp5624151340366167592.tmp17475321413844756632.tmp,
                             second=Scan[regexFrom=RegexFrom[longValue=189173], regexTo=RegexTo[longValue=189173]],
                             third=/tmp/junit12846080606552793251/lipsum.txt],
                      Triple[first=/tmp/jreplace15136626061785690216/lipsum_01.txt11952626427712081167.tmp6487695223890812105.tmp14450012772055841964.tmp,
                             second=Scan[regexFrom=RegexFrom[longValue=189173], regexTo=RegexTo[longValue=189173]],
                             third=/tmp/junit12846080606552793251/level_01_5765478396524244896/lipsum_01.txt],
                      Triple[first=/tmp/jreplace15136626061785690216/lipsum_02.txt6376837281501448414.tmp17862836574776791900.tmp1244310566348121527.tmp,
                             second=Scan[regexFrom=RegexFrom[longValue=189173], regexTo=RegexTo[longValue=189173]],
                             third=/tmp/junit12846080606552793251/level_01_5765478396524244896/level_02_7852129066820767536/lipsum_02.txt],
                      Triple[first=/tmp/jreplace15136626061785690216/lipsum_03.txt16762056969114561535.tmp11993418727442847029.tmp11495512972283383944.tmp,
                             second=Scan[regexFrom=RegexFrom[longValue=189173], regexTo=RegexTo[longValue=189173]],
                             third=/tmp/junit12846080606552793251/level_01_5765478396524244896/level_02_7852129066820767536/level_03_18111795681972204346/lipsum_03.txt],
                      Triple[first=/tmp/jreplace15136626061785690216/lipsum_04.txt11811057212557646234.tmp7037647224102780969.tmp17347183669303020926.tmp,
                             second=Scan[regexFrom=RegexFrom[longValue=189173], regexTo=RegexTo[longValue=189173]],
                             third=/tmp/junit12846080606552793251/level_01_5765478396524244896/level_02_7852129066820767536/level_03_18111795681972204346/level_04_11980346687805166542/lipsum_04.txt]
                    ],
              second=[]
             ]
       */
    }

    // character count: regex from < regex to
    @Test
    void given_5_files_and_pattern_duo_to_binary_split_on_1MB_and_back_again_should_return_5_files_with_no_changes_or_malformations(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      // LinkedList<Pair<Path, Path>>
      var listOfPairPaths = pullout2800KB(tempDir);

      assertEquals(5, listOfPairPaths.size());

      var strings = new LinkedList<Tuple<Path, Long, Stream<String>, List<String>>>();

      for (Pair<Path, Path> pairs : listOfPairPaths) {
        try { strings.add(new Tuple<>(pairs.first(), Files.size(pairs.first()), Files.lines(pairs.first()), Files.readAllLines(pairs.first()))); } catch (IOException e) { throw new RuntimeException(e); }
      }

      assertTrue(Files.readString(tempDir.resolve("lipsum.txt")).contains("duo"));
      assertFalse(Files.readString(tempDir.resolve("lipsum.txt")).contains("binary"));

      // ------------ Aristides ------------------------- \\
      // ------------ STAGE 1 --------------------------- \\
      // ------------ split files on megabytes ---------- \\
      var triple = new Triple<>(TS, listOfPairPaths, 1L);
      var aristidesObj = new Aristides(triple);
      var process = aristidesObj.aristides();
      var result = process.second();

      assertTrue(result.isSuccess());
      assertFalse(result.successValue().first().isEmpty());
      assertTrue(result.successValue().second().isEmpty());
      assertTrue(result.successValue().third().isEmpty());

      var listOfPairs = result.successValue().first();

      // ---------------- AmanuensisProcess ------------ \\
      // ---------------- STAGE 2 ----------------------- \\
      // process the file and check for any malformations
      var patterns = new Patterns(new From("duo"), new To("binary"));
      var amanuensisProcessObj = new AmanuensisProcess(new Triple<>(TS, listOfPairs, patterns));
      var pair = amanuensisProcessObj.amanuensisProcess();

      assertTrue(pair.second().isSuccess());
      assertFalse(pair.second().successValue().first().isEmpty());
      assertTrue(pair.second().successValue().second().isEmpty());
      assertTrue(pair.second().successValue().third().isEmpty());

      var llist = pair.second().successValue().first();

      assertFalse(llist.isEmpty());
      assertEquals(5, llist.size());
      assertEquals(3, Objects.requireNonNull(llist.peekFirst()).first().size());

      // ----------------- Melendez -------------------- \\
      // ----------------- STAGE 3 --------------------- \\
      // --------- write to endpoint ------------------- \\
      var melendezObj = new Melendez(new Pair<>(TS, llist));
      var melendezProcessed = melendezObj.melendez();
      var mpllist = melendezProcessed.second().successValue().first();
      var failures = melendezProcessed.second().successValue().second();

      assertFalse(mpllist.isEmpty());
      assertTrue(failures.isEmpty());

      var size = mpllist.size();
      assertEquals(5, size);

      // first i have to ensure the original and processed file sizes now don't match but their file names do
      for (int index = 0; index < size; index++) {

        var innertriple = mpllist.get(index);

        var source = innertriple.third();

        var stringTuple = strings.get(index);

        assertEquals(stringTuple.first().getFileName().toString(), source.getFileName().toString());
        assertNotEquals(stringTuple.second(), Files.size(innertriple.first()));
        assertTrue(Files.readString(innertriple.first()).contains("binary"));
        assertFalse(Files.readString(innertriple.first()).contains("duo"));

        innertriple = null;
        source = null;
        stringTuple = null;
      }

      // ------------ Aristides ------------------------- \\
      // ------------ AND BACK AGAIN STAGE 1 ------------ \\
      // ------------ split files on megabytes ---------- \\
      listOfPairPaths.clear();
      for (Triple<Path, Scan, Path> pathScanPathTriple : mpllist) { listOfPairPaths.add(new Pair<>(pathScanPathTriple.first(), pathScanPathTriple.third())); }

      triple = new Triple<>(TS, listOfPairPaths, 1L);
      aristidesObj = new Aristides(triple);
      process = aristidesObj.aristides();
      result = process.second();

      assertTrue(result.isSuccess());
      assertFalse(result.successValue().first().isEmpty());
      assertTrue(result.successValue().second().isEmpty());
      assertTrue(result.successValue().third().isEmpty());

      listOfPairs = result.successValue().first();

      // ---------------- AmanuensisProcess ------------ \\
      // ---------------- AND BACK AGAIN STAGE 2 ------- \\
      // process the file and check for any malformations
      patterns = new Patterns(new From("binary"), new To("duo"));
      amanuensisProcessObj = new AmanuensisProcess(new Triple<>(TS, listOfPairs, patterns));
      pair = amanuensisProcessObj.amanuensisProcess();

      assertTrue(pair.second().isSuccess());
      assertFalse(pair.second().successValue().first().isEmpty());
      assertTrue(pair.second().successValue().second().isEmpty());
      assertTrue(pair.second().successValue().third().isEmpty());

      llist = pair.second().successValue().first();

      assertFalse(llist.isEmpty());
      assertEquals(5, llist.size());
      assertEquals(3, Objects.requireNonNull(llist.peekFirst()).first().size());

      // ----------------- Melendez -------------------- \\
      // ----------------- AND BACK AGAIN STAGE 3 ------ \\
      // ----------------- write to endpoint ----------- \\
      melendezObj = new Melendez(new Pair<>(TS, llist));
      melendezProcessed = melendezObj.melendez();
      mpllist = melendezProcessed.second().successValue().first();
      failures = melendezProcessed.second().successValue().second();

      assertFalse(mpllist.isEmpty());
      assertTrue(failures.isEmpty());

      size = mpllist.size();
      assertEquals(5, size);

      // first we have to ensure the original and processed file sizes now do match but their file names don't
      for (int index = 0; index < size; index++) {

        var innertriple = mpllist.get(index);

        var source = innertriple.third();

        var stringTuple = strings.get(index);

        assertEquals(stringTuple.first().getFileName().toString(), source.getFileName().toString());
        assertEquals(stringTuple.second(), Files.size(innertriple.first()));
        assertLinesMatch(stringTuple.third(), Files.lines(innertriple.first()));
        assertTrue(Files.readString(innertriple.first()).contains("duo"));
        assertFalse(Files.readString(innertriple.first()).contains("binary"));

        innertriple = null;
        source = null;
        stringTuple = null;
      }
    }

    // character count: regex from > regex to
    @Test
    void given_5_files_and_pattern_animal_to_yob_split_on_1MB_and_back_again_should_return_no_changes_or_malformations(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      // LinkedList<Pair<Path, Path>>
      var listOfPairPaths = pullout2800KB(tempDir);

      assertEquals(5, listOfPairPaths.size());

      var strings = new LinkedList<Tuple<Path, Long, Stream<String>, List<String>>>();

      for (Pair<Path, Path> pairs : listOfPairPaths) {
        try { strings.add(new Tuple<>(pairs.first(), Files.size(pairs.first()), Files.lines(pairs.first()), Files.readAllLines(pairs.first()))); } catch (IOException e) { throw new RuntimeException(e); }
      }

      assertTrue(Files.readString(tempDir.resolve("lipsum.txt")).contains("animal"));
      assertFalse(Files.readString(tempDir.resolve("lipsum.txt")).contains("yob"));

      // ------------ Aristides ------------------------- \\
      // ------------ STAGE 1 --------------------------- \\
      // ------------ split files on megabytes ---------- \\
      var triple = new Triple<>(TS, listOfPairPaths, 1L);
      var aristidesObj = new Aristides(triple);
      var process = aristidesObj.aristides();
      var result = process.second();

      assertTrue(result.isSuccess());
      assertFalse(result.successValue().first().isEmpty());
      assertTrue(result.successValue().second().isEmpty());
      assertTrue(result.successValue().third().isEmpty());

      var listOfPairs = result.successValue().first();

      // ---------------- AmanuensisProcess ------------ \\
      // ---------------- STAGE 2 ----------------------- \\
      // process the file and check for any malformations
      var patterns = new Patterns(new From("animal"), new To("yob"));
      var amanuensisProcessObj = new AmanuensisProcess(new Triple<>(TS, listOfPairs, patterns));
      var pair = amanuensisProcessObj.amanuensisProcess();

      assertTrue(pair.second().isSuccess());
      assertFalse(pair.second().successValue().first().isEmpty());
      assertTrue(pair.second().successValue().second().isEmpty());
      assertTrue(pair.second().successValue().third().isEmpty());

      var llist = pair.second().successValue().first();

      assertFalse(llist.isEmpty());
      assertEquals(5, llist.size());
      assertEquals(3, Objects.requireNonNull(llist.peekFirst()).first().size());

      // ----------------- Melendez -------------------- \\
      // ----------------- STAGE 3 --------------------- \\
      // --------- write to endpoint ------------------- \\
      var melendezObj = new Melendez(new Pair<>(TS, llist));
      var melendezProcessed = melendezObj.melendez();
      var mpllist = melendezProcessed.second().successValue().first();
      var failures = melendezProcessed.second().successValue().second();

      assertFalse(mpllist.isEmpty());
      assertTrue(failures.isEmpty());

      var size = mpllist.size();
      assertEquals(5, size);

      // first i have to ensure the original and processed file sizes now don't match but their file names do
      for (int index = 0; index < size; index++) {

        var innertriple = mpllist.get(index);

        var source = innertriple.third();

        var stringTuple = strings.get(index);

        assertEquals(stringTuple.first().getFileName().toString(), source.getFileName().toString());
        assertNotEquals(stringTuple.second(), Files.size(innertriple.first()));
        assertTrue(Files.readString(innertriple.first()).contains("yob"));
        assertFalse(Files.readString(innertriple.first()).contains("animal"));

        innertriple = null;
        source = null;
        stringTuple = null;
      }

      // ------------ Aristides ------------------------- \\
      // ------------ AND BACK AGAIN STAGE 1 ------------ \\
      // ------------ split files on megabytes ---------- \\
      listOfPairPaths.clear();
      for (Triple<Path, Scan, Path> pathScanPathTriple : mpllist) { listOfPairPaths.add(new Pair<>(pathScanPathTriple.first(), pathScanPathTriple.third())); }

      triple = new Triple<>(TS, listOfPairPaths, 1L);
      aristidesObj = new Aristides(triple);
      process = aristidesObj.aristides();
      result = process.second();

      assertTrue(result.isSuccess());
      assertFalse(result.successValue().first().isEmpty());
      assertTrue(result.successValue().second().isEmpty());
      assertTrue(result.successValue().third().isEmpty());

      listOfPairs = result.successValue().first();

      // ---------------- AmanuensisProcess ------------ \\
      // ---------------- AND BACK AGAIN STAGE 2 ------- \\
      // process the file and check for any malformations
      patterns = new Patterns(new From("yob"), new To("animal"));
      amanuensisProcessObj = new AmanuensisProcess(new Triple<>(TS, listOfPairs, patterns));
      pair = amanuensisProcessObj.amanuensisProcess();

      assertTrue(pair.second().isSuccess());
      assertFalse(pair.second().successValue().first().isEmpty());
      assertTrue(pair.second().successValue().second().isEmpty());
      assertTrue(pair.second().successValue().third().isEmpty());

      llist = pair.second().successValue().first();

      assertFalse(llist.isEmpty());
      assertEquals(5, llist.size());
      assertEquals(3, Objects.requireNonNull(llist.peekFirst()).first().size());

      // ----------------- Melendez -------------------- \\
      // ----------------- AND BACK AGAIN STAGE 3 ------ \\
      // --------- write to endpoint ------------------- \\
      melendezObj = new Melendez(new Pair<>(TS, llist));
      melendezProcessed = melendezObj.melendez();
      mpllist = melendezProcessed.second().successValue().first();
      failures = melendezProcessed.second().successValue().second();

      assertFalse(mpllist.isEmpty());
      assertTrue(failures.isEmpty());

      size = mpllist.size();
      assertEquals(5, size);

      // first i have to ensure the original and processed file sizes now do match but their file names don't
      for (int index = 0; index < size; index++) {

        var innertriple = mpllist.get(index);

        var source = innertriple.third();

        var stringTuple = strings.get(index);

        assertEquals(stringTuple.first().getFileName().toString(), source.getFileName().toString());
        assertEquals(stringTuple.second(), Files.size(innertriple.first()));
        assertLinesMatch(stringTuple.third(), Files.lines(innertriple.first()));
        assertTrue(Files.readString(innertriple.first()).contains("animal"));
        assertFalse(Files.readString(innertriple.first()).contains("yob"));

        innertriple = null;
        source = null;
        stringTuple = null;
      }
    }

    @Test
    void given_5_files_and_pattern_a_to_j_should_return_regexFrom_equals_1873_and_regexTo_equals_0(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      // LinkedList<Pair<Path, Path>>
      var listOfPairPaths = pullout29KB(tempDir);

      assertEquals(5, listOfPairPaths.size());

      var listOfPairs = new LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>();

      do {

        var pairPath = listOfPairPaths.removeFirst();

        var position = new LinkedList<Long>();
        position.add(0L);

        var pairPathLong = new Pair<>(pairPath.first(), position);

        var ll = new LinkedList<Pair<Path, LinkedList<Long>>>();

        ll.addFirst(pairPathLong);

        var pairPathLongPath = new Pair<>(ll, pairPath.second());

        listOfPairs.add(pairPathLongPath);

        pairPath = null;
        position = null;
        pairPathLong = null;
        ll = null;
        pairPathLongPath = null;

      } while (!listOfPairPaths.isEmpty());

      assertTrue(Files.readString(tempDir.resolve("lipsum.txt")).contains("a"));
      assertFalse(Files.readString(tempDir.resolve("lipsum.txt")).contains("j"));

      // ---------------- AmanuensisProcess ------------ \\
      var patterns = new Patterns(new From("a"), new To("j"));
      var triple = new Triple<>(TS, listOfPairs, patterns);
      var amanuensisProcessObj = new AmanuensisProcess(triple);

      // Pair<Integer, Result<Triple<LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>>
      var pair = amanuensisProcessObj.amanuensisProcess();

      assertTrue(pair.second().isSuccess());
      assertFalse(pair.second().successValue().first().isEmpty());
      assertTrue(pair.second().successValue().second().isEmpty());
      assertTrue(pair.second().successValue().third().isEmpty());

      // LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>
      var llist = pair.second().successValue().first();

      assertEquals(5, llist.size());

      do {

        // Pair<LinkedList<Triple<Path, Long, Scan>>, Path>
        var innerpair = llist.removeFirst();
        var source = innerpair.second();

        // LinkedList<Triple<Path, Long, Scan>>
        var innerlist = innerpair.first();

        assertEquals(1, innerlist.size());

        var innertriple = innerlist.removeFirst();

        // processed temp file name contains the source file name
        assertTrue(innertriple.first().getFileName().toString().contains(source.getFileName().toString()));

        String processed = Files.readString(innertriple.first());

        assertTrue(processed.contains("j"));
        assertFalse(processed.contains("a"));

        assertEquals(1873, innertriple.third().regexFrom().longValue().sumThenReset());
        assertEquals(0, innertriple.third().regexTo().longValue().sumThenReset());

        innerpair = null;
        source = null;
        innerlist = null;
        innertriple = null;
        processed = null;

      } while (!llist.isEmpty());

      /*  pair.second().bind()
          Triple[first=[
                         Pair[first=[
                                      Triple[first=/tmp/jreplace15637602213669490732/lipsum.txt16413798643665904119.tmp, second=0, third=Scan[regexFrom=RegexFrom[longValue=1873], regexTo=RegexTo[longValue=0]]]],
                              second=/tmp/junit9272052706774476932/lipsum.txt],
                         Pair[first=[
                                      Triple[first=/tmp/jreplace15637602213669490732/lipsum_01.txt5923908452725144225.tmp, second=0, third=Scan[regexFrom=RegexFrom[longValue=1873], regexTo=RegexTo[longValue=0]]]],
                              second=/tmp/junit9272052706774476932/level_01_1703503384078508212/lipsum_01.txt],
                         Pair[first=[
                                      Triple[first=/tmp/jreplace15637602213669490732/lipsum_02.txt12924637717307607021.tmp, second=0, third=Scan[regexFrom=RegexFrom[longValue=1873], regexTo=RegexTo[longValue=0]]]],
                              second=/tmp/junit9272052706774476932/level_01_1703503384078508212/level_02_2869872789471537538/lipsum_02.txt],
                         Pair[first=[
                                      Triple[first=/tmp/jreplace15637602213669490732/lipsum_03.txt5084112417883208542.tmp, second=0, third=Scan[regexFrom=RegexFrom[longValue=1873], regexTo=RegexTo[longValue=0]]]],
                              second=/tmp/junit9272052706774476932/level_01_1703503384078508212/level_02_2869872789471537538/level_03_8007590503168788274/lipsum_03.txt],
                         Pair[first=[
                                      Triple[first=/tmp/jreplace15637602213669490732/lipsum_04.txt16699609091013251832.tmp, second=0, third=Scan[regexFrom=RegexFrom[longValue=1873], regexTo=RegexTo[longValue=0]]]],
                              second=/tmp/junit9272052706774476932/level_01_1703503384078508212/level_02_2869872789471537538/level_03_8007590503168788274/level_04_642127534466213169/lipsum_04.txt]
                       ],
                 second=[],
                 third=[]
                ]
       */
    }

    @Test
    void given_5_files_and_pattern_a_to_j_split_on_1MB_should_return_regexFrom_equals_1873_regexTo_equals_0(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      // LinkedList<Pair<Path, Path>>
      var listOfPairPaths = pullout2800KB(tempDir);

      assertEquals(5, listOfPairPaths.size());

      // ------------ Aristides ------------------------- \\
      var triple = new Triple<>(TS, listOfPairPaths, 1L);
      var aristidesObj = new Aristides(triple);

      // Pair<Integer, Result<Triple<LinkedList<Pair<LinkedList<Pair<Path, Long>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>>
      var process = aristidesObj.aristides();
      var result = process.second();

      assertTrue(result.isSuccess());
      assertFalse(result.successValue().first().isEmpty());
      assertTrue(result.successValue().second().isEmpty());
      assertTrue(result.successValue().third().isEmpty());

      // LinkedList<Pair<LinkedList<Pair<Path, Long>>, Path>>
      var listOfPairs = result.successValue().first();

      // ---------------- AmanuensisProcess ------------ \\
      var patterns = new Patterns(new From("a"), new To("j"));
      var amanuensisProcessObj = new AmanuensisProcess(new Triple<>(TS, listOfPairs, patterns));

      // Pair<Integer, Result<Triple<LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>, LinkedList<Path>, LinkedList<Object>>>>
      var pair = amanuensisProcessObj.amanuensisProcess();

      assertTrue(pair.second().isSuccess());
      assertFalse(pair.second().successValue().first().isEmpty());
      assertTrue(pair.second().successValue().second().isEmpty());
      assertTrue(pair.second().successValue().third().isEmpty());

      // LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>
      var llist = pair.second().successValue().first();

      assertEquals(5, llist.size());

      do {

        // Pair<LinkedList<Triple<Path, Long, Scan>>, Path>
        var innerpair = llist.removeFirst();

        // LinkedList<Triple<Path, Long, Scan>>
        var innerlist = innerpair.first();
        var path = innerpair.second();

        assertEquals(3, innerlist.size());

        // Triple<Path, Long, Scan>
        var innertriple1 = innerlist.removeFirst();
        var innertriple2 = innerlist.removeFirst();
        var innertriple3 = innerlist.removeFirst();

        // processed temp file name contains the source file name
        assertTrue(innertriple1.first().getFileName().toString().contains(path.getFileName().toString()));
        assertTrue(innertriple2.first().getFileName().toString().contains(path.getFileName().toString()));
        assertTrue(innertriple3.first().getFileName().toString().contains(path.getFileName().toString()));

        String processed1 = Files.readString(innertriple1.first());
        String processed2 = Files.readString(innertriple2.first());
        String processed3 = Files.readString(innertriple3.first());

        assertTrue(processed1.contains("j"));
        assertFalse(processed1.contains("a"));

        assertTrue(processed2.contains("j"));
        assertFalse(processed2.contains("a"));

        assertTrue(processed3.contains("j"));
        assertFalse(processed3.contains("a"));

        assertEquals(67583, innertriple1.third().regexFrom().longValue().sumThenReset());
        assertEquals(0, innertriple1.third().regexTo().longValue().sumThenReset());

        assertEquals(67577, innertriple2.third().regexFrom().longValue().sumThenReset());
        assertEquals(0, innertriple2.third().regexTo().longValue().sumThenReset());

        assertEquals(54013, innertriple3.third().regexFrom().longValue().sumThenReset());
        assertEquals(0, innertriple3.third().regexTo().longValue().sumThenReset());

        deleteTempFile(innertriple1.first());
        deleteTempFile(innertriple2.first());
        deleteTempFile(innertriple3.first());

        innerpair = null;
        path = null;
        innertriple1 = null;
        innertriple2 = null;
        innertriple3 = null;
        processed1 = null;
        processed2 = null;
        processed3 = null;

      } while (!llist.isEmpty());

      /* result.bind()
         Triple[first=[
                        Pair[first=[
                                     Pair[first=/tmp/jreplace16368035917018006711/lipsum.txt5449020915073367835.tmp, second=0],
                                     Pair[first=/tmp/jreplace16368035917018006711/lipsum.txt5280770213946858986.tmp, second=1048576],
                                     Pair[first=/tmp/jreplace16368035917018006711/lipsum.txt14229105984300807801.tmp, second=2097152]
                                   ],
                             second=/tmp/junit7079440507425792763/lipsum.txt],
                        Pair[first=[
                                     Pair[first=/tmp/jreplace16368035917018006711/lipsum_01.txt2173449105202474469.tmp, second=0],
                                     Pair[first=/tmp/jreplace16368035917018006711/lipsum_01.txt18429466936356050686.tmp, second=1048576],
                                     Pair[first=/tmp/jreplace16368035917018006711/lipsum_01.txt15029206723335591871.tmp, second=2097152]
                                    ],
                             second=/tmp/junit7079440507425792763/level_01_12625080258704370486/lipsum_01.txt],
                        Pair[first=[
                                     Pair[first=/tmp/jreplace16368035917018006711/lipsum_02.txt14650793762815873150.tmp, second=0],
                                     Pair[first=/tmp/jreplace16368035917018006711/lipsum_02.txt356132936096819537.tmp, second=1048576],
                                     Pair[first=/tmp/jreplace16368035917018006711/lipsum_02.txt14791690448498274704.tmp, second=2097152]
                                   ],
                             second=/tmp/junit7079440507425792763/level_01_12625080258704370486/level_02_15621545988662879569/lipsum_02.txt],
                        Pair[first=[
                                     Pair[first=/tmp/jreplace16368035917018006711/lipsum_03.txt11965642767188029745.tmp, second=0],
                                     Pair[first=/tmp/jreplace16368035917018006711/lipsum_03.txt4030807545117841847.tmp, second=1048576],
                                     Pair[first=/tmp/jreplace16368035917018006711/lipsum_03.txt7173230680780441379.tmp, second=2097152]
                                   ],
                             second=/tmp/junit7079440507425792763/level_01_12625080258704370486/level_02_15621545988662879569/level_03_15123384917020543571/lipsum_03.txt],
                        Pair[first=[
                                     Pair[first=/tmp/jreplace16368035917018006711/lipsum_04.txt14227796075558543628.tmp, second=0],
                                     Pair[first=/tmp/jreplace16368035917018006711/lipsum_04.txt2786960505420488399.tmp, second=1048576],
                                     Pair[first=/tmp/jreplace16368035917018006711/lipsum_04.txt13737536841564845293.tmp, second=2097152]
                                   ],
                             second=/tmp/junit7079440507425792763/level_01_12625080258704370486/level_02_15621545988662879569/level_03_15123384917020543571/level_04_1985837179216042029/lipsum_04.txt]
                      ],
                second=[],
                third=[]
               ]

      // Question: why is it creating two tmp files xxx.tmpxxxtmp?
      // Aristides: firstly creates xxx.tmp to write from byte buffer into empty tmp file
      // AmanuensisProcess: secondly creates xxxtmp to write the processed file into empty tmp destination file
      // the combination of the two processes creates a single file with extension xxx.tmpxxxtmp

         pair.second.bind()
         Triple[first=[
                        Pair[first=[
                                     Triple[first=/tmp/jreplace16368035917018006711/lipsum.txt5449020915073367835.tmp1460715942223262432.tmp, second=0, third=Scan[regexFrom=RegexFrom[longValue=67583], regexTo=RegexTo[longValue=0]]],
                                     Triple[first=/tmp/jreplace16368035917018006711/lipsum.txt5280770213946858986.tmp11109355529022814837.tmp, second=1048576, third=Scan[regexFrom=RegexFrom[longValue=67577], regexTo=RegexTo[longValue=0]]],
                                     Triple[first=/tmp/jreplace16368035917018006711/lipsum.txt14229105984300807801.tmp8648848511140539643.tmp, second=2097152, third=Scan[regexFrom=RegexFrom[longValue=54013], regexTo=RegexTo[longValue=0]]]
                                   ],
                             second=/tmp/junit7079440507425792763/lipsum.txt],
                        Pair[first=[
                                     Triple[first=/tmp/jreplace16368035917018006711/lipsum_01.txt2173449105202474469.tmp6440213489841923787.tmp, second=0, third=Scan[regexFrom=RegexFrom[longValue=67583], regexTo=RegexTo[longValue=0]]],
                                     Triple[first=/tmp/jreplace16368035917018006711/lipsum_01.txt18429466936356050686.tmp2347212604429361832.tmp, second=1048576, third=Scan[regexFrom=RegexFrom[longValue=67577], regexTo=RegexTo[longValue=0]]],
                                     Triple[first=/tmp/jreplace16368035917018006711/lipsum_01.txt15029206723335591871.tmp350255546205239318.tmp, second=2097152, third=Scan[regexFrom=RegexFrom[longValue=54013], regexTo=RegexTo[longValue=0]]]
                                   ],
                             second=/tmp/junit7079440507425792763/level_01_12625080258704370486/lipsum_01.txt],
                        Pair[first=[
                                     Triple[first=/tmp/jreplace16368035917018006711/lipsum_02.txt14650793762815873150.tmp4194859556499440568.tmp, second=0, third=Scan[regexFrom=RegexFrom[longValue=67583], regexTo=RegexTo[longValue=0]]],
                                     Triple[first=/tmp/jreplace16368035917018006711/lipsum_02.txt356132936096819537.tmp5355496794883557220.tmp, second=1048576, third=Scan[regexFrom=RegexFrom[longValue=67577], regexTo=RegexTo[longValue=0]]],
                                     Triple[first=/tmp/jreplace16368035917018006711/lipsum_02.txt14791690448498274704.tmp17146878626724071486.tmp, second=2097152, third=Scan[regexFrom=RegexFrom[longValue=54013], regexTo=RegexTo[longValue=0]]]
                                   ],
                             second=/tmp/junit7079440507425792763/level_01_12625080258704370486/level_02_15621545988662879569/lipsum_02.txt],
                        Pair[first=[
                                     Triple[first=/tmp/jreplace16368035917018006711/lipsum_03.txt11965642767188029745.tmp5800191594819196220.tmp, second=0, third=Scan[regexFrom=RegexFrom[longValue=67583], regexTo=RegexTo[longValue=0]]],
                                     Triple[first=/tmp/jreplace16368035917018006711/lipsum_03.txt4030807545117841847.tmp13265993462741178672.tmp, second=1048576, third=Scan[regexFrom=RegexFrom[longValue=67577], regexTo=RegexTo[longValue=0]]],
                                     Triple[first=/tmp/jreplace16368035917018006711/lipsum_03.txt7173230680780441379.tmp2885452283538876818.tmp, second=2097152, third=Scan[regexFrom=RegexFrom[longValue=54013], regexTo=RegexTo[longValue=0]]]
                                   ],
                             second=/tmp/junit7079440507425792763/level_01_12625080258704370486/level_02_15621545988662879569/level_03_15123384917020543571/lipsum_03.txt],
                        Pair[first=[
                                     Triple[first=/tmp/jreplace16368035917018006711/lipsum_04.txt14227796075558543628.tmp77082713026571108.tmp, second=0, third=Scan[regexFrom=RegexFrom[longValue=67583], regexTo=RegexTo[longValue=0]]],
                                     Triple[first=/tmp/jreplace16368035917018006711/lipsum_04.txt2786960505420488399.tmp6535391247049642362.tmp, second=1048576, third=Scan[regexFrom=RegexFrom[longValue=67577], regexTo=RegexTo[longValue=0]]],
                                     Triple[first=/tmp/jreplace16368035917018006711/lipsum_04.txt13737536841564845293.tmp5259029342428232362.tmp, second=2097152, third=Scan[regexFrom=RegexFrom[longValue=54013], regexTo=RegexTo[longValue=0]]]
                                   ],
                             second=/tmp/junit7079440507425792763/level_01_12625080258704370486/level_02_15621545988662879569/level_03_15123384917020543571/level_04_1985837179216042029/lipsum_04.txt]
                      ],
                second=[],
                third=[]
               ]
       */
    }

    // What's the purpose of this test?
    // Given a directory tree contains file sizes that exceed 2047mb
    // and no split command line flag was given --megabytes=0
    // an automatic split will take place on Integer.MAX_VALUE
    // 2047mb due to the java ByteBuffer constraint.

    // see: au.kjd.jreplace.domain.Aristides.AristidesFractionalise

    // if this test is run in isolation you will need to:
    // $ rm -r /tmp/junit*
    // $ rm -r /tmp/jreplace*

    @Test
    @Disabled("-Xmx7168m - I don't have enough available system memory to run this test. Process finished with exit code 137 (interrupted by signal 9: SIGKILL)")
    void given_a_file_size_2866_megabytes_and_pattern_animal_to_yob_with_no_splits_and_back_again_should_return_no_changes_or_malformations(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      // LinkedList<Pair<Path, Path>>
      var listOfPairPaths = pullout2800KB(tempDir);

      assertEquals(5, listOfPairPaths.size());

      Path source = listOfPairPaths.removeFirst().first();
      Path target = listOfPairPaths.removeFirst().first();

      StandardOpenOption[] WA = { StandardOpenOption.WRITE, StandardOpenOption.APPEND };

      Files.write(target, Files.readAllLines(source), StandardCharsets.UTF_8, WA);
      Files.write(target, Files.readAllLines(target), StandardCharsets.UTF_8, WA);
      Files.write(target, Files.readAllLines(target), StandardCharsets.UTF_8, WA);
      Files.write(target, Files.readAllLines(target), StandardCharsets.UTF_8, WA);
      Files.write(target, Files.readAllLines(target), StandardCharsets.UTF_8, WA);
      Files.write(target, Files.readAllLines(target), StandardCharsets.UTF_8, WA);
      Files.write(target, Files.readAllLines(target), StandardCharsets.UTF_8, WA);
      Files.write(target, Files.readAllLines(target), StandardCharsets.UTF_8, WA);
      Files.write(target, Files.readAllLines(target), StandardCharsets.UTF_8, WA);
      Files.write(target, Files.readAllLines(target), StandardCharsets.UTF_8, WA);

      // created file size is approximately 2866MB
      System.out.println("created file size is approximately -> " + (Files.size(target) / 1024 / 1024) + "MB");

      var strings = new LinkedList<Tuple<Path, Long, Stream<String>, List<String>>>();

      try { strings.add(new Tuple<>(target, Files.size(target), Files.lines(target), Files.readAllLines(target))); } catch (IOException e) { throw new RuntimeException(e); }

      try (Stream<String> line = Files.lines(target)) {
        boolean animal = line.anyMatch(str -> str.contains("animal"));
        assertTrue(animal);
      }

      try (Stream<String> line = Files.lines(target)) {
        boolean yob = line.anyMatch(str -> str.contains("yob"));
        assertFalse(yob);
      }

      var pairPaths = new LinkedList<Pair<Path, Path>>();
      pairPaths.add(new Pair<>(target, target));

      // ------------ Aristides ------------------------- \\
      // ------------ STAGE 1 --------------------------- \\
      // ------- don't split file on megabytes ---------- \\
      var triple = new Triple<>(TS, pairPaths, 0L);
      var aristidesObj = new Aristides(triple);
      var process = aristidesObj.aristides();
      var result = process.second();

      assertTrue(result.isSuccess());
      assertFalse(result.successValue().first().isEmpty());
      assertTrue(result.successValue().second().isEmpty());
      assertTrue(result.successValue().third().isEmpty());

      var listOfPairs = result.successValue().first();

      // ---------------- AmanuensisProcess ------------ \\
      // ---------------- STAGE 2 ----------------------- \\
      // process the file and check for any malformations
      var patterns = new Patterns(new From("animal"), new To("yob"));
      var amanuensisProcessObj = new AmanuensisProcess(new Triple<>(TS, listOfPairs, patterns));
      var pair = amanuensisProcessObj.amanuensisProcess();

      assertTrue(pair.second().isSuccess());
      assertFalse(pair.second().successValue().first().isEmpty());
      assertTrue(pair.second().successValue().second().isEmpty());
      assertTrue(pair.second().successValue().third().isEmpty());

      var llist = pair.second().successValue().first();

      assertFalse(llist.isEmpty());
      assertEquals(1, llist.size());
      assertEquals(1, Objects.requireNonNull(llist.peekFirst()).first().size());

      // ----------------- Melendez -------------------- \\
      // ----------------- STAGE 3 --------------------- \\
      // --------- write to endpoint ------------------- \\
      var melendezObj = new Melendez(new Pair<>(TS, llist));
      var melendezProcessed = melendezObj.melendez();
      var mpllist = melendezProcessed.second().successValue().first();
      var failures = melendezProcessed.second().successValue().second();

      assertFalse(mpllist.isEmpty());
      assertTrue(failures.isEmpty());

      var size = mpllist.size();
      assertEquals(1, size);

      // first i have to ensure the original and processed file sizes now don't match but their file names do
      for (int index = 0; index < size; index++) {

        var innertriple = mpllist.get(index);

        var sourceFile = innertriple.third();

        var stringTuple = strings.get(index);

        assertEquals(stringTuple.first().getFileName().toString(), sourceFile.getFileName().toString());
        assertNotEquals(stringTuple.second(), Files.size(innertriple.first()));

        try (Stream<String> line = Files.lines(innertriple.first())) {
          boolean animal = line.anyMatch(str -> str.contains("animal"));
          assertFalse(animal);
        }

        try (Stream<String> line = Files.lines(innertriple.first())) {
          boolean yob = line.anyMatch(str -> str.contains("yob"));
          assertTrue(yob);
        }

        innertriple = null;
        sourceFile = null;
        stringTuple = null;
      }

      // ------------ Aristides ------------------------- \\
      // ------------ AND BACK AGAIN STAGE 1 ------------ \\
      // ------------ split files on megabytes ---------- \\
      listOfPairPaths.clear();
      for (Triple<Path, Scan, Path> pathScanPathTriple : mpllist) { listOfPairPaths.add(new Pair<>(pathScanPathTriple.first(), pathScanPathTriple.third())); }

      triple = new Triple<>(TS, listOfPairPaths, 0L);
      aristidesObj = new Aristides(triple);
      process = aristidesObj.aristides();
      result = process.second();

      assertTrue(result.isSuccess());
      assertFalse(result.successValue().first().isEmpty());
      assertTrue(result.successValue().second().isEmpty());
      assertTrue(result.successValue().third().isEmpty());

      listOfPairs = result.successValue().first();

      // ---------------- AmanuensisProcess ------------ \\
      // ---------------- AND BACK AGAIN STAGE 2 ------- \\
      // process the file and check for any malformations
      patterns = new Patterns(new From("yob"), new To("animal"));
      amanuensisProcessObj = new AmanuensisProcess(new Triple<>(TS, listOfPairs, patterns));
      pair = amanuensisProcessObj.amanuensisProcess();

      assertTrue(pair.second().isSuccess());
      assertFalse(pair.second().successValue().first().isEmpty());
      assertTrue(pair.second().successValue().second().isEmpty());
      assertTrue(pair.second().successValue().third().isEmpty());

      llist = pair.second().successValue().first();

      assertFalse(llist.isEmpty());
      assertEquals(1, llist.size());
      assertEquals(1, Objects.requireNonNull(llist.peekFirst()).first().size());

      // ----------------- Melendez -------------------- \\
      // ----------------- AND BACK AGAIN STAGE 3 ------ \\
      // --------- write to endpoint ------------------- \\
      melendezObj = new Melendez(new Pair<>(TS, llist));
      melendezProcessed = melendezObj.melendez();
      mpllist = melendezProcessed.second().successValue().first();
      failures = melendezProcessed.second().successValue().second();

      assertFalse(mpllist.isEmpty());
      assertTrue(failures.isEmpty());

      size = mpllist.size();
      assertEquals(1, size);

      // first i have to ensure the original and processed file sizes now do match but their file names don't
      for (int index = 0; index < size; index++) {

        var innertriple = mpllist.get(index);

        var sourceAgain = innertriple.third();

        var stringTuple = strings.get(index);

        assertEquals(stringTuple.first().getFileName().toString(), sourceAgain.getFileName().toString());
        assertEquals(stringTuple.second(), Files.size(innertriple.first()));
        assertLinesMatch(stringTuple.third(), Files.lines(innertriple.first()));

        try (Stream<String> line = Files.lines(innertriple.first())) {
          boolean animal = line.anyMatch(str -> str.contains("animal"));
          assertTrue(animal);
        }

        try (Stream<String> line = Files.lines(innertriple.first())) {
          boolean yob = line.anyMatch(str -> str.contains("yob"));
          assertFalse(yob);
        }

        innertriple = null;
        sourceAgain = null;
        stringTuple = null;
      }
    }
  }

  @Nested
  @Order(2)
  class AmanuensisProcess_Break_It {

    @Test
    void given_empty_list_should_return_is_failure() {

      var listOfPairs = new LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>();

      var patterns = new Patterns(new From("a"), new To("j"));
      var amanuensisProcessObj = new AmanuensisProcess(new Triple<>(TS, listOfPairs, patterns));

      var pair = amanuensisProcessObj.amanuensisProcess();
      var result = pair.second();

      assertTrue(result.isFailure());
      assertInstanceOf(IllegalStateException.class, result.failureValue());
    }

    @Test
    void given_absent_path_should_return_list_failure_size_1() {

      var listOfPairs = new LinkedList<Pair<LinkedList<Pair<Path, LinkedList<Long>>>, Path>>();
      var innerList = new LinkedList<Pair<Path, LinkedList<Long>>>();
      var longList = new LinkedList<Long>();

      longList.add(0L);
      innerList.add(new Pair<>(Path.of("target"), longList));
      listOfPairs.add(new Pair<>(innerList, Path.of("source")));

      var patterns = new Patterns(new From("a"), new To("j"));
      var amanuensisProcessObj = new AmanuensisProcess(new Triple<>(TS, listOfPairs, patterns));

      var pair = amanuensisProcessObj.amanuensisProcess();
      var result = pair.second();

      assertTrue(result.isSuccess());

      var triple = result.successValue();
      var success = triple.first();
      var failure = triple.second();

      assertEquals(1, failure.size());
      assertEquals("target", failure.removeFirst().toString());

      assertTrue(success.removeFirst().first().isEmpty());
    }

  }

}
