package au.kjd.jreplace.domain;

import au.kjd.jreplace.model.From;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Patterns;
import au.kjd.jreplace.model.To;
import au.kjd.jreplace.model.Triple;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.LinkedList;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestClassOrder;

import static au.kjd.jreplace.model.Route.TS;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.io.CleanupMode.ALWAYS;


@TestClassOrder(ClassOrderer.OrderAnnotation.class)
class AmanuensisMatchTest implements TempFileProducer {

//  @AfterAll
//  static void teardown() { Replicate.deleteJreplaceTempDirectory(); }

  @Nested
  @Order(1)
  class AmanuensisMatch_Make_It {

    @Test
    void given_5_files_that_contain_a_match_should_return_list_off_5_successes_and_0_failures(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      var listOfPairPaths = pullout29KB(tempDir);

      assertEquals(5, listOfPairPaths.size());
      assertTrue(Files.readString(tempDir.resolve("lipsum.txt")).contains("a"));

      var patterns = new Patterns(new From("a"), new To("j"));
      var amanuensisMatchObj = new AmanuensisMatch(new Triple<>(TS, listOfPairPaths, patterns));
      var outerpair = amanuensisMatchObj.amanuensisMatch();

      var result = outerpair.second();

      assertTrue(result.isSuccess());

      var innerpair = result.successValue();

      assertEquals(5, innerpair.first().size());
      assertEquals(0, innerpair.second().size());
    }

    @Test
    void given_5_files_that_dont_contain_a_match_should_return_list_off_0_successes_and_5_failures(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      var listOfPairPaths = pullout29KB(tempDir);

      assertEquals(5, listOfPairPaths.size());
      assertFalse(Files.readString(tempDir.resolve("lipsum.txt")).contains("j"));

      var patterns = new Patterns(new From("j"), new To("a"));
      var amanuensisMatchObj = new AmanuensisMatch(new Triple<>(TS, listOfPairPaths, patterns));
      var outerpair = amanuensisMatchObj.amanuensisMatch();

      var result = outerpair.second();

      assertTrue(result.isSuccess());

      var innerpair = result.successValue();

      assertEquals(0, innerpair.first().size());
      assertEquals(5, innerpair.second().size());
    }

  }

  @Nested
  @Order(2)
  class AmanuensisMatch_Break_It {

    @Test
    void given_empty_list_should_result_is_failure() {

      var listOfPairPaths = new LinkedList<Pair<Path, Path>>();

      var patterns = new Patterns(new From("j"), new To("a"));
      var amanuensisMatchObj = new AmanuensisMatch(new Triple<>(TS, listOfPairPaths, patterns));
      var outerpair = amanuensisMatchObj.amanuensisMatch();

      var result = outerpair.second();

      assertTrue(result.isFailure());
      assertInstanceOf(IllegalStateException.class, result.failureValue());
    }

    @Test
    void given_null_should_result_is_failure() {

      var listOfPairPaths = new LinkedList<Pair<Path, Path>>();
      listOfPairPaths.add(null);

      var patterns = new Patterns(new From("j"), new To("a"));
      var amanuensisMatchObj = new AmanuensisMatch(new Triple<>(TS, listOfPairPaths, patterns));
      var outerpair = amanuensisMatchObj.amanuensisMatch();

      var result = outerpair.second();

      assertTrue(result.isFailure());
      assertInstanceOf(IllegalStateException.class, result.failureValue());
    }
  }
}
