package au.kjd.jreplace.domain;

import au.kjd.jreplace.model.Pair;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestClassOrder;

import static au.kjd.jreplace.domain.DomainObjectsAndMethods.ReplicateMethodsImpl.obtainResultTest;
import static au.kjd.jreplace.model.Route.TS;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.io.CleanupMode.ALWAYS;


@TestClassOrder(ClassOrderer.OrderAnnotation.class)
class ReplicateTest implements TempFileProducer {

//  @AfterAll
//  static void teardown() { Replicate.deleteJreplaceTempDirectory(); }

  @Nested
  @Order(1)
  class Replicate_Make_It {

    @Test
    void replicate_source_to_target_and_all_lines_match(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      // LinkedList<Pair<Path, Path>>
      var listOfPairPaths = pullout29KB(tempDir);

      assertEquals(5, listOfPairPaths.size());

      var paths = listOfPairPaths.removeFirst();

      var pairRouteResult = new Replicate(new Pair<>(TS, paths.first())).replicate();
      var resultTriple = pairRouteResult.second();

      assertTrue(resultTriple.isSuccess());

      // Pair<LinkedList<Pair<Path, Path>>, LinkedList<Path>>
      var triple = resultTriple.successValue();

      assertEquals(1, triple.first().size());
      assertTrue(triple.second().isEmpty());
      assertTrue(triple.third().isEmpty());

      var llist = triple.first();

      // pair.first = target, pair.second = source
      do {

        var pairPath = llist.removeFirst();

        assertLinesMatch(Files.lines(pairPath.second()), Files.lines(pairPath.first()));

        assertTrue(Replicate.deleteTempFile(pairPath.first()).orThrow());

        pairPath = null;

      } while (!llist.isEmpty());
    }
  }

  @Nested
  @Order(2)
  class Replicate_Break_It {

    @Test
    void given_absent_file_replicate_returns_1_failure_1_exception() {

      var source = Path.of("absentfile.txt");

      // Result<Triple<LinkedList<Pair<Path, Path>>, LinkedList<Path>, LinkedList<Exception>>>
      var result = obtainResultTest(source);

      assertTrue(result.isSuccess());

      var triple = result.successValue();

      var successes = triple.first();
      var failures = triple.second();
      var exceptions = triple.third();

      assertEquals(0, successes.size());
      assertEquals(1, failures.size());
      assertEquals(1, exceptions.size());

      assertEquals("absentfile.txt", failures.removeFirst().toString());
      assertInstanceOf(IllegalStateException.class, exceptions.removeFirst());
    }

    @Test
    void given_null_replicate_returns_1_failure_1_exception() {

      // Result<Triple<LinkedList<Pair<Path, Path>>, LinkedList<Path>, LinkedList<Exception>>>
      var result = obtainResultTest((Path) null);

      assertTrue(result.isSuccess());

      var triple = result.successValue();

      var successes = triple.first();
      var failures = triple.second();
      var exceptions = triple.third();

      assertEquals(0, successes.size());
      assertEquals(1, failures.size());
      assertEquals(1, exceptions.size());

      assertNull(failures.removeFirst());
      assertInstanceOf(IllegalStateException.class, exceptions.removeFirst());
    }
  }
}
