package au.kjd.jreplace.domain;

import au.kjd.jreplace.fpjava.Result;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Triple;

import java.nio.channels.FileChannel;
import java.nio.file.Path;
import java.util.LinkedList;

import static au.kjd.jreplace.domain.Replicate.getEmptyFile;
import static au.kjd.jreplace.exceptional.ExceptionalUtilities.results;

import static java.nio.channels.FileChannel.open;
import static java.nio.file.Files.size;
import static java.nio.file.StandardOpenOption.READ;
import static java.nio.file.StandardOpenOption.WRITE;


interface DomainObjectsAndMethods {

  // --------------------------------------------------------------------------------------------- \\
  class ReplicateMethodsImpl {

    static Result<Triple<LinkedList<Pair<Path, Path>>, LinkedList<Path>, LinkedList<Exception>>> obtainResultTest(Path source) {

      return results(() -> {

        var successes  = new LinkedList<Pair<Path, Path>>();
        var failures   = new LinkedList<Path>();
        var exceptions = new LinkedList<Exception>();

        Result<Path> target = duplicateSourceTest(source);

        if (target.isFailure()) { failures.add(source); exceptions.add(target.failureValue()); }

        else {

          Pair<Path, Path> pair = new Pair<>(target.successValue(), source);

          successes.add(pair);

          pair = null;
        }

        return new Triple<>(successes, failures, exceptions);
      });
    }

    static Result<Path> duplicateSourceTest(Path source) {

      return results(() -> {

        Path target = getEmptyFile(source).orThrow();

        try (FileChannel channel = open(target, WRITE)) { channel.transferFrom(open(source, READ), 0, size(source)); }

        return target;
      });
    }
  }

  // --------------------------------------------------------------------------------------------- \\
  class AristidesMethodsImpl {

    static class AristidesFractionaliseTest {

      static boolean isGreaterThanIntegerMaxValueTest(long sourceSize) { return Integer.MAX_VALUE < sourceSize; }

      static long nTimesGreaterTest(long sourceSize) {
        var totalsize = sourceSize;
        var integermaxvalue = (long) Integer.MAX_VALUE;
        var times = 0;

        do {
          times++;
          totalsize -= integermaxvalue;
        } while (totalsize > 0);

        return times;
      }

    }

  }

}
