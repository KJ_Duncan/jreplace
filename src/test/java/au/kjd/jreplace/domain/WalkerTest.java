package au.kjd.jreplace.domain;

import au.kjd.jreplace.model.Tuple;

import java.io.IOException;
import java.nio.file.attribute.PosixFilePermission;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.LinkedList;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Stream;

import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestClassOrder;

import static java.lang.String.format;
import static java.nio.file.attribute.PosixFilePermission.OWNER_READ;
import static java.nio.file.attribute.PosixFilePermission.OWNER_WRITE;
import static java.nio.file.Files.setPosixFilePermissions;
import static java.nio.file.FileSystems.getDefault;

import static au.kjd.jreplace.model.Route.TS;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.io.CleanupMode.ALWAYS;


@ExtendWith({Teardown.class})
@TestClassOrder(ClassOrderer.OrderAnnotation.class)
class WalkerTest implements TempFileProducer {


  @Nested
  @Order(1)
  class Walker_Make_It {

    @Test
    void traverse_depth_5_glob_pattern_returns_5_from_6_files_ending_with_txt(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      var listOfPaths = pullout29KB(tempDir);

      Path lipsum_text = tempDir.resolve("lipsum.text");
      Files.copy(tempDir.resolve("lipsum.txt"), lipsum_text, StandardCopyOption.REPLACE_EXISTING);

      var tuple = new Tuple<>(TS, tempDir, 5, getDefault().getPathMatcher(format("%s:%s", "glob", "*.txt")));

      // Pair<Route, Result<Triple<LinkedList<Path>, LinkedList<Path>, LinkedList<Exception>>>>
      var traversal = new Walker(tuple).walker();

      assertTrue(traversal.second().isSuccess());

      var triple = traversal.second().successValue();

      assertEquals(5, triple.first().size());
      assertEquals(0, triple.second().size());
      assertEquals(0, triple.third().size());

      var paths = triple.first();

      do {

        Path path = paths.removeFirst();

        assertTrue(path.getFileName().toString().endsWith("txt"));

      } while (!paths.isEmpty());

    }

    @Test
    void traverse_depth_5_regex_pattern_returns_5_from_6_files_ending_with_txt(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      var listOfPaths = pullout29KB(tempDir);

      Path lipsum_text = tempDir.resolve("lipsum.text");
      Files.copy(tempDir.resolve("lipsum.txt"), lipsum_text, StandardCopyOption.REPLACE_EXISTING);

      var tuple = new Tuple<>(TS, tempDir, 5, getDefault().getPathMatcher(format("%s:%s", "regex", "[a-z_0-9]*.txt")));

      // Pair<Route, Result<Triple<LinkedList<Path>, LinkedList<Path>, LinkedList<Exception>>>>
      var traversal = new Walker(tuple).walker();

      assertTrue(traversal.second().isSuccess());

      var triple = traversal.second().successValue();

      assertEquals(5, triple.first().size());
      assertEquals(0, triple.second().size());
      assertEquals(0, triple.third().size());

      var paths = triple.first();

      do {

        Path path = paths.removeFirst();

        assertTrue(path.getFileName().toString().endsWith("txt"));

      } while (!paths.isEmpty());

    }
  }

  @Nested
  @Order(2)
  class Walker_Break_It {

    @Test
    void traverse_given_null_returns_result_failure_IllegalStateException() {

      var tuple = new Tuple<>(TS, (Path) null, 5, getDefault().getPathMatcher(format("%s:%s", "regex", "[a-z_0-9]*.txt")));

      // Pair<Route, Result<Triple<LinkedList<Path>, LinkedList<Path>, LinkedList<Exception>>>>
      var traversal = new Walker(tuple).walker();

      assertTrue(traversal.second().isFailure());
      // java.lang.IllegalStateException: java.lang.NullPointerException: Cannot invoke "java.nio.file.Path.getFileSystem()" because "path" is null
      assertInstanceOf(IllegalStateException.class, traversal.second().failureValue());
    }

    @Test
    void traverse_given_an_non_executable_folder_should_return_1_failure_1_exception(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      var listOfPaths = pullout29KB(tempDir);

      Set<PosixFilePermission> USER_RW  = Set.of(OWNER_READ, OWNER_WRITE);

      var listOfDirectories = new LinkedList<Path>();

      try (Stream<Path> directory = Files.find(tempDir, 5, (path, attributes) ->  attributes.isDirectory() && path.getFileName().toString().startsWith("level"))) {
        directory.forEach(listOfDirectories::add);
      }

      assertEquals(4, listOfDirectories.size());

      try {
        Path dir = listOfDirectories.peekLast();
        setPosixFilePermissions(Objects.requireNonNull(dir), USER_RW);
      }
      catch (IOException e) { throw new RuntimeException(e); }

      var tuple = new Tuple<>(TS, tempDir, 5, getDefault().getPathMatcher(format("%s:%s", "regex", "[a-z_0-9]*.txt")));

      // Pair<Route, Result<Triple<LinkedList<Path>, LinkedList<Path>, LinkedList<Exception>>>>
      var traversal = new Walker(tuple).walker();

      assertTrue(traversal.second().isSuccess());

      var triple = traversal.second().successValue();

      assertEquals(4, triple.first().size());
      assertEquals(1, triple.second().size());
      assertEquals(1, triple.third().size());

      assertEquals("lipsum_04.txt", triple.second().removeFirst().getFileName().toString());
      assertInstanceOf(java.nio.file.AccessDeniedException.class, triple.third().removeFirst());
    }

  }

}
