package au.kjd.jreplace.domain;

import au.kjd.jreplace.model.RegexFrom;
import au.kjd.jreplace.model.RegexTo;
import au.kjd.jreplace.model.Scan;
import au.kjd.jreplace.model.Triple;

import java.io.IOException;
import java.nio.file.Path;
import java.util.concurrent.atomic.LongAdder;
import java.util.LinkedList;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.ClassOrderer;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestClassOrder;

import static au.kjd.jreplace.model.Route.TS;

import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.io.CleanupMode.ALWAYS;


@TestClassOrder(ClassOrderer.OrderAnnotation.class)
class InterchangeTest implements TempFileProducer {

//  @AfterAll
//  static void teardown() { Replicate.deleteJreplaceTempDirectory(); }

  @Nested
  @Order(1)
  class Interchange_Make_It {

    // Triple<Route, LinkedList<Triple<Path, Scan, Path>>, Boolean>
    @Test
    void given_5_files_and_replace_false_should_return_list_size_5_with_path_names_containing_jreplace(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      // LinkedList<Pair<Path, Path>>
      var listOfPairPaths = pullout29KB(tempDir);
      assertEquals(5, listOfPairPaths.size());

      var list = new LinkedList<Triple<Path, Scan, Path>>();

      do {

        var pair = listOfPairPaths.removeFirst();
        var scan = new Scan(new RegexFrom(new LongAdder()), new RegexTo(new LongAdder()));

        list.add(new Triple<>(pair.first(), scan, pair.second()));

        pair = null;
        scan = null;

      } while (!listOfPairPaths.isEmpty());

      var triple = new Triple<>(TS, list, Boolean.FALSE);

      var interchangeObj = new Interchange(triple);

      // Pair<Route, Result<LinkedList<Tuple<Path, Scan, Path, Result<Path>>>>>
      var pairResult = interchangeObj.interchange();

      var result = pairResult.second();

      assertTrue(result.isSuccess());

      var listTuple = result.successValue();

      assertEquals(5, listTuple.size());

      do {

        var tuple = listTuple.removeFirst();

        assertSame(tuple.first(), tuple.third());
        assertTrue(tuple.fourth().isSuccess());
        assertTrue(tuple.fourth().successValue().getFileName().toString().matches("lipsum[_0-4]*?-jreplace-\\d+.txt"));

        tuple = null;

      } while (!listTuple.isEmpty());
    }

    @Test
    void given_5_files_and_replace_true_should_return_list_size_5_with_path_names_matching_source_path(@TempDir(cleanup = ALWAYS) Path tempDir) throws IOException {

      // LinkedList<Pair<Path, Path>>
      var listOfPairPaths = pullout29KB(tempDir);
      assertEquals(5, listOfPairPaths.size());

      var list = new LinkedList<Triple<Path, Scan, Path>>();

      do {

        var pair = listOfPairPaths.removeFirst();
        var scan = new Scan(new RegexFrom(new LongAdder()), new RegexTo(new LongAdder()));

        list.add(new Triple<>(pair.first(), scan, pair.second()));

        pair = null;
        scan = null;

      } while (!listOfPairPaths.isEmpty());

      var triple = new Triple<>(TS, list, Boolean.TRUE);

      var interchangeObj = new Interchange(triple);

      // Pair<Route, Result<LinkedList<Tuple<Path, Scan, Path, Result<Path>>>>>
      var pairResult = interchangeObj.interchange();

      var result = pairResult.second();

      assertTrue(result.isSuccess());

      var listTuple = result.successValue();

      assertEquals(5, listTuple.size());

      do {

        var tuple = listTuple.removeFirst();

        assertSame(tuple.first(), tuple.third());
        assertTrue(tuple.fourth().isSuccess());
        assertTrue(tuple.fourth().successValue().getFileName().toString().matches("lipsum[_0-4]*?.txt"));

        tuple = null;

      } while (!listTuple.isEmpty());
    }
  }

  @Nested
  @Order(2)
  class Interchange_Break_It {

    @Test
    void given_empty_list_and_replace_true_should_return_result_of_empty_list() {

      var list = new LinkedList<Triple<Path, Scan, Path>>();

      var triple = new Triple<>(TS, list, Boolean.TRUE);

      var interchangeObj = new Interchange(triple);

      // Pair<Route, Result<LinkedList<Tuple<Path, Scan, Path, Result<Path>>>>>
      var pairResult = interchangeObj.interchange();

      var result = pairResult.second();

      assertTrue(result.isSuccess());
      assertTrue(result.successValue().isEmpty());
    }

    @Test
    void given_empty_list_and_replace_false_should_return_result_of_empty_list() {

      var list = new LinkedList<Triple<Path, Scan, Path>>();

      var triple = new Triple<>(TS, list, Boolean.FALSE);

      var interchangeObj = new Interchange(triple);

      // Pair<Route, Result<LinkedList<Tuple<Path, Scan, Path, Result<Path>>>>>
      var pairResult = interchangeObj.interchange();

      var result = pairResult.second();

      assertTrue(result.isSuccess());
      assertTrue(result.successValue().isEmpty());
    }

    @Test
    void given_list_of_null_should_return_result_is_failure() {

      var list = new LinkedList<Triple<Path, Scan, Path>>();
      list.add(null);

      var triple = new Triple<>(TS, list, Boolean.FALSE);

      var interchangeObj = new Interchange(triple);

      // Pair<Route, Result<LinkedList<Tuple<Path, Scan, Path, Result<Path>>>>>
      var pairResult = interchangeObj.interchange();

      var result = pairResult.second();

      assertTrue(result.isFailure());
      assertInstanceOf(IllegalStateException.class, result.failureValue());
    }

    @Test
    void given_list_of_absent_path_and_replace_false_should_return_tuple_fourth_move_result_is_failure() {

      var list = new LinkedList<Triple<Path, Scan, Path>>();
      var scan = new Scan(new RegexFrom(new LongAdder()), new RegexTo(new LongAdder()));
      list.add(new Triple<>(Path.of("target"), scan, Path.of("source")));

      var triple = new Triple<>(TS, list, Boolean.FALSE);

      var interchangeObj = new Interchange(triple);

      // Pair<Route, Result<LinkedList<Tuple<Path, Scan, Path, Result<Path>>>>>
      var pairResult = interchangeObj.interchange();

      var result = pairResult.second();

      assertTrue(result.isSuccess());

      var listTuple = result.successValue();

      assertEquals(1, listTuple.size());

      var tuple = listTuple.removeFirst();

      assertTrue(tuple.fourth().isFailure());
      assertInstanceOf(IllegalStateException.class, tuple.fourth().failureValue());
    }

    @Test
    void given_list_of_absent_path_and_replace_true_should_return_tuple_fourth_move_result_is_failure() {

      var list = new LinkedList<Triple<Path, Scan, Path>>();
      var scan = new Scan(new RegexFrom(new LongAdder()), new RegexTo(new LongAdder()));
      list.add(new Triple<>(Path.of("target"), scan, Path.of("source")));

      var triple = new Triple<>(TS, list, Boolean.TRUE);

      var interchangeObj = new Interchange(triple);

      // Pair<Route, Result<LinkedList<Tuple<Path, Scan, Path, Result<Path>>>>>
      var pairResult = interchangeObj.interchange();

      var result = pairResult.second();

      assertTrue(result.isSuccess());

      var listTuple = result.successValue();

      assertEquals(1, listTuple.size());

      var tuple = listTuple.removeFirst();

      assertTrue(tuple.fourth().isFailure());
      assertInstanceOf(IllegalStateException.class, tuple.fourth().failureValue());
    }

  }

}
