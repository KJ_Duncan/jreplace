package au.kjd.jreplace.domain;

import au.kjd.jreplace.model.Pair;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.StandardOpenOption;
import java.util.LinkedList;
import java.util.stream.Stream;


interface TempFileProducer {

  default LinkedList<Pair<Path, Path>> pullout29KB(Path tempDir) throws IOException {

    Path original = Paths.get("src/test/resources/Latin-Lipsum.txt");

    Path lipsum = tempDir.resolve("lipsum.txt");
    Path level_01 = Files.createTempDirectory(tempDir, "level_01_");
    Path level_02 = Files.createTempDirectory(level_01, "level_02_");
    Path level_03 = Files.createTempDirectory(level_02, "level_03_");
    Path level_04 = Files.createTempDirectory(level_03, "level_04_");

    Path lipsum_01 = level_01.resolve("lipsum_01.txt");
    Path lipsum_02 = level_02.resolve("lipsum_02.txt");
    Path lipsum_03 = level_03.resolve("lipsum_03.txt");
    Path lipsum_04 = level_04.resolve("lipsum_04.txt");

    Files.copy(original, lipsum, StandardCopyOption.REPLACE_EXISTING);
    Files.copy(lipsum, lipsum_01, StandardCopyOption.REPLACE_EXISTING);
    Files.copy(lipsum, lipsum_02, StandardCopyOption.REPLACE_EXISTING);
    Files.copy(lipsum, lipsum_03, StandardCopyOption.REPLACE_EXISTING);
    Files.copy(lipsum, lipsum_04, StandardCopyOption.REPLACE_EXISTING);

    LinkedList<Pair<Path, Path>> listOfPairPaths = new LinkedList<>();

    try (Stream<Path> files = Files.find(tempDir, 5, (path, attributes) -> path.getFileName().toString().endsWith(".txt"))) {
      files.forEach(path -> listOfPairPaths.add(new Pair<>(path, path)));
    }

    return listOfPairPaths;
  }

  default LinkedList<Pair<Path, Path>> pullout2800KB(Path tempDir) throws IOException {

    Path original = Paths.get("src/test/resources/Latin-Lipsum.txt");

    Path lipsum = tempDir.resolve("lipsum.txt");
    Path level_01 = Files.createTempDirectory(tempDir, "level_01_");
    Path level_02 = Files.createTempDirectory(level_01, "level_02_");
    Path level_03 = Files.createTempDirectory(level_02, "level_03_");
    Path level_04 = Files.createTempDirectory(level_03, "level_04_");

    Path lipsum_01 = level_01.resolve("lipsum_01.txt");
    Path lipsum_02 = level_02.resolve("lipsum_02.txt");
    Path lipsum_03 = level_03.resolve("lipsum_03.txt");
    Path lipsum_04 = level_04.resolve("lipsum_04.txt");

    StandardOpenOption[] CWA = { StandardOpenOption.CREATE, StandardOpenOption.WRITE, StandardOpenOption.APPEND };

    // created file size is approximately 2.8MB
    for (int i = 0; i <= 100; i++) Files.write(lipsum, Files.readAllLines(original), StandardCharsets.UTF_8, CWA);

    Files.copy(lipsum, lipsum_01, StandardCopyOption.REPLACE_EXISTING);
    Files.copy(lipsum, lipsum_02, StandardCopyOption.REPLACE_EXISTING);
    Files.copy(lipsum, lipsum_03, StandardCopyOption.REPLACE_EXISTING);
    Files.copy(lipsum, lipsum_04, StandardCopyOption.REPLACE_EXISTING);

    LinkedList<Pair<Path, Path>> listOfPairPaths = new LinkedList<>();

    try (Stream<Path> files = Files.find(tempDir, 5, (path, attributes) -> path.getFileName().toString().endsWith(".txt"))) {
      files.forEach(path -> listOfPairPaths.add(new Pair<>(path, path)));
    }

    return listOfPairPaths;
  }
}

/*

    TEST: failing test suit as the jreplace temporary directory must be empty before deletion, thus throws an IOException
          a conditional check for command line property -Djreplace=tests will disable the jreplace temporary directory
          deletion via the SourceFile.deleteJreplaceTempDirectory() static method and has been added to the jvmArgs in
          the test task of the build.gradle.kts file to avoid the failing tests scenario.

  if (Objects.equals(System.getProperty("jreplace"), "tests")) ; // a workaround as negation on a null value equals true
  else  Replicate.deleteJreplaceTempDirectory();

*/
