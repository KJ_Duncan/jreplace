package au.kjd.jreplace.domain;

import org.junit.jupiter.api.extension.*;


public final class Teardown implements org.junit.jupiter.api.extension.AfterAllCallback {

  @Override
  public void afterAll(ExtensionContext context) { Replicate.deleteJreplaceTempDirectory(); }

}
