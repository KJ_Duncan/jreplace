package au.kjd.jreplace.domain;

import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.Triple;
import au.kjd.jreplace.model.Tuple;
import au.kjd.jreplace.model.Route;

import java.util.LinkedList;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;
import java.util.regex.Pattern;
import java.util.StringJoiner;

import org.junit.jupiter.api.*;

import static au.kjd.jreplace.model.Route.TS;

import static java.util.logging.Logger.getLogger;


class MinitruTest {

  static final Logger logger = getLogger(MinitruTest.class.getSimpleName());
  // %u=N.log
  static final String LOGFILE = "jreplace-example-tourism-%u.log";
  static final String FORMAT   = "JReplace example error log: %-8s -> %n-- %-8s";
  // <https://regex101.com/r/9DrDRd/2>
  static final Pattern pattern = Pattern.compile("(,\\h)(?!second|third|fourth)|(?<=\\])(,\\h)");
  static final String BANNER = "-----------------------------------------------------";
  static final String INDENT = "-- ";

  static void initialise() {
    try {
      Handler fileHandler = new FileHandler(LOGFILE);
      logger.setLevel(Level.WARNING);
      logger.addHandler(fileHandler);
      fileHandler.setFormatter(new SimpleFormatter());
    }
    catch (Exception ignored) { }
  }

  static void winston(Pair<Route, Pair<String, LinkedList<Object>>> pair) {

    loggerJoiner(String.format(FORMAT, pair.second().first(), editorial(pair.second().second())));

    pair.second().second().clear();
  }

  static String editorial(LinkedList<Object> list) {

    return pattern.matcher(list.toString()).replaceAll("\n-- ");
  }

  // Apache Derby 2020, Logger.java, org.apache.derbyDemo.scores.util
  static void loggerJoiner(String text) {
    logger.warning("\n" + new StringJoiner("\n", BANNER, BANNER)
        .add(INDENT)
        .add(INDENT)
        .add(INDENT + text)
        .add(INDENT)
        .add(INDENT));
  }

  @Test
  void test_winston_example_log() {

    initialise();

    LinkedList<Object> list = new LinkedList<>();

    LinkedList<Object> innerlist = new LinkedList<>();

    innerlist.add("java.nio.file.AccessDeniedException: level_1/level_2/level_3/javaConstants03_2.7MB.kak");
    innerlist.add("java.nio.file.AccessDeniedException: level_1/level_2/level_3/level_4");
    innerlist.add("java.nio.file.AccessDeniedException: level_1/level_2/level_3/javaConstants03_14MB.kak");

    Pair<?, ?> innerpair = new Pair<>("this", "that");
    Pair<?, ?> anotherpair = new Pair<>(innerlist, "that");
    Triple<?, ?, ?> innertriple = new Triple<>("this", "that", "thing");
    Triple<?, ?, ?> anothertriple = new Triple<>("this", innerlist, "thing");
    Tuple<?, ?, ?, ?> innertuple = new Tuple<>("this", "that", "thing", "though");
    Tuple<?, ?, ?, ?> anothertuple = new Tuple<>("this", "that", "thing", innerlist);

    list.add(innerpair);
    list.add(anotherpair);
    list.add(innertriple);
    list.add(anothertriple);
    list.add(innertuple);
    list.add(anothertuple);

    var pair = new Pair<>(TS, new Pair<>("MinitruTest", list));

    winston(pair);
  }

}
