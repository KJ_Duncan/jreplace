package au.kjd.jreplace.handler.atjhandlers;

import au.kjd.jreplace.fpjava.Result;
import au.kjd.jreplace.model.Nothing;
import au.kjd.jreplace.model.Pair;
import au.kjd.jreplace.model.RegexFrom;
import au.kjd.jreplace.model.RegexTo;
import au.kjd.jreplace.model.Route;
import au.kjd.jreplace.model.Scan;
import au.kjd.jreplace.model.Triple;

import java.nio.file.Path;
import java.util.concurrent.atomic.LongAdder;
import java.util.LinkedList;

import static au.kjd.jreplace.exceptional.ExceptionalUtilities.perhaps;
import static au.kjd.jreplace.fpjava.Case.cause;
import static au.kjd.jreplace.fpjava.Case.match;
import static au.kjd.jreplace.model.BooleanSuppliers.BooleanSupplierPair;
import static au.kjd.jreplace.model.BooleanSuppliers.BooleanSupplierTriple;
import static au.kjd.jreplace.model.Nothing.NOTHING;
import static au.kjd.jreplace.model.Route.M2;
import static au.kjd.jreplace.model.Route.M4;
import static au.kjd.jreplace.model.Route.STR;
import static au.kjd.jreplace.model.Route.TS;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;


/* tests omitted for classes as same logic used: Controller, TheJustHandler, WalkerReplicateHandler */
class AmanuensisHandlerTest {

  LinkedList<String> linkedList = new LinkedList<>();

  @BeforeEach
  void setup() { if (0 < linkedList.size()) linkedList.clear(); }

  void amanuensisMatchHandlerTest(Pair<Route, Result<Pair<LinkedList<Pair<Path, Path>>, LinkedList<Path>>>> pairResult) {

    // Pair<LinkedList<Pair<Path, Path>>, LinkedList<Path>>
    var bsp = new BooleanSupplierPair<>(pairResult);

    boolean checkResult = pairResult.second().isSuccess();

    perhaps(checkResult,
        NOTHING,
        otiose ->
            match(
                cause(() -> stateRouteTest(new Pair<>(M2, "default cause"))),

                cause(bsp.hasSuccessFailures, () -> {

                  stateRouteTest(new Pair<>(STR, "hasSuccessFailures"));

                  return stateRouteTest(new Pair<>(M2, "hasSuccessFailures"));

                }),

                cause(bsp.noSuccessesHasFailures, () -> stateRouteTest(new Pair<>(STR, "noSuccessesHasFailures"))),

                cause(bsp.noSuccessesFailures, () -> stateRouteTest(new Pair<>(STR, "noSuccessesFailures")))
            ),
        otiose -> stateRouteTest(new Pair<>(STR, "result is failure"))
    );
  }

  void amanuensisProcessHandlerTest(Pair<Route, Result<Triple<LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>> pairResult) {

    // Supplier<Triple<LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>
    var bst = new BooleanSupplierTriple<>(pairResult);

    boolean checkResult = pairResult.second().isSuccess();

    perhaps(checkResult,
        NOTHING,
        otiose ->
            match(
                cause(() -> stateRouteTest(new Pair<>(STR, "default cause"))),

                cause(bst.hasSuccessesFailuresExceptions, () -> {

                  stateRouteTest(new Pair<>(STR, "hasSuccessesFailuresExceptions"));

                  stateRouteTest(new Pair<>(STR, "hasSuccessesFailuresExceptions"));

                  return stateRouteTest(new Pair<>(M4, "hasSuccessesFailuresExceptions"));
                }),

                cause(bst.hasSuccessesExceptionsNoFailures, () -> {

                  stateRouteTest(new Pair<>(STR, "hasSuccessesExceptionsNoFailures"));

                  return stateRouteTest(new Pair<>(M4, "hasSuccessesExceptionsNoFailures"));
                }),

                cause(bst.hasSuccessesFailuresNoExceptions, () -> {

                  stateRouteTest(new Pair<>(STR, "hasSuccessesFailuresNoExceptions"));

                  return stateRouteTest(new Pair<>(M4, "hasSuccessesFailuresNoExceptions"));
                }),

                cause(bst.hasSuccessesNoFailuresExceptions, () -> stateRouteTest(new Pair<>(M4, "hasSuccessesNoFailuresExceptions"))),

                cause(bst.noSuccessesHasFailuresExceptions, () -> {

                  stateRouteTest(new Pair<>(STR, "noSuccessesHasFailuresExceptions"));

                  return stateRouteTest(new Pair<>(STR, "noSuccessesHasFailuresExceptions"));
                }),

                cause(bst.noSuccessesExceptionsHasFailures, () -> stateRouteTest(new Pair<>(STR, "noSuccessesExceptionsHasFailures"))),

                cause(bst.noSuccessesFailuresHasExceptions, () -> stateRouteTest(new Pair<>(STR, "noSuccessesFailuresHasExceptions")))
            ),
        otiose -> stateRouteTest(new Pair<>(STR, "result is failure"))
    );
  }

  <M> Nothing stateRouteTest(Pair<Route, M> pair) {
    switch (pair.first()) {
      case STR -> { linkedList.add(pair.first().name()); linkedList.add((String) pair.second()); }
      case M2  -> { linkedList.add(pair.first().name()); linkedList.add((String) pair.second()); }
      case N2  -> { linkedList.add(pair.first().name()); linkedList.add((String) pair.second()); }
      case M4  -> { linkedList.add(pair.first().name()); linkedList.add((String) pair.second()); }
      case N4  -> { linkedList.add(pair.first().name()); linkedList.add((String) pair.second()); }
    }
    return NOTHING;
  }

  @Nested
  class AmanuensisMatchHandlerTest {

    @Test
    void amanuensis_match_result_is_failure_STR() {

      var pairFailure = new Pair<Route, Result<Pair<LinkedList<Pair<Path, Path>>, LinkedList<Path>>>>(TS, Result.failure("a failed state"));

      amanuensisMatchHandlerTest(pairFailure);

      assertEquals(2, linkedList.size());
      assertEquals("STR", linkedList.removeFirst());
      assertEquals("result is failure", linkedList.removeFirst());
    }

    @Test
    void amanuensis_match_has_successes_and_failures_STR_M2() {

      var successes = new LinkedList<Pair<Path, Path>>();
      successes.add(new Pair<>(Path.of("target"), Path.of("source")));

      var failures = new LinkedList<Path>();
      failures.add(Path.of("source"));

      var innerPair = new Pair<>(successes, failures);
      var outerPair = new Pair<>(TS, Result.success(innerPair));

      amanuensisMatchHandlerTest(outerPair);

      assertEquals(4, linkedList.size());
      assertEquals("STR", linkedList.removeFirst());
      assertEquals("hasSuccessFailures", linkedList.removeFirst());
      assertEquals("M2", linkedList.removeFirst());
      assertEquals("hasSuccessFailures", linkedList.removeFirst());
    }

    @Test
    void amanuensis_match_has_successes_no_failures_M2() {

      var successes = new LinkedList<Pair<Path, Path>>();
      successes.add(new Pair<>(Path.of("target"), Path.of("source")));

      var failures = new LinkedList<Path>();
      var innerPair = new Pair<>(successes, failures);
      var outerPair = new Pair<>(TS, Result.success(innerPair));

      amanuensisMatchHandlerTest(outerPair);

      assertEquals(2, linkedList.size());
      assertEquals("M2", linkedList.removeFirst());
      assertEquals("default cause", linkedList.removeFirst());
    }

    @Test
    void amanuensis_match_no_successes_and_has_failures_STR() {

      var successes = new LinkedList<Pair<Path, Path>>();
      var failures = new LinkedList<Path>();
      failures.add(Path.of("source"));

      var innerPair = new Pair<>(successes, failures);
      var outerPair = new Pair<>(TS, Result.success(innerPair));

      amanuensisMatchHandlerTest(outerPair);

      assertEquals(2, linkedList.size());
      assertEquals("STR", linkedList.removeFirst());
      assertEquals("noSuccessesHasFailures", linkedList.removeFirst());
    }

    @Test
    void amanuensis_match_no_successes_no_failures_STR() {

      var successes = new LinkedList<Pair<Path, Path>>();
      var failures = new LinkedList<Path>();
      var innerPair = new Pair<>(successes, failures);
      var outerPair = new Pair<>(TS, Result.success(innerPair));

      amanuensisMatchHandlerTest(outerPair);

      assertEquals(2, linkedList.size());
      assertEquals("STR", linkedList.removeFirst());
      assertEquals("noSuccessesFailures", linkedList.removeFirst());
    }

  }

  @Nested
  class AmanuensisProcessHandlerTest {

    @Test
    void amanuensis_process_result_is_failure_STR() {

      var failed = new Pair<Route, Result<Triple<LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>, LinkedList<Path>, LinkedList<Exception>>>>(TS, Result.failure("a failed state"));

      amanuensisProcessHandlerTest(failed);

      assertEquals(2, linkedList.size());
      assertEquals("STR", linkedList.removeFirst());
      assertEquals("result is failure", linkedList.removeFirst());
    }

    @Test
    void amanuensis_process_no_successes_failures_exceptions_STR() {

      var successes = new LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>();
      var failures = new LinkedList<Path>();
      var exceptions = new LinkedList<Exception>();
      var outertriple = new Triple<>(successes, failures, exceptions);
      var outerpair = new Pair<>(TS, Result.success(outertriple));

      amanuensisProcessHandlerTest(outerpair);

      assertEquals(2, linkedList.size());
      assertEquals("STR", linkedList.removeFirst());
      assertEquals("default cause", linkedList.removeFirst());
    }

    @Test
    void amanuensis_process_has_successes_failures_exceptions_STR_STR_M4() {

      var scan = new Scan(new RegexFrom(new LongAdder()), new RegexTo(new LongAdder()));

      var failures = new LinkedList<Path>();
      failures.add(Path.of("source"));

      var exceptions = new LinkedList<Exception>();
      exceptions.add(new Exception("failed reason"));

      var innertriple = new LinkedList<Triple<Path, Long, Scan>>();
      innertriple.add(new Triple<>(Path.of("target"), 20L, scan));

      var innerpair = new Pair<>(innertriple, Path.of("source"));

      var successes = new LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>();
      successes.add(innerpair);

      var outertriple = new Triple<>(successes, failures, exceptions);
      var outerpair = new Pair<>(TS, Result.success(outertriple));

      amanuensisProcessHandlerTest(outerpair);

      assertEquals(6, linkedList.size());
      assertEquals("STR", linkedList.removeFirst());
      assertEquals("hasSuccessesFailuresExceptions", linkedList.removeFirst());
      assertEquals("STR", linkedList.removeFirst());
      assertEquals("hasSuccessesFailuresExceptions", linkedList.removeFirst());
      assertEquals("M4", linkedList.removeFirst());
      assertEquals("hasSuccessesFailuresExceptions", linkedList.removeFirst());
    }

    @Test
    void amanuensis_process_has_successes_exceptions_no_failures_STR_M4() {

      var scan = new Scan(new RegexFrom(new LongAdder()), new RegexTo(new LongAdder()));

      var failures = new LinkedList<Path>();
      var exceptions = new LinkedList<Exception>();
      exceptions.add(new Exception("failed reason"));

      var innertriple = new LinkedList<Triple<Path, Long, Scan>>();
      innertriple.add(new Triple<>(Path.of("target"), 20L, scan));

      var innerpair = new Pair<>(innertriple, Path.of("source"));

      var successes = new LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>();
      successes.add(innerpair);

      var outertriple = new Triple<>(successes, failures, exceptions);
      var outerpair = new Pair<>(TS, Result.success(outertriple));

      amanuensisProcessHandlerTest(outerpair);

      assertEquals(4, linkedList.size());
      assertEquals("STR", linkedList.removeFirst());
      assertEquals("hasSuccessesExceptionsNoFailures", linkedList.removeFirst());
      assertEquals("M4", linkedList.removeFirst());
      assertEquals("hasSuccessesExceptionsNoFailures", linkedList.removeFirst());
    }

    @Test
    void amanuensis_process_has_successes_failures_no_exceptions_STR_M4() {

      var scan = new Scan(new RegexFrom(new LongAdder()), new RegexTo(new LongAdder()));

      var failures = new LinkedList<Path>();
      failures.add(Path.of("source"));

      var exceptions = new LinkedList<Exception>();
      var innertriple = new LinkedList<Triple<Path, Long, Scan>>();
      innertriple.add(new Triple<>(Path.of("target"), 20L, scan));

      var innerpair = new Pair<>(innertriple, Path.of("source"));

      var successes = new LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>();
      successes.add(innerpair);

      var outertriple = new Triple<>(successes, failures, exceptions);
      var outerpair = new Pair<>(TS, Result.success(outertriple));

      amanuensisProcessHandlerTest(outerpair);

      assertEquals(4, linkedList.size());
      assertEquals("STR", linkedList.removeFirst());
      assertEquals("hasSuccessesFailuresNoExceptions", linkedList.removeFirst());
      assertEquals("M4", linkedList.removeFirst());
      assertEquals("hasSuccessesFailuresNoExceptions", linkedList.removeFirst());
    }

    @Test
    void amanuensis_process_has_successes_no_failures_exceptions_M4() {

      var scan = new Scan(new RegexFrom(new LongAdder()), new RegexTo(new LongAdder()));

      var failures = new LinkedList<Path>();
      var exceptions = new LinkedList<Exception>();

      var innertriple = new LinkedList<Triple<Path, Long, Scan>>();
      innertriple.add(new Triple<>(Path.of("target"), 20L, scan));

      var innerpair = new Pair<>(innertriple, Path.of("source"));

      var successes = new LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>();
      successes.add(innerpair);

      var outertriple = new Triple<>(successes, failures, exceptions);
      var outerpair = new Pair<>(TS, Result.success(outertriple));

      amanuensisProcessHandlerTest(outerpair);

      assertEquals(2, linkedList.size());
      assertEquals("M4", linkedList.removeFirst());
      assertEquals("hasSuccessesNoFailuresExceptions", linkedList.removeFirst());
    }

    @Test
    void amanuensis_process_no_successes_has_failures_exceptions_STR_STR() {

      var successes = new LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>();

      var failures = new LinkedList<Path>();
      failures.add(Path.of("source"));

      var exceptions = new LinkedList<Exception>();
      exceptions.add(new Exception("failed reason"));

      var outertriple = new Triple<>(successes, failures, exceptions);
      var outerpair = new Pair<>(TS, Result.success(outertriple));

      amanuensisProcessHandlerTest(outerpair);

      assertEquals(4, linkedList.size());
      assertEquals("STR", linkedList.removeFirst());
      assertEquals("noSuccessesHasFailuresExceptions", linkedList.removeFirst());
      assertEquals("STR", linkedList.removeFirst());
      assertEquals("noSuccessesHasFailuresExceptions", linkedList.removeFirst());
    }

    @Test
    void amanuensis_process_no_successes_exceptions_has_failures_STR() {

      var successes = new LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>();

      var failures = new LinkedList<Path>();
      failures.add(Path.of("source"));

      var exceptions = new LinkedList<Exception>();
      var outertriple = new Triple<>(successes, failures, exceptions);
      var outerpair = new Pair<>(TS, Result.success(outertriple));

      amanuensisProcessHandlerTest(outerpair);

      assertEquals(2, linkedList.size());
      assertEquals("STR", linkedList.removeFirst());
      assertEquals("noSuccessesExceptionsHasFailures", linkedList.removeFirst());
    }

    @Test
    void amanuensis_process_no_successes_failures_has_exceptions_STR() {

      var successes = new LinkedList<Pair<LinkedList<Triple<Path, Long, Scan>>, Path>>();
      var failures = new LinkedList<Path>();

      var exceptions = new LinkedList<Exception>();
      exceptions.add(new Exception("failed reason"));

      var outertriple = new Triple<>(successes, failures, exceptions);
      var outerpair = new Pair<>(TS, Result.success(outertriple));

      amanuensisProcessHandlerTest(outerpair);

      assertEquals(2, linkedList.size());
      assertEquals("STR", linkedList.removeFirst());
      assertEquals("noSuccessesFailuresHasExceptions", linkedList.removeFirst());
    }

  }

}
