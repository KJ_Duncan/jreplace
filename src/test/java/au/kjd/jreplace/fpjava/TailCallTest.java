package au.kjd.jreplace.fpjava;

import au.kjd.jreplace.model.Pair;

import java.util.function.Function;
import java.util.LinkedList;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;


class TailCallTest {

  public <A, B, C> LinkedList<C> zipWith(LinkedList<A> aList, LinkedList<B> bList, Function<A, Function<B, C>> fn) {

    return zipWithHelper(new LinkedList<>(), aList, bList, fn).eval();
  }

  private <A, B, C> TailCall<LinkedList<C>> zipWithHelper(LinkedList<C> acc, LinkedList<A> aList, LinkedList<B> bList, Function<A, Function<B, C>> fn) {

    return aList.isEmpty() || bList.isEmpty()
        ? TailCall.ret(acc)
        : TailCall.sus(() -> zipWithHelper((acc.add(fn.apply(aList.removeFirst()).apply(bList.removeFirst()))) ? acc : acc, aList, bList, fn));
  }

  @Test
  void given_two_even_lists_zip_with_returns_list_size_2() {

    var alist = new LinkedList<>();
    alist.add("alist: first");
    alist.add("alist: second");

    var blist = new LinkedList<>();
    blist.add("blist: first");
    blist.add("blist: second");

    //  [Pair[first=alist: first, second=blist: first], Pair[first=alist: second, second=blist: second]]
    var clist = zipWith(alist, blist, a -> b -> new Pair<>(a, b));

    assertEquals(2, clist.size());

    var firstelement = clist.removeFirst();

    assertEquals("alist: first", firstelement.first().toString());
    assertEquals("blist: first", firstelement.second().toString());

    var secondelement = clist.removeFirst();

    assertEquals("alist: second", secondelement.first().toString());
    assertEquals("blist: second", secondelement.second().toString());
  }

}
