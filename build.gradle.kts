plugins {
  `java-library`
  // id("me.champeau.jmh") version "0.6.8"
}

group = "au.kjd.jreplace"
version = "0.6.1"

val moduleName by extra("au.kjd.jreplace")
val javaHome: String = System.getProperty("java.home")

repositories { mavenCentral() }

dependencies {
  implementation("info.picocli:picocli:4.7.0")
  annotationProcessor("info.picocli:picocli-codegen:4.7.0") // GraalVM native-image

  // https://mvnrepository.com/artifact/org.jetbrains/annotations
  compileOnly("org.jetbrains:annotations:23.0.0")

  // implementation("org.openjdk.jmh:jmh-generator-asm:1.35")
  testImplementation("org.junit.jupiter:junit-jupiter-api:5.9.0")
  testImplementation("org.assertj:assertj-core:3.23.1")
  testRuntimeOnly("org.junit.jupiter:junit-jupiter-engine:5.9.0")
}

/* As at 04 December 2022: GRAALVM_HOME/bin/native-image --version GraalVM 22.3.0 Java 19 CE (Java Version 19.0.1+10-jvmci-22.3-b08) */
tasks.withType<JavaCompile> { options.compilerArgs = listOf("--release", "19", "-Aproject=${project.group}/${project.name}") }

// "-Xmx7168m" au.kjd.jreplace.domain.AmanuensisProcessTest.AmanuensisProcess_Make_It.given_a_file_size_2866_megabytes....
tasks.withType<Test> { jvmArgs("--enable-preview", "-enableassertions") }

tasks {
  /* https://docs.gradle.org/current/userguide/java_library_plugin.html#sec:java_library_modular
   * Gradle - Building Modules for the Java Module System
   *        - Java Module System support is an incubating feature */
  compileJava {
    options.javaModuleMainClass.set("au.kjd.jreplace.Main")
    options.javaModuleVersion.set(provider { project.version as String })
    java.modularity.inferModulePath.set(true)
  }

  test {
    enabled = true
    useJUnitPlatform { excludeTags("Broken") }
    failFast = true
    testLogging.showStandardStreams = false // true
    testLogging { events("passed", "failed" ) }
  }

  jar {
    enabled = true

    from(sourceSets.main.get().output)
    exclude { details: FileTreeElement ->
      details.file.name.endsWith(".jpg")  ||
      details.file.name.endsWith(".png")  ||
      details.file.name.endsWith(".txt")  ||
      details.file.name.endsWith(".puml") ||
      details.file.name.endsWith(".text")
    }

    manifest {
      attributes(
        "Manifest-Version" to "2.0",
        "Created-By" to "Kirk Duncan",
        "Name" to "au/kjd/jreplace/",
        "Sealed" to "true",
        "Main-Class" to "au.kjd.jreplace.Main",
        "Implementation-Title" to "Replace a files text: regex command line utility",
        "Implementation-Version" to project.version
      )
    }
  }

  register<Jar>("uberJar") {
    archiveClassifier.set("uber")

    from(sourceSets.main.get().output)
    exclude { details: FileTreeElement ->
      details.file.name.endsWith(".jpg")  ||
          details.file.name.endsWith(".png")  ||
          details.file.name.endsWith(".txt")  ||
          details.file.name.endsWith(".puml") ||
          details.file.name.endsWith(".text")
    }

    dependsOn(configurations.runtimeClasspath)
    from({
      configurations.runtimeClasspath.get().filter {
        it.name.endsWith("jar")
      }.map { zipTree(it) }
    })

    manifest {
      attributes(
        "Manifest-Version" to "2.0",
        "Created-By" to "Kirk Duncan",
        "Name" to "au/kjd/jreplace/",
        "Sealed" to "true",
        "Main-Class" to "au.kjd.jreplace.Main",
        "Implementation-Title" to "Replace a files text: a regex command line utility",
        "Implementation-Version" to project.version
      )
    }
  }
}

/*
jmh {
  // includes = ['some regular expression'] // include pattern (regular expression) for benchmarks to be executed
  // excludes = ['some regular expression'] // exclude pattern (regular expression) for benchmarks to be executed
  iterations.set(5) // Number of measurement iterations to do.
  benchmarkMode.addAll("ss") //= ['thrpt','ss'] // Benchmark mode. Available modes are: [Throughput/thrpt, AverageTime/avgt, SampleTime/sample, SingleShotTime/ss, All/all]
  batchSize.set(5000) //= 1 // Batch size: number of benchmark method calls per operation. (some benchmark modes can ignore this setting)
  fork.set(1) //= 0 // How many times to forks a single benchmark. Use 0 to disable forking altogether
  failOnError.set(true) //= true // Should JMH fail immediately if any benchmark had experienced the unrecoverable error?
  forceGC.set(true)// = false // Should JMH force GC between iterations?
  // jvm = 'myjvm' // Custom JVM to use when forking.
  jvmArgs.addAll("-Xmx3072M") //= [ 'Custom JVM args to use when forking.'] "--release", "17", "--enable-preview",
  // jvmArgsAppend = ['Custom JVM args to use when forking (append these)']
  // jvmArgsPrepend =[ 'Custom JVM args to use when forking (prepend these)']
  humanOutputFile.set(project.file("${project.buildDir}/results/jmh/human.txt")) // = project.file("${project.buildDir}/results/jmh/human.txt") // human-readable output file
  resultsFile.set(project.file("${project.buildDir}/results/jmh/results.txt")) //= project.file("${project.buildDir}/results/jmh/results.txt") // results file
  // operationsPerInvocation.set(10) //= 10 // Operations per invocation.
  benchmarkParameters.empty() // =  [:] // Benchmark parameters.
  profilers.empty() //= [] // Use profilers to collect additional data. Supported profilers: [cl, comp, gc, stack, perf, perfnorm, perfasm, xperf, xperfasm, hs_cl, hs_comp, hs_gc, hs_rt, hs_thr, async]
  timeOnIteration.set("5s") //= '1s' // Time to spend at each measurement iteration.
  resultFormat.set("TEXT") //= 'TEXT' // Result format type (one of CSV, JSON, NONE, SCSV, TEXT)
  // synchronizeIterations.set(true) //= true // Synchronize iterations?
  threads.set(1) //= 0 // Number of worker threads to run with.
  // threadGroups = [2,3,4] //Override thread group distribution for asymmetric benchmarks.
  jmhTimeout.set("1s") // Timeout for benchmark iteration.
  timeUnit.set("ms") // = 'ms' // Output time unit. Available time units are: [m, s, ms, us, ns].
  verbosity.set("NORMAL") //= 'NORMAL' // Verbosity mode. Available modes are: [SILENT, NORMAL, EXTRA]
  // warmup.set("5s") //= '1s' // Time to spend at each warmup iteration.
  warmupBatchSize.set(5000) //= 10 // Warmup batch size: number of benchmark method calls per operation.
  warmupForks.set(1) //= 0 // How many warmup forks to make for a single benchmark. 0 to disable warmup forks.
  warmupIterations.set(5) // = 0 // Number of warmup iterations to do.
  // warmupMode.set("INDI") //= 'INDI' // Warmup mode for warming up selected benchmarks. Warmup modes are: [INDI, BULK, BULK_INDI].
  // warmupBenchmarks = ['.*Warmup'] // Warmup benchmarks to include in the run in addition to already selected. JMH will not measure these benchmarks, but only use them for the warmup.
  zip64.set(true) //= true // Use ZIP64 format for bigger archives
  jmhVersion.set("1.35") //= '1.35' // Specifies JMH version
  includeTests.set(false) //= true // Allows to include test sources into generate JMH jar, i.e. use it when benchmarks depend on the test classes.
  duplicateClassesStrategy.set(DuplicatesStrategy.WARN) //= DuplicatesStrategy.FAIL // Strategy to apply when encountring duplicate classes during creation of the fat jar (i.e. while executing jmhJar task)
}
*/

