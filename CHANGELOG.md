# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.6.1 - 2022-12-04

- patch: Route.lock released on terminal processes see: handler/InterchangeEmissionMOTHandler.java

## 0.6.0 - 2022-11-26

- minor: took a small project analysed the data flow and decoupled the domain logic

## 0.5.0 - 2022-10-10

- minor: transformed the single file script into a small project

## 0.4.0 - 2021-09-18

- minor: JEP 411: Deprecate the Security Manager for Removal

## 0.3.0 - 2021-09-08

- minor: SedConsumer runs stream processing in parallel

## 0.2.0 - 2021-09-07

- minor: accepts -glob or -regex file name patterns from cli
- patch: walk the file tree and filter paths
- patch: print to stdout -> parent/filename.ext

## 0.1.0 - 2021-08-25

- minor: package/module rename replace.java to au.kjd.jrename
- patch: update to gradle version 7.2


